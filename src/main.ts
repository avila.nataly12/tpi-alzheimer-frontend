import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import Mapboxgl from 'mapbox-gl';

Mapboxgl.accessToken = 'pk.eyJ1IjoibmF0YXZpbGEiLCJhIjoiY2w4YjN5Ymk1MGhlajN2bHlmdnI3ZGg3ZyJ9.l6rNDRSpojQpphQXH-GYLA';

if (environment.production) {
  enableProdMode();
}

if (!navigator.geolocation) {
  alert('El navegador no soporta la geolocalización.');
  throw new Error('El navegador no soporta la geolocalización.');
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
