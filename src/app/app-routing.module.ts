import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { MapaRecorridoFamiliarComponent } from './pages/mapa-recorrido-familiar/mapa-recorrido-familiar.component';
import { LoginGuard } from './services/auth/login.guard';
export const Approutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'cuidador',
        loadChildren: () => import('./users/cuidador/cuidador.module').then(m => m.CuidadorModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'notas',
        loadChildren: () => import('./pages/notas/notas.module').then(m => m.NotasModule),
        canActivate: [LoginGuard]
      }
      ,{
        path: 'reconocimientoFacial',
        loadChildren: () => import('./pages/reconocimientoFacial/reconocimiento.module').then(m => m.ReconocimientoModule),
        canActivate: [LoginGuard]
      }
      ,
      {
        path: 'compartirMultimedia',
        loadChildren: () => import('./pages/compartirMultimedia/compartirMultimedia.module').then(m => m.CompartirMultimediaModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'mapa',
        loadChildren: () => import('./pages/mapa/mapa.module').then(m => m.MapaModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'juegos',
        loadChildren: () => import('./pages/juegos/juegos.module').then(m => m.JuegosModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'pacientes',
        loadChildren: () => import('./pages/pacientes/pacientes.module').then(m => m.PacientesModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'about',
        loadChildren: () => import('./about/about.module').then(m => m.AboutModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'galeria',
        loadChildren: () => import('./pages/galeria/galeria.module').then(m => m.GaleriaModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'upload',
        loadChildren: () => import('./pages/upload/upload.module').then(m => m.UploadModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'deteccion',
        loadChildren: () => import('./pages/deteccion/deteccion.module').then(m => m.DeteccionModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'identificar',
        loadChildren: () => import('./pages/identificar/identificar.module').then(m => m.IdentificarModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'cronograma-medicamentos',
        loadChildren: () => import('./pages/cronograma-medicamentos/cronograma-medicamentos.module').then(m => m.CronogramaMedicamentosModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'contactos',
        loadChildren: () => import('./pages/contactos/contactos.module').then(m => m.ContactosModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'datos-salud',
        loadChildren: () => import('./pages/datos-salud/datos-salud.module').then(m => m.DatosSaludModule),
        canActivate: [LoginGuard]
      },
      {
        path: 'alarma',
        loadChildren: () => import('./pages/alarma/alarma.module').then(m => m.AlarmaModule),
        canActivate: [LoginGuard]
      },
    ]
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path:'familiar/ver-recorrido/:id',
    component: MapaRecorridoFamiliarComponent
  },
  {
    path: '**',
    redirectTo: '/auth/login'
  }
];
