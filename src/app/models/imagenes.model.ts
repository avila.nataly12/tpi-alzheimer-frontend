export class ImagenesModel{

    id?: string;
    nombreImagen: string;
    imgUrl:string;
    idUsuario:string;


    constructor(nombreImagen:string, imgUrl:string,idUsuario:string){
        this.nombreImagen = nombreImagen;
        this.imgUrl = imgUrl;
         this.idUsuario = idUsuario;
    }

}