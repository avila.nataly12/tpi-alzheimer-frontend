import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotasComponent } from '../../pages/notas/notas.component';
import { VerNotaComponent } from '../../pages/notas/ver-nota/ver-nota.component';
import { CuidadorComponent } from './cuidador.component';
import { CrearNotaComponent } from '../../pages/notas/crear-nota/crear-nota.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'notas',
        component: CrearNotaComponent
      },
      {
        path: 'vernotas',
        component: NotasComponent
      },
      {
        path: 'notas/:id',
        component: VerNotaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuidadorRoutingModule { }
