export interface Usuario {
    id: string,
    nombre: string,
    apellido: string,
    dni: string,
    telefono: string,
    rol: string, 
    email: string,
    contrasena: string, 
    direccion: string,
    idCuidador: string,
    coordsdir: [number, number]
    
}