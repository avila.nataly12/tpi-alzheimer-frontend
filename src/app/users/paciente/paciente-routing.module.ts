import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearNotaComponent } from 'app/pages/notas/crear-nota/crear-nota.component';
import { NotasComponent } from 'app/pages/notas/notas.component';
import { VerNotaComponent } from 'app/pages/notas/ver-nota/ver-nota.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CrearNotaComponent
      },
      {
        path: 'notas',
        component: NotasComponent
      },
      {
        path: 'notas/:id',
        component: VerNotaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacienteRoutingModule { }
