import { RouteInfo } from './sidebar.metadata';
import { Usuario } from '../../users/interface/usuario';

export const RoutesPaciente: RouteInfo[] = [

  {
    path:'/dashboard',
    title: 'Inicio',
    icon: 'bi bi-house',
    class: '',

  },
  {
    path: '/galeria',
    title: 'Galería',
    icon: 'bi bi-camera',
    class: '',
  },
  {
    path: '/mapa/empezar-recorrido',
    title: 'Recorrido',
    icon: 'bi bi-geo-alt',
    class: '',

  },
  {
    path: '/identificar',
    title: 'Reconocimiento Facial',
    icon: 'bi bi-emoji-smile',
    class: '',

  },
  // {
  //   path: '/notas/crear',
  //   title: 'Crear Nota',
  //   icon: 'bi bi-plus',
  //   class: '',

  // },
  {
    path: '/notas',
    title: 'Notas',
    icon: 'bi bi-sticky',
    class: '',

  },
  {
    path: '/cronograma-medicamentos',
    title: 'Cronograma Medicamentos',
    icon: 'bi bi-list-check',
    class: '',

  },
];
