import { RouteInfo } from './sidebar.metadata';
import { Usuario } from '../../users/interface/usuario';

export const ROUTES: RouteInfo[] = [

  {
    path:'/dashboard',
    title: 'Inicio',
    icon: 'bi bi-house',
    class: '',

  },  
  {
    path: '/galeria',
    title: 'Galería',
    icon: 'bi bi-camera',
    class: '',


  },
  {
    path: '/mapa',
    title: 'Recorrido',
    icon: 'bi bi-geo-alt',
    class: '',

  }/*,
  {
    path: '/identificar',
    title: 'Reconocimiento Facial',
    icon: 'bi bi-emoji-smile',
    class: '',

  }
  ,  
  {
    path: '/upload/1',
    title: 'Cargar Archivo',
    icon: 'bi bi-collection-play',
    class: '',

  }*/
  ,  
  {
    path: '/compartirMultimedia/listado',
    title: 'Listado Multimedia',
    icon: 'bi bi-collection-play',
    class: '',

  },  
  {
    path: '/compartirMultimedia',
    title: 'Transmitir en pantalla',
    icon: 'bi bi-tv',
    class: '',

  },
  {
    path: '/notas',
    title: 'Notas',
    icon: 'bi bi-sticky',
    class: '',

  },
  {
    path: '/juegos/habilitar',
    title: 'Administrar Juegos',
    icon: 'bi bi-joystick',
    class: '',

  },
  {
    path: '/juegos/reporte',
    title: 'Reporte de Juegos',
    icon: 'bi bi-bar-chart-line',
    class: '',

  },
  // {
  //   path: '/cronograma-medicamentos',
  //   title: 'Cronogramas Medicamentos',
  //   icon: 'bi bi-list-check',
  //   class: '',

  // },
  {
    path: '/contactos',
    title: 'Listado Contactos',
    icon: 'bi bi-people-fill',
    class: '',

  },
  // {
  //   path: '/cronograma-medicamentos/crear',
  //   title: 'Crear Cronograma Medicamento',
  //   icon: 'bi bi-plus',
  //   class: '',

  // },
];

