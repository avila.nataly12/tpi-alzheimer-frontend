import { Component, AfterViewInit, OnInit } from '@angular/core';
import { ROUTES } from './menu-items';
import { RoutesPaciente } from './menu-items-paciente';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MapService } from '../../services/mapa/map.service';
import { AuthService } from '../../services/auth/auth.service';
//declare var $: any;
import { ArchivoService } from '../../services/archivos/archivos.service';
import { Archivos } from '../../interfaces/archivo';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  foto!: Archivos;
  nombreUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).nombre;
  rolUsuario =(this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  showMenu = '';
  showSubMenu = '';
  fotoPerfil="user1.jpg";

  public sidebarnavItems:RouteInfo[]=[];
  public sidebarnavItemsPacientes:RouteInfo[]=[];
  // this is for the open close
  addExpandClass(element: string) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private archivoService: ArchivoService,

  ) {
     if(this.rolUsuario==="Paciente"){
      this.fotoPerfil="paciente.jpg";

    this.archivoService.verFotoPerfil(this.idUsuario).subscribe(archivo => {
      this.foto=archivo;
      }, err => {
        //console.log('Error');
      });
    }

  }

  // End open close
  ngOnInit() {
    if(this.rolUsuario==="Cuidador"){
      this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
    }else if (this.rolUsuario === "Paciente") {
      this.sidebarnavItemsPacientes = RoutesPaciente.filter(sidebarnavItemPacientes => sidebarnavItemPacientes);
    }
    
  }

}
