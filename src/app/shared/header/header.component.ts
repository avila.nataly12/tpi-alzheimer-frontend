import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth/auth.service';
import { Usuario } from '../../users/interface/usuario';
import { CronogramaMedicamentosService } from '../../services/cronograma-medicamentos/cronograma-medicamentos.service';
import { ContactoService } from '../../services/contacto/contacto.service';
import { Contacto } from '../../interfaces/contacto';
import Swal from 'sweetalert2';
import { MapService } from '../../services/mapa/map.service';
import { RecorridoService } from '../../services/recorrido/recorrido.service';
import { Recorrido } from '../../interfaces/recorrido';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  styles: [
    `
    .alert-link {
      cursor: pointer;
    }
    `
  ]
})
export class HeaderComponent implements AfterViewInit {
  listaContacto!: Contacto[];

  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  nombreUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).nombre;
  idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  listaPacientes!: Usuario[];
  bandera = 0;
  telefonoLlamar!: any;

  userLocation = this.mapService.userLocation;

  calle!: string;

  localidad!: string;

  coordenadas!: any;

  ultubicacion!: any;

  posicion: number = 0;

  recorrido!: Recorrido;

  estadoRecorrido!: string;

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private cronogramaService: CronogramaMedicamentosService,
    private contactoService: ContactoService,
    private mapService: MapService,
    private recorridoService: RecorridoService

  ) {
    this.calle="";
    this.localidad="";
    this.contactoService.miBoton(this.idUsuario).subscribe(contactos => {
      this.listaContacto = contactos;
      this.listaContacto.forEach((contacto) => {

        this.telefonoLlamar = contacto.telefono;
      });

      if(this.listaContacto.length==0){ this.telefonoLlamar = "";}
    });

    this.userLocation = this.mapService.userLocation;
    
  }

  ngAfterViewInit(): void {

    console.log(this.mapService.userLocation);
    const tiempoTranscurrido = Date.now();

    const dias = [
      'Domingo',
      'Lunes',
      'Martes',
      'Miércoles',
      'Jueves',
      'Viernes',
      'Sábado',
    ];
    const numeroDia = new Date(tiempoTranscurrido).getDay();
    const nombreDia = dias[numeroDia];

    if (this.rolUsuario == "Cuidador") {
      this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
        this.listaPacientes = usuarios;
        usuarios.forEach((elemento) => {


          this.cronogramaService.verCronogramaDePaciente(elemento.id)
            .subscribe(data => {
              if (data.length > 0) {
                data.forEach((cronograma) => {
                  cronograma.items.forEach((remedio) => {

                    if(remedio.estado==true){
                    var momentoActual2 = new Date();
                    const hhora2 = momentoActual2.getHours();
                    var horaActual2 = ("0" + hhora2).slice(-2);
                    var mimmnutoActual2 = momentoActual2.getMinutes()
                    var minutoActual2 = ("0" + mimmnutoActual2).slice(-2);
                    var tiempoActual2 = horaActual2 + ":" + minutoActual2;

                    var fecha2 = new Date('1/1/1990 ' + tiempoActual2);
                    var fecha1 = new Date('1/1/1990 ' + remedio.hora);

                    if (remedio.dias.includes(nombreDia) && fecha1 > fecha2) {

                      setInterval(() => {
                        this.muestroAlarma(remedio.hora, remedio.medicamento, elemento.id)
                      }, 50000);


                    }

                  }
                    //fin de verificar el estado del medicamento
                  });
                });

              }

            }, error => {
             // console.log(error.error);
            });


        });

      }, err => {
      //  console.log('Error');
      });
    }
    if (this.rolUsuario == "Paciente") {

      this.cronogramaService.verCronogramaDePaciente(this.idUsuario)
        .subscribe(data => {
          if (data.length > 0) {
            data.forEach((cronograma) => {
              cronograma.items.forEach((remedio) => {

                if(remedio.estado==true){

                var momentoActual2 = new Date();
                const hhora2 = momentoActual2.getHours();
                var horaActual2 = ("0" + hhora2).slice(-2);
                var mimmnutoActual2 = momentoActual2.getMinutes()
                var minutoActual2 = ("0" + mimmnutoActual2).slice(-2);
                var tiempoActual2 = horaActual2 + ":" + minutoActual2;

                var fecha2 = new Date('1/1/1990 ' + tiempoActual2);
                var fecha1 = new Date('1/1/1990 ' + remedio.hora);

                if (remedio.dias.includes(nombreDia) && fecha1 > fecha2) {

                  setInterval(() => {
                    this.muestroAlarma(remedio.hora, remedio.medicamento, this.idUsuario)
                  }, 50000);
                }

              }//fin de verificar el estado del medicamento

              });
            });

          }

        }, error => {
        });

      // Actualización última ubicación

      this.recorridoService.listarRecorridoPorIdUsuario(this.idUsuario)
        .subscribe(data => {
          this.recorrido = data[data.length - 1];
          this.coordenadas = data[data.length - 1].coordenadas;
          this.estadoRecorrido = this.recorrido.estado;

         // console.log(this.coordenadas.length);

          if (this.recorrido.estado === "En curso") {
            var interval = setInterval(() => {
              if (this.coordenadas.length > this.posicion) {

                this.ultubicacion = this.coordenadas[this.posicion];

               // console.log(this.ultubicacion);
                this.posicion++;

              //  console.log("Length coords: " + this.coordenadas.length)
               // console.log("Posicion " + this.posicion);

                this.actulizarUbicacionActual();

              } else {
                this.recorridoService.editarEstadoRecorrido(this.recorrido.id, 'Finalizado')
                  .subscribe(data => {
                    console.log('Recorrido finalizado');
                    Swal.fire({
                      icon: 'success',
                      title: 'Recorrido finalizado',
                      showCancelButton: false,
                      confirmButtonText: 'Aceptar',
                      allowOutsideClick: false
                    }).then((result) => {
                      if (result.isConfirmed) {
                        this.router.navigate([`/dashboard`]);
                      }

                      clearInterval(interval);

                    }, error => {
                    //  console.log(error);

                    });
                  });
              }
            }, 10000);
          } else {
           // console.log("Recorrido finalizado");
          }

        }, error => {
       //   console.log(error);

        });

    }

    this.mapService.geocoding(this.mapService.userLocation[0], this.mapService.userLocation[1])
       .subscribe(data => {
         this.calle = data.features[0].text;
         this.localidad = data.features[0].context[1].text;
         
       });
  }


  muestroAlarma(hora: any, medicamento: string, id: string) {
    var momentoActual = new Date()

    const hhora = momentoActual.getHours();
    var horaActual = ("0" + hhora).slice(-2);
    var mimmnutoActual = momentoActual.getMinutes();
    var minutoActual = ("0" + mimmnutoActual).slice(-2);
    var tiempoActual = horaActual + ":" + minutoActual;

   // console.log(tiempoActual);
   // console.log(hora);

    if (tiempoActual == hora && this.bandera == 0) {
      const bandera = 1
      this.router.navigate([`/alarma/${hora}/${medicamento}/${id}`]);
      //window.location.href = "/alarma/" + hora + "/" + medicamento + '/' + id;
    }
    return "";
  }


dameDireccion(){
  this.mapService.geocoding(this.mapService.userLocation[0], this.mapService.userLocation[1])
  .subscribe(data => {
    this.calle = data.features[0].text;
    this.localidad = data.features[0].context[1].text;

  });
}

  public enviarSOS(): void {


   
    this.dameDireccion();
    this.idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

    this.contactoService.miBoton(this.idUsuario).subscribe(contactos => {
      this.listaContacto = contactos;
      this.listaContacto.forEach((contacto) => {

        var nombre = contacto.nombre;
        var telefono = contacto.telefono;
        var mensaje = `${this.nombreUsuario} se encuentra perdido . Ingresa al siguiente link para ver su ubicación: https://www.google.es/maps?q=${this.mapService.userLocation[1]},${this.mapService.userLocation[0]}`;

        if(this.calle!="" && this.localidad!=""){
        var mensaje = `${this.nombreUsuario} se encuentra perdido en ${this.calle}, ${this.localidad}. Ingresa al siguiente link para ver su ubicación: https://www.google.es/maps?q=${this.mapService.userLocation[1]},${this.mapService.userLocation[0]}`;
        }
        this.contactoService.EnviarWhatsapp(nombre, telefono, mensaje).subscribe(mensaje => {

          Swal.fire({
            icon: 'warning',
            title: 'Tranquilo, ya estamos en camino',
           // text: `${this.mapService.userLocation}`,
            showCancelButton: true,
            confirmButtonText: 'Llamar S.O.S',
            cancelButtonText: 'Cancelar S.O.S',
            allowOutsideClick: false
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = "tel:" + telefono;
            //  console.log("Llamar por telefono");

            } else {
           //   console.log("Falsa alarma");

       
             this.listaContacto.forEach((contacto) => {
              var telefono = contacto.telefono;
              this.contactoService.EnviarWhatsapp(nombre, telefono, "Falsa alarma.").subscribe();
              }); 
              
            
            }


          });

          //console.log("Telefono " + telefono);

        });

      });

    }, err => {

      console.log('Error');
    });

  }

  actulizarUbicacionActual() {
    this.recorridoService.editarUltimaUbicacion(this.idUsuario, this.ultubicacion)
      .subscribe(data => {
      //  console.log("Ultima ubicacion actualizada");
      }, error => {
      //  console.log(error);
      });

  }

  mostrarBotones(variable:any){
 
 
    if(variable=="compu"){
      (document.querySelector('.ocultobotonesEmergencia') as HTMLElement).style.display = 'block';
      (document.querySelector('.nuevoBotonEmergencias') as HTMLElement).style.display = 'none';
      }else{
        (document.querySelector('.ocultobotonesEmergenciaCelular') as HTMLElement).style.display = 'block';
        (document.querySelector('.celular') as HTMLElement).style.display = 'none';
      }
  }
  
  ocultarBotones(variable:any){
  
  
    if(variable=="compu"){
      (document.querySelector('.ocultobotonesEmergencia') as HTMLElement).style.display = 'none';
      (document.querySelector('.nuevoBotonEmergencias') as HTMLElement).style.display = 'block';
    }else{
      (document.querySelector('.ocultobotonesEmergenciaCelular') as HTMLElement).style.display = 'none';
      (document.querySelector('.celular') as HTMLElement).style.display = 'block';
    }
  
  }

  editarEstadoRecorrido() {
//    console.log(this.recorrido);
//envio link para compartir recorrido
if(!this.recorrido){}else{

this.contactoService.miBoton(this.idUsuario).subscribe(contactos => {
  this.listaContacto = contactos;
  this.listaContacto.forEach((contacto) => {
    const link = `https://alfa-frontend-366501.rj.r.appspot.com/familiar/ver-recorrido/${this.idUsuario}`;
    var nombre = contacto.nombre;
    var telefono = contacto.telefono;
    var telefono = contacto.telefono;
    var mensaje = `${this.nombreUsuario} comenzo un recorrido. Haz click en siguiente link para ver su recorrido: ${link}`;
    this.contactoService.EnviarWhatsapp(nombre, telefono, mensaje).subscribe();
  
  });  }); }


    this.recorridoService.editarEstadoRecorrido(this.recorrido.id, 'En curso')
      .subscribe(data => {
        window.location.href ='/mapa/empezar-recorrido';
      });
  }

}



/*
   notifyMe():void {
    // Comprobamos si el navegador soporta las notificaciones
    if (!("Notification" in window)) {
      alert("Este navegador no es compatible con las notificaciones de escritorio");
    }
  
    // Comprobamos si los permisos han sido concedidos anteriormente
    else if (Notification.permission === "granted") {
      // Si es correcto, lanzamos una notificación
    //  var notification = new Notification("¡Hola!");
    }
  
    // Si no, pedimos permiso para la notificación
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        // Si el usuario nos lo concede, creamos la notificación
        if (permission === "granted") {
//          var notification = new Notification("¡Hola!");
        }
      });
    }
  
    // Por último, si el usuario ha denegado el permiso,
    // y quiere ser respetuoso, no hay necesidad de molestarlo.
  }

*/




