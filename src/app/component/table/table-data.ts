export interface Product {
    image: string,
    uname: string,
    gmail: string,
    productName: string,
    status: string,
    weeks: number,
    budget: string
}

export interface TableRows {
    fname: string,
    lname: string,
    uname: string,
}

export const TopSelling: Product[] = [

    {
        image: 'assets/images/users/user1.jpg',
        uname: 'Ibuprofeno',
        gmail: 'Lunes',
        productName: 'Flexy React',
        status: '18:00',
        weeks: 35,
        budget: '95K',
    },
    {
        image: 'assets/images/users/user2.jpg',
        uname: 'Antiinflamatorio',
        gmail: 'Viernes',
        productName: 'Landing pro React',
        status: '13:00',
        weeks: 35,
        budget: '95K'
    },
    {
        image: 'assets/images/users/user3.jpg',
        uname: 'Bromazepan',
        gmail: 'Martes',
        productName: 'Elite React	',
        status: '8:30',
        weeks: 35,
        budget: '95K'
    }

]

export const Employee : TableRows[] = [
    {
        fname: "Mark",
        lname: "Otto",
        uname: "@mdo",
    },
    {
        fname: "Jacob",
        lname: "Thornton",
        uname: "@fat",
    },
    {
        fname: "Larry",
        lname: "the Bird",
        uname: "@twitter",
    }
]