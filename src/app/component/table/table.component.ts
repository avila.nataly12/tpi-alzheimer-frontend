import { Component, OnInit } from '@angular/core';
import { Cronograma } from '../../interfaces/cronograma-medicamento';
import {Product,TopSelling, TableRows, Employee} from './table-data';
import { CronogramaMedicamentosService } from '../../services/cronograma-medicamentos/cronograma-medicamentos.service';


@Component({
    selector: 'app-table',
    templateUrl: 'table.component.html'
})
export class TableComponent implements OnInit{
  topSelling:Product[];

  trow:TableRows[];

  cronogramas!: Cronograma;

  constructor(private cronogramaService: CronogramaMedicamentosService) { 

    this.topSelling=TopSelling;

    this.trow=Employee;
  }
  ngOnInit(): void {
    /*this.cronogramaService.verCronogramaDePaciente('634708204c5c1d9b67d43e54')
      .subscribe(resp => {
        this.cronogramas = resp;
        console.log(resp);
      });*/
  }
}
