import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';

import * as mapboxgl from 'mapbox-gl';
import { Marker, Popup } from 'mapbox-gl';

import { MapService } from '../../services/mapa/map.service';
import { RecorridoService } from '../../services/recorrido/recorrido.service';
import { AuthService } from '../../services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  styles: [
    `
    #map {
      width: 100%;
      height: 300px;
    }
    `
  ]
})
export class MapComponent implements AfterViewInit {

  @ViewChild('mapDiv')
  mapDivElement!: ElementRef;

  start = this.mapService.userLocation;

  coordenadas!: number[][];

  ubicacionDestino!: [number, number];

  ubicacionPartida!: [number, number];

  idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

  idPaciente = this.route.snapshot.paramMap.get('id')!;

  hayCoordenadas = true;

  estadoRecorrido!: string;

  fechaRecorrido!: string;
  horaRecorrido!: string;
  
  subscription!: Subscription;

  constructor(private mapService: MapService,
    private recorridoService: RecorridoService,
    private authService: AuthService,
    private route: ActivatedRoute
  ) { }

  ngAfterViewInit(): void {
    this.generateMap();

  }

  generateMap() {
    if (!this.mapService.userLocation) {
      throw Error('No hay localizacion de usuario');
    }

    const map = new mapboxgl.Map({
      container: this.mapDivElement.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.mapService.userLocation, // TODO: cambiar por la ubicacion de partida
      zoom: 14,

    });


    setTimeout(() => {
  
    this.recorridoService.listarRecorridoPorIdUsuario(this.idPaciente)
      .subscribe(resp => {
        console.log(resp);
        if (resp.length > 0) {
          this.coordenadas = resp[resp.length - 1].coordenadas;
          this.ubicacionPartida = [this.coordenadas[this.coordenadas.length - 1][0], this.coordenadas[this.coordenadas.length - 1][1]];
          this.ubicacionDestino = [this.coordenadas[0][0], this.coordenadas[0][1]];
          this.estadoRecorrido = resp[resp.length - 1].estado;

          if(this.estadoRecorrido === "Finalizado") {
            this.fechaRecorrido = resp[resp.length - 1].fechaEstimadaLlegada;
            this.horaRecorrido = resp[resp.length - 1].horaEstimadaLlegada;
          } else {
            this.fechaRecorrido = resp[resp.length - 1].fechaEstimadaPartida;
            this.horaRecorrido = resp[resp.length - 1].horaEstimadaPartida;
          }
          // Marcador inicio
          new Marker({ color: 'red' })
            .setLngLat(this.ubicacionPartida)
            .setPopup(popUp)
            .addTo(map)

          // Marcador final
          new Marker({ color: 'red' })
            .setLngLat(this.ubicacionDestino)
            .setPopup(popUp)
            .addTo(map);

          map.addControl(new mapboxgl.NavigationControl);

          map.fitBounds([this.ubicacionPartida, this.ubicacionDestino], {
            padding: 100
          });

          this.mapService.setMap(map);

          this.mapService.drawPolyLineCoordenadas(this.coordenadas);

          this.mapService.deletePlaces();
        } else {
          this.hayCoordenadas = false;
        }

      }, error => {
        console.log(error);
      });


    }, 1000);





    const popUp = new Popup()
      .setHTML(`
            <h6>Ubicación actual</h6>
        `);
  }

}
