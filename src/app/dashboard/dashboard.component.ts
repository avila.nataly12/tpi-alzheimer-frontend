import { Component, AfterViewInit } from '@angular/core';
import { Recorrido } from '../interfaces/recorrido';
//declare var require: any;
import { AuthService } from '../services/auth/auth.service';
import { MapService } from '../services/mapa/map.service';
import { RecorridoService } from '../services/recorrido/recorrido.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements AfterViewInit {
  subtitle: string;
  rol = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

  idPaciente = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

  recorrido!: Recorrido;

  estadoRecorrido!: string;

  constructor(private authService: AuthService,
              private recorridoService: RecorridoService,
              private router: Router) {

    this.subtitle = 'This is some text within a card block.';

  }
  ngAfterViewInit(): void { 

    this.recorridoService.listarRecorridoPorIdUsuario(this.idPaciente)
      .subscribe(data => {
        this.recorrido = data[data.length - 1];
        this.estadoRecorrido = data[data.length -1].estado;
        console.log(this.estadoRecorrido);
      }, error => {
        console.log(error);
      });
  }

}


