export interface Notas {
    class: string,
    icon: string,
    task: string,
    time: string,
    prioridad?: string;
}

export const notasCuidador: Notas[] = [

    {
        class: 'bg-success',
        icon: 'bi bi-bag-check',
        task: 'Mirar la novela',
        time: 'Ahora'
    },
    {
        class: 'bg-success',
        icon: 'bi bi-bag-check',
        task: 'Tomar medicación',
        time: 'Hace 14 minutos'
    },
    {
        class: 'bg-success',
        icon: 'bi bi-bag-check',
        task: 'Hacer ejercicio',
        time: 'Hace 23 minutos'
    },
    {
        class: 'bg-warning',
        icon: 'bi bi-exclamation-triangle',
        task: 'Jugar',
        time: 'Vence en 1 hora'
    },
    {
        class: 'bg-warning',
        icon: 'bi bi-exclamation-triangle',
        task: 'Cenar',
        time: 'Vence en 2 hora'
    },


]

export const notasPaciente: Notas[] = [

    {
        class: 'bg-success',
        icon: 'bi bi-bell',
        task: 'Mirar la novela',
        time: 'Ahora'
    },
    {
        class: 'bg-success',
        icon: 'bi bi-bell',
        task: 'Tomar medicación',
        time: 'Hace 14 minutos'
    },
    {
        class: 'bg-success',
        icon: 'bi bi-bell',
        task: 'Hacer ejercicio',
        time: 'Hace 23 minutos'
    },
    {
        class: 'bg-warning',
        icon: 'bi bi-bell',
        task: 'Jugar',
        time: 'Vence en 1 hora'
    },
    {
        class: 'bg-warning',
        icon: 'bi bi-bell',
        task: 'Cenar',
        time: 'Vence en 2 hora'
    },


] 