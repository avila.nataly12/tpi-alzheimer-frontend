import { Component, Input, OnInit } from '@angular/core';
import { Notas, notasCuidador, notasPaciente } from './feeds-data';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html'
})
export class FeedsComponent implements OnInit {

  notasCuidador:Notas[];
  notasPaciente:Notas[];

  @Input()
  rol!:String;

  constructor() {

    this.notasCuidador = notasCuidador;
    this.notasPaciente = notasPaciente;
  }

  ngOnInit(): void {
  }

}
