export interface Recorrido {
    image: string,
    paciente: string,
    fRecorrido: string,
    hPartida: string,
    hLlegada: string,
}

export const TopSelling: Recorrido[] = [

    {
        image: 'assets/images/users/user1.jpg',
        paciente: 'Paciente 1',
        fRecorrido: '21/09/2022',
        hPartida: '08:00 a.m',
        hLlegada: '08:45 a.m',
    },
    {
        image: 'assets/images/users/user2.jpg',
        paciente: 'Paciente 2',
        fRecorrido: '21/09/2022',
        hPartida: '11:30 a.m',
        hLlegada: '12:15 p.m',
    },
    {
        image: 'assets/images/users/user3.jpg',
        paciente: 'Paciente 3',
        fRecorrido: '19/09/2022',
        hPartida: '9:00 a.m',
        hLlegada: '9:45 a.m',
    },
    {
        image: 'assets/images/users/user4.jpg',
        paciente: 'Paciente 4',
        fRecorrido: '18/09/2022',
        hPartida: '13:15 p.m',
        hLlegada: '14:00 p.m',
    },

]