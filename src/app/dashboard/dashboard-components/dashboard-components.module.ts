import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesSummaryComponent } from './sales-summary/sales-summary.component';
import { BlogCardsComponent } from './blog-cards/blog-cards.component';
import { PacientesComponent } from '../../pages/pacientes/pacientes.component';
import { FeedsComponent } from './feeds/feeds.component';
import { TopCardsComponent } from './top-cards/top-cards.component';
import { TopSellingComponent } from './top-selling/top-selling.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgApexchartsModule } from 'ng-apexcharts';



@NgModule({
  declarations: [
    SalesSummaryComponent,
    FeedsComponent,
    TopSellingComponent,
    TopCardsComponent,
    BlogCardsComponent
    //,    PacientesComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgApexchartsModule,
  ],
  exports: [
    SalesSummaryComponent,
    FeedsComponent,
    TopSellingComponent,
    TopCardsComponent,
    BlogCardsComponent
    //,    PacientesComponent
  ]
})
export class DashboardComponentsModule { }
