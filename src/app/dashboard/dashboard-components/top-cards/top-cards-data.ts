export interface topcard {
    bgcolor: string,
    icon: string,
    title: string,
    subtitle?: string
}

export const topcards: topcard[] = [

    {
        bgcolor: 'success',
        icon: 'bi bi-speedometer',
        title: '140/90 mm Hg',
        subtitle: 'Presión'
    },
    {
        bgcolor: 'danger',
        icon: 'bi bi-thermometer-half',
        title: '36º',
        subtitle: 'Temperatura'
    },
    {
        bgcolor: 'warning',
        icon: 'bi bi-lungs',
        title: '98%',
        subtitle: 'Oxígeno'
    },
    
]

export const SOS: topcard[] = [

    {
        bgcolor: 'danger',
        icon: 'bi bi-telephone',
        title: 'SOS',
    },
    
]