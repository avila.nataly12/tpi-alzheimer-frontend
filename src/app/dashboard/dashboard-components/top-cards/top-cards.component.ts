import { Component, Input, OnInit } from '@angular/core';
import {SOS, topcard,topcards} from './top-cards-data';
import Swal from 'sweetalert2';
import { Usuario } from '../../../users/interface/usuario';
import { AuthService } from '../../../services/auth/auth.service';
import { Contacto } from '../../../interfaces/contacto';
import { Router,ActivatedRoute } from '@angular/router'; 
import { FormBuilder, Validators } from '@angular/forms';
import { ContactoService } from '../../../services/contacto/contacto.service';


@Component({
  selector: 'app-top-cards',
  templateUrl: './top-cards.component.html'
})
export class TopCardsComponent implements OnInit {

  @Input()
  rol!: String;

  topcards: topcard[];

  sos: topcard[];
  idUsuario!:string;
  listaContacto!: Contacto[];

  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  constructor(
    private fb: FormBuilder,
     private router: Router,
    private route: ActivatedRoute, 
    private contactoService: ContactoService,
    private authService: AuthService
     ) { 

      

    this.topcards = topcards;

    this.sos = SOS;
  }
  
  ngOnInit(): void {
    this.idUsuario=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    if(this.rolUsuario=="Paciente"){
      this.router.navigate(['//juegos']);

    }
    console.log(this.rolUsuario);
    if(this.rolUsuario=="Cuidador"){
      this.router.navigate(['//pacientes/listado']);

    }

    this.contactoService.miBoton(this.idUsuario).subscribe(contactos => {
      this.listaContacto = contactos;
    }, err => {
      console.log('Error');
    });


  }


  mePerdi(){
    Swal.fire({
      icon: 'warning',
      title: 'Tranquilo, ya estamos en camino',
      showCancelButton:true,
      confirmButtonText:'Llamar SOS',
      allowOutsideClick:false
    }).then((result)=>{
      if (result.isConfirmed) {
 
 
      }
   

    })
  }

}
