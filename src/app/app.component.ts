import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Recorrido } from './interfaces/recorrido';
import { AuthService } from './services/auth/auth.service';
import { RecorridoService } from './services/recorrido/recorrido.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  
  constructor() { 
  }

}

