export interface Contacto {
    id: string,
    idPaciente: string,
    nombre: string,
    telefono: string,
    estado: boolean,
    paciente: boolean,
    cuidador: boolean
    
}