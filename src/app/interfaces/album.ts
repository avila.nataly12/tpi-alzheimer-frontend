export interface Album{
    id: string,
	nombre: string,
	idUsuarioOrigen: string,
	idUsuarioDestino: string,
	estado: boolean,
	fechaCreacion: string
}