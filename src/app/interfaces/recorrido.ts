export interface Recorrido {
    id: string,
    idUsuarioOrigen: string,
    idUsuarioDestino: string,
    estado: string,
    coordenadas: number[][],
    fechaEstimadaPartida: string,
    fechaEstimadaLlegada: string,
    horaEstimadaPartida: string,
    horaEstimadaLlegada: string
}