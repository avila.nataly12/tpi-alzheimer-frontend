export interface Juego {
    id: string,
    nombre: string,
    descripcion: string, 
    foto: string, 
    estado: boolean, 
    ruta: string
}