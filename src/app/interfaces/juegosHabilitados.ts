export interface JuegosHabilitados {
    id: string,
    idUsuario: string,
    idJuego: string,
    nivel: string
}