export interface Nota {
    id: string,
    idUsuarioOrigen: string,
    idUsuarioDestino: string,
    contenido: string,
    fechaCreacion: string,
    prioridad: number,
    estado: boolean,
    urlImagen: string,
    titulo: string
    
}