import { Usuario } from "./usuario"

export interface Cronograma {
    id: string,
    idUsuarioOrigen: string,
    idUsuarioDestino: string,
    paciente: Usuario,
    items: [{dias:any, hora:string, medicamento:string, estado:boolean}],
    estado: boolean,
    fechaCreacion: String
}