export interface Archivos {
    id: string,
    nombre: string,
    ruta: string,
    idAlbum: string,
    idUsuario: string,
    estado: boolean,
    idfirebase:string,
    idUsuarioOrigen:string,
    formato:string,
    fechaCreacion:string
}