export interface DatosSalud {
	id: string,
    idPaciente: string,
	idCuidador: string,
	presionsistolica: number,
	presiondiastolica: number,
	oxigeno: number,
	temperatura: number,
	fecha: string,
	hora: string
}