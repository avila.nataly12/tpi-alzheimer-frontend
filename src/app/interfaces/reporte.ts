export interface Reporte {
    id: string,
    idJuego: string,
    idPaciente: string,
    duracion: string,
    nivel: string,
    fecha: string,
    termino: boolean,
    fechaCreacion: string
 

}