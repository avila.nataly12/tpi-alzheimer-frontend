import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from '../../users/interface/usuario';
import { AuthService } from '../../services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { HttpContextToken } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]], // TODO: cambiar validacion por expresion regular
    contrasena: ['', [Validators.required, Validators.minLength(8)]]
  }); 

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router,
              private toastr: ToastrService
              ) { }

  ngOnInit(): void {
  }

  login() {
    const {email, contrasena} = this.loginForm.value;

    this.authService.login(email, contrasena)
      .subscribe(res => {
        localStorage.setItem('token', res.token);
        this.router.navigate(['/dashboard']);
      }, err => {
        console.log(err.error.mensaje);
        if (err.error.mensaje === "Debe verificar la cuenta para poder loguearse") {
					this.toastr.error('Debe verificar la cuenta para poder loguearse', 'Verificar cuenta');
				} else if (err.error.mensaje === "Email o contraseña incorrecta") {
					this.toastr.error('El Email o la contraseña son incorrectas', 'Email o contraseña incorrecta');
				} else if (err.error.mensaje === "Verificacion realizada con exito") {
          this.toastr.success('Verificacion realizada', 'La cuenta ha sido verificada con éxito');
        }
      });
      
  }

}
