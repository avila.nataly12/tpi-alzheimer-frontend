import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { Usuario } from '../../users/interface/usuario';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JuegosService } from '../../services/juegos/juegos.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  opcionSeleccionado: string  = '0';
  verSeleccion: string        = '';

  registerForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    apellido: ['', [Validators.required]],
    dni: ['', [Validators.required]],
    telefono: ['', [Validators.required]],
    rol: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    contrasena: ['', [Validators.required, Validators.minLength(8)]],
    direccion: ['', [Validators.required]],
    coordsdir: ['']
  });

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private toastr: ToastrService,
              private juegosService: JuegosService , 
              ) { 
              }

  ngOnInit(): void {
  }

  register() {
    const user: Usuario = this.registerForm.value;
    user.coordsdir = [0, 0];
    this.verSeleccion = this.opcionSeleccionado;

    this.authService.register( user )
      .subscribe(data => {


        this.juegosService.actualizarHabilitados("null","no",data.id,"63852d2e0085fe090189c9e2").subscribe(
          juego1 => {  
        this.juegosService.actualizarHabilitados("null","no",data.id,"63852b850085fe090189c9e1").subscribe(
          juego2 => {  

        this.toastr.success('¡El registro fue exitoso!', 'Usuario registrado');
        this.router.navigate(['/auth/login']);
        console.log(data);
        
       });
      });

      }, err => {
        console.log(err.error.mensaje);
        if (err.error.mensaje === "El email " + this.registerForm.get('email')?.value + " se encuentra registrado"){
          this.toastr.error("El Email proporcionado ya se encuentra registrado", 'Email ya registrado');
        }
      });
  }


}
