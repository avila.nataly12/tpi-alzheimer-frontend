import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-recuperar-contrasena',
  templateUrl: './recuperar-contrasena.component.html',
  styleUrls: ['./recuperar-contrasena.component.css']
})
export class RecuperarContrasenaComponent implements OnInit {

  recuperarForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]]
  }); 

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router,
              private toastr: ToastrService

              ) { }

  ngOnInit(): void {
    this.authService.listadoDeUsuarios()
      .subscribe(resp => console.log(resp));
  }

  recuperar() {
    const email = this.recuperarForm.value;

    this.authService.recuperarContrasena(email)
      .subscribe(res => {
        this.toastr.success('Nueva contraseña enviada al email');
        this.router.navigate(['/auth/login']);
      }, err => {
        console.log(err.error.mensaje);
        if (err.error.mensaje === "No existe un usuario con el mail " + this.recuperarForm.get('email')?.value){
          this.toastr.error("El Email proporcionado no está asociado a ninguna cuenta", 'Cuenta no existente');
        }
      });
      
  }

}
