import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Archivos } from '../../interfaces/archivo';

const AUTH_API = environment.baseUrl+'archivo/';
const estadoCrear = false;
const estado = false;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    }),
};


@Injectable({
    providedIn: 'root'
})
export class ArchivoService {



    private baseUrl: string = environment.baseUrl;

    constructor(private http: HttpClient) { }
/*
    verNotas(): Observable<Nota[]> {
        const url = `${this.baseUrl}nota/estado/activo`;
        return this.http.get<Nota[]>(url);
    }


      */
    cambiarEstado(id: string) {
         return this.http.post(
            AUTH_API+'cambiarestado/'+id,
            {
                id
            }
            ,
            httpOptions
        );
    }      
  

    eliminar(idArchivo: string) {
        const url = `${this.baseUrl}archivo/eliminar/`+idArchivo;
        console.log(url);
        return this.http.delete(url);
    }

    verArchivos(idUsuario: string,idAlbum:string) {
        const url = `${this.baseUrl}archivo/`+idUsuario+`/`+idAlbum;
        return this.http.get<Archivos[]>(url);
      }


      demeUnArchivo(idUsuario: string,idAlbum:string): Observable<Archivos>  {
        const url = `${this.baseUrl}archivo/uno/`+idUsuario+`/`+idAlbum;
        return this.http.get<Archivos>(url);
      }


      verFotoPerfil(nombre: string) {
        const url = `${this.baseUrl}archivo/verFotoPerfil/porUsuario/`+nombre;
        return this.http.get<Archivos>(url);
      }

    Crear(idAlbum: Object, idUsuario: Object,  nombre: string, ruta: string,idfirebase:string,idUsuarioOrigen:string,formato:string): Observable<any> {
    
        return this.http.post(
            AUTH_API,
            {
                idAlbum,
                idUsuario,
                nombre,
                ruta,
                estadoCrear,
                idfirebase,
                idUsuarioOrigen,
                formato
            }
            ,
            httpOptions
        );
    }

    verMultimedia(idUsuarioOrigen: string) {
        const url = `${this.baseUrl}archivo/verMultimedia/estado/true/`+idUsuarioOrigen;
        return this.http.get<Archivos[]>(url);
      }
  



 
}
