import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { DatosSalud } from '../../interfaces/datos-salud';

@Injectable({
  providedIn: 'root'
})
export class DatosSaludService {

  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  cargarDatosSalud(datosSalud: DatosSalud): Observable<any> {
    const url = `${this.baseUrl}datos-salud/`;

    return this.http.post(url, datosSalud);
  }

  editarDatosSalud(datosSalud: DatosSalud): Observable<DatosSalud> {
    const url = `${this.baseUrl}datos-salud/${datosSalud.id}`;

    return this.http.put<DatosSalud>(url, datosSalud);
  }

  verDatosSaludPaciente(idPaciente: string): Observable<DatosSalud[]> {
    const url = `${this.baseUrl}datos-salud/paciente/${idPaciente}`;

    return this.http.get<DatosSalud[]>(url);
  }


  verDatosSaludPacienteFechas(idPaciente: string,desde: string,hasta: string): Observable<DatosSalud[]> {
    const url = `${this.baseUrl}datos-salud/paciente/fechas/${idPaciente}/${desde}/${hasta}`;

    return this.http.get<DatosSalud[]>(url);
  }

  verDatoSalud(idDatoSalud: string): Observable<DatosSalud> {
    const url = `${this.baseUrl}datos-salud/${idDatoSalud}`;

    return this.http.get<DatosSalud>(url);
  }
}
