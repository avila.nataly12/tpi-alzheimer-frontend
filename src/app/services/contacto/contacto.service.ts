import { Injectable } from '@angular/core';
import { Contacto } from '../../interfaces/contacto';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';
const AUTH_API = environment.baseUrl+'contacto/';
const AUTH_API_WHATSAPP = environment.baseUrl+'whatsapp/';

const estadoCrear = true;
const estado = true;
const estadoEliminar = false;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    }),
};


@Injectable({
    providedIn: 'root'
})
export class ContactoService {
 

    private baseUrl: string = environment.baseUrl;

    constructor(private http: HttpClient) { }

    getContacto(idContacto: string): Observable<Contacto> {
        const url = `${this.baseUrl}contacto/getContacto/`+idContacto;
        return this.http.get<Contacto>(url);
    }


    verContactos(idUsuario: string): Observable<Contacto[]> {
        const url = `${this.baseUrl}contacto/usuario/`+idUsuario;
        return this.http.get<Contacto[]>(url);
    }

    buscarOrigen(idUsuario: string,idPaciente: string): Observable<Contacto[]> {
        const url = `${this.baseUrl}contacto/origen/`+idUsuario+`/`+idPaciente;
        return this.http.get<Contacto[]>(url);
      }
  


      
    listadoOrigen(idUsuario: string): Observable<Contacto[]> {
        const url = `${this.baseUrl}contacto/origen/listado/boton/`+idUsuario;
        return this.http.get<Contacto[]>(url);
      }
  

      listadoOrigenCuidador(idUsuario: string): Observable<Contacto[]> {
        const url = `${this.baseUrl}contacto/origen/listado/boton/cuidador/`+idUsuario;
        return this.http.get<Contacto[]>(url);
      }
  

      contactosCuidadoresPorPaciente(idUsuario: string): Observable<Contacto[]> {
        const url = `${this.baseUrl}contacto/origen/listado/boton/paciente/`+idUsuario;
        return this.http.get<Contacto[]>(url);
      }
  

      miBoton(idPaciente: string) {
        const url = `${this.baseUrl}contacto/boton/`+idPaciente;
        return this.http.get<Contacto[]>(url);
      }
  
  

    Crear(idPaciente: Object, nombre: Object, telefono: string,paciente: Boolean, cuidador: Boolean,idOrigen:string): Observable<any> {
        return this.http.post(
            AUTH_API+'crear',
            {
                idPaciente,
                nombre,
                telefono,
                estadoCrear,
                paciente,
                cuidador,
                idOrigen
            },
            httpOptions
        );
    }


    
    EnviarWhatsapp(nombre:string, telefono:string, mensaje: string): Observable<any> {
        return this.http.post(
            AUTH_API_WHATSAPP+'enviar',
            {
                nombre,
                telefono,
                mensaje
            },
            httpOptions
        );
    }

/*
router.post('/crear', contactoPost);
router.get('/usuario/:idPaciente', contactoUsuarios);
router.put('/editar/:id', contactoPut);
 
*/
     


    Editar(id: Object, nombre: string, telefono: string,estado:string,paciente: Boolean, cuidador: Boolean): Observable<any> {
        return this.http.put(
            AUTH_API+'editar/'+id,
            {
                nombre,
                telefono,
                estado,
                paciente,
                cuidador
            },
            httpOptions
        );
    }


    Eliminar(id: any) {
        return this.http.put(
            AUTH_API+'editar/'+id,
            {
                estado:false
            },
            httpOptions
        );     
     }


 
  


 


}
