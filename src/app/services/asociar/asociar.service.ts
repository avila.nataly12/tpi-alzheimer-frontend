import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../../users/interface/usuario';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AsociarService {

  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  asociarPaciente(idCuidador: string, dniPaciente: string): Observable<any> {
    const url = `${this.baseUrl}usuario/relacionar-cuidador-paciente`;
    const body = { idCuidador, dniPaciente }

    return this.http.post(url, body);

  }

  verPacientesAsociados(id: string): Observable<Usuario[]> {
    const url = `${this.baseUrl}usuario/listar-pacientes-de-cuidador/${id}`;

    return this.http.get<Usuario[]>(url);
  }

  obtenerCuidadorAsociado(id: string): Observable<Usuario> {
    const url = `${this.baseUrl}usuario/${id}`;

    return this.http.get<Usuario>(url);
  }

}
