import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../../users/interface/usuario';
import { Swpush } from '../../interfaces/swpush';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {




  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient,
    private router: Router) { }

  register(user: Usuario): Observable<Usuario> {
    const url = `${this.baseUrl}usuario/`;

    return this.http.post<Usuario>(url, user);
  }


  save(clave: string,idUsuario :string): Observable<Swpush> {
    const url = `${this.baseUrl}usuario/notificaciones/save/swpush`;
    const body = { clave, idUsuario };

    return this.http.post<Swpush>(url, body);
  }


  send(idUsuario :string,cuerpo :string,titulo :string,link:any): Observable<Swpush> {
    const url = `${this.baseUrl}usuario/notificaciones/send/swpush`;
    const body = { idUsuario , cuerpo, titulo , link };

    return this.http.post<Swpush>(url, body);
  }

  verifico(idUsuario: any): Observable<Swpush> {
    const url = `${this.baseUrl}usuario/notificaciones/verifico/swpush/`+idUsuario;
 
    return this.http.get<Swpush>(url);
  }



  listadoDePacientesPorCuidador(idCuidador:string){
    const url = `${this.baseUrl}usuario/listar-pacientes-de-cuidador/`+idCuidador;
    return this.http.get<Usuario[]>(url);
  }

  login(email: string, contrasena: string): Observable<any> {
    const url = `${this.baseUrl}usuario/login`;
    const body = { email, contrasena };

    return this.http.post(url, body);
  }

  listadoDeUsuarios(): Observable<Usuario[]> {
    const url = `${this.baseUrl}usuario`;

    return this.http.get<Usuario[]>(url);
  }

  getUsuarioPorId(id: string): Observable<Usuario> {
    const url = `${this.baseUrl}usuario/${id}`;

    return this.http.get<Usuario>(url);
  }

  dameUsuarioPorDni(dniPaciente: any):Observable<Usuario> {
    const url = `${this.baseUrl}usuario/dni/${dniPaciente}`;
    return this.http.get<Usuario>(url);  
  }



  getToken() {
    return localStorage.getItem('token');
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  getDecodedAccessToken(token: string): any {
    try {

      return jwt_decode(token);
    } catch (error) {

      return null;
    }
    
  }

  recuperarContrasena(email: string): Observable<any> {
    const url = `${this.baseUrl}usuario/recuperar-contrasena`;
    const body = email;

    return this.http.post(url, body);
  }
 


}
