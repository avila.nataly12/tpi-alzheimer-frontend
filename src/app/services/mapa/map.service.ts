import { Injectable } from '@angular/core';
import { Feature, PlacesResponse } from '../../pages/mapa/interfaces/places';
import { AnySourceData, LngLatBounds, LngLatLike, Map, Marker, Popup } from 'mapbox-gl';
import { DirectionsApiClient } from '../../api';
import { DirectionsResponse, Route } from '../../pages/mapa/interfaces/directions';
import { PlacesApiClient } from '../../api/placesapiclient';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private map?: Map;

  private markers: Marker[] = [];

  userLocation!: [number, number]; // Puede dar error

  isLoadingPlaces: boolean = false;

  places: Feature[] = [];

  coords!: number[][];

  get isUserLocationReady(): boolean {
    return !!this.userLocation;
  }

  get isMapReady() {
    return !!this.map;
  }

  get coordinates() {
    return this.coords;
  }

  constructor(private placesApi: PlacesApiClient, 
    private directionsApi: DirectionsApiClient,
    private http: HttpClient
    ) { 
    this.getUserLocation();
  }

  setMap(map: Map) {
    this.map = map;
  }

  flyTo(coords: LngLatLike) {
    if (!this.isMapReady) {
      throw new Error('El mapa no esta inicializado');
    }

    this.map?.flyTo({
      zoom: 14,
      center: coords
    });
  }

  async getUserLocation(): Promise<[number, number]> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        ({ coords }) => {
          this.userLocation = [coords.longitude, coords.latitude];
          console.log(this.userLocation);
          resolve(this.userLocation);
        },
        (err) => {
          alert(err);
          console.log('No se pudo obtener geolocalización');
          reject();
        }
      );
    });
  }

  createMarkersFromPlaces(places: Feature[], userLocation: [number, number]) {
    if (!this.map) throw Error('Mapa no inicializado');

    this.markers.forEach(marker => marker.remove());

    const newMarkers = [];

    for (const place of places) {
      const [lng, lat] = place.center;
      const popup = new Popup()
        .setHTML(`
          <h6>${place.text}</h6>
          <span>${place.place_name}</span>
        `);
      const newMarker = new Marker()
        .setLngLat([lng, lat])
        .setPopup(popup)
        .addTo(this.map);

      newMarkers.push(newMarker)
    }
    this.markers = newMarkers;

    if (places.length === 0) return;

    //Limites del mapa
    const bounds = new LngLatBounds();
    newMarkers.forEach(marker => bounds.extend(marker.getLngLat()));
    bounds.extend(userLocation);

    this.map.fitBounds(bounds, {
      padding: 200
    });
  }

  getRouteBetweenPoints(start?: [number, number], end?: [number, number]) {
    this.directionsApi.get<DirectionsResponse>(`/${start?.join(',')};${end?.join(',')}`)
      .subscribe(resp => this.drawPolyLine(resp.routes[0]));

  }

  drawPolyLineCoordenadas(coordenadas: number[][]) {

    if (!this.map) throw Error('Mapa no inicilizado');

    const coords = coordenadas;

    const bounds = new LngLatBounds();
    coords.forEach(([lng, lat]) => {
      bounds.extend([lng, lat])
    });

    this.map?.fitBounds(bounds, {
      padding: 200
    });

    // Polyline
    const sourceData: AnySourceData = {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'LineString',
              coordinates: coords
            }
          }
        ]
      }
    }

    // Limpiar ruta previa para redibujar
    if (this.map.getLayer('RouteString')) {
      this.map.removeLayer('RouteString');
      this.map.removeSource('RouteString');
    }

    this.map.addSource('RouteString', sourceData);

    this.map.addLayer({
      id: 'RouteString',
      type: 'line',
      source: 'RouteString',
      layout: {
        'line-cap': 'round',
        'line-join': 'round',
      },
      paint: {
        'line-color': 'black',
        'line-width': 3
      }
    });

  }

  private drawPolyLine(route: Route) {
    console.log({ kms: route.distance / 1000, durarion: route.duration / 60 });

    if (!this.map) throw Error('Mapa no inicilizado');

    const coords = route.geometry.coordinates;

    this.coords = coords;

    const bounds = new LngLatBounds();
    coords.forEach(([lng, lat]) => {
      bounds.extend([lng, lat])
    });

    this.map?.fitBounds(bounds, {
      padding: 200
    })

    // Polyline
    const sourceData: AnySourceData = {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'LineString',
              coordinates: coords
            }
          }
        ]
      }
    }

    // Limpiar ruta previa para redibujar
    if (this.map.getLayer('RouteString')) {
      this.map.removeLayer('RouteString');
      this.map.removeSource('RouteString');
    }

    this.map.addSource('RouteString', sourceData);

    this.map.addLayer({
      id: 'RouteString',
      type: 'line',
      source: 'RouteString',
      layout: {
        'line-cap': 'round',
        'line-join': 'round',
      },
      paint: {
        'line-color': 'black',
        'line-width': 3
      }
    });

  }

  getPlacesByQuery(query: string = '') {
    if (query.length === 0) {
      this.places = [];
      this.isLoadingPlaces = false;
      return;
    }

    if (!this.userLocation) throw Error('No hay user location');
    this.isLoadingPlaces = true;

    this.placesApi.get<PlacesResponse>(`/${query}.json`, {
      params: {
        proximity: this.userLocation.join(',')
      }
    })
      .subscribe(resp => {
        this.isLoadingPlaces = false;
        this.places = resp.features;

        this.createMarkersFromPlaces(this.places, this.userLocation!);
      });
  }

  deletePlaces() {
    this.places = [];
  }

  geocoding(long: number, lat: number): Observable<any> {
    const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${long},${lat}.json?access_token=pk.eyJ1IjoibmF0YXZpbGEiLCJhIjoiY2w4YjN5Ymk1MGhlajN2bHlmdnI3ZGg3ZyJ9.l6rNDRSpojQpphQXH-GYLA`;
    
    return this.http.get(url);
  }

}