import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Album } from '../../interfaces/album';

@Injectable({
  providedIn: 'root'
})
export class GaleriaService {

  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  crearAlbum(album: Album): Observable<any> {
    const url = `${this.baseUrl}album/`;

    return this.http.post(url, album);
  }

  editarAlbum(id: string, nombre:string): Observable<any> {
    const url = `${this.baseUrl}album/${id}`;
    const body = { nombre };

    return this.http.put(url, body);
  }

  eliminarAlbum(id: string,estado:string): Observable<any> {
    const url = `${this.baseUrl}album/${id}`;
    const body = {  estado };

    return this.http.put(url, body);
  }

  verGaleriaPaciente(idPaciente: string): Observable<Album[]> {
    const url = `${this.baseUrl}album/paciente/${idPaciente}`;

    return this.http.get<Album[]>(url);
  }

  verGaleriaCuidador(idCuidador: string): Observable<Album[]> {
    const url = `${this.baseUrl}album/cuidador/${idCuidador}`;

    return this.http.get<Album[]>(url);
  }

  verAlbum(idAlbum: string): Observable<Album> {
    const url = `${this.baseUrl}album/${idAlbum}`;

    return this.http.get<Album>(url);
  }
}
