import { Injectable } from '@angular/core';
import { Nota } from '../../interfaces/nota';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Quesoy } from '../../interfaces/quesoy';
import { Reporte } from '../../interfaces/reporte';
import { Juego } from '../../interfaces/juego';
import { JuegosHabilitados } from '../../interfaces/juegosHabilitados';

 
//const AUTH_API = 'http://localhost:3000/juegos/';
const AUTH_API = environment.baseUrl+'juegos/';
const AUTH_API_QUESOY = environment.baseUrl+'quesoy/';

const  estadoCrear=true;
const  estado=false;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  }),
};


@Injectable({
  providedIn: 'root'
})
export class JuegosService {


 

  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }
/*
  verJuegos():Observable<Juego[]> {
    const url = `${this.baseUrl}juego/`;
    return this.http.get<Juego[]>(url);
  }
 */

  verJuegosQueSoy():Observable<Quesoy[]>  {
     return this.http.get<Quesoy[]>(AUTH_API_QUESOY+'list');
  }



  verJuegosMemoria(nivel: string) {
 
       return this.http.get<Quesoy[]>(AUTH_API_QUESOY+'list/nivel/'+nivel);

}



  
  verTodos() {
    return this.http.get<Juego[]>(AUTH_API+'list');
 }


   
 dameJuego(idJuego:string) {
  return this.http.get<Juego[]>(AUTH_API+'/dameJuego/'+idJuego);
}

   
dameUnJuego(idJuego:string) {
  return this.http.get<Juego>(AUTH_API+'dame/Juego/'+idJuego);
}

 
 verHabilitados(idUsuario:string) {
  return this.http.get<JuegosHabilitados[]>(AUTH_API+'habilitados/'+idUsuario);
}



actualizarHabilitados(idHabilitado: any, nivel: any,idUsuario: any, idJuego: any) {

  return this.http.post(
    AUTH_API,
    {
      idHabilitado,
      nivel,
      idUsuario,
      idJuego 
    }
    ,
    httpOptions
);

}
   
dameReporte(idJuego:string,idPaciente:string):Observable<Reporte[]>  {

  return this.http.get<Reporte[]>(AUTH_API+'/list/reporte/'+idJuego+'/'+idPaciente);
}


dameReportePorFechas(desde: string, hasta: string, idJuego: string, idPaciente: string) {
  return this.http.get<Reporte[]>(AUTH_API+'/list/reportePorFecha/'+idJuego+'/'+idPaciente+'/'+desde+'/'+hasta);
}






guardarReporte(idJuego: string, idPaciente: any, duracion: number, nivel: string, fecha: string, termino: boolean) {
 
  return this.http.post(
    AUTH_API+'reporte',
    {
      idJuego,
      idPaciente,
      duracion,
      nivel,
      fecha,
      termino 
    }
    ,
    httpOptions
);
}



}
