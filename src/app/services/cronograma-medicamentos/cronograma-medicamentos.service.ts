import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cronograma } from '../../interfaces/cronograma-medicamento';

@Injectable({
  providedIn: 'root'
})
export class CronogramaMedicamentosService {

  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  verCronogramas(): Observable<Cronograma[]> {
    const url = `${this.baseUrl}cronograma-medicamentos/`;

    return this.http.get<Cronograma[]>(url);
}

  verCronograma(id:string) : Observable<Cronograma> {
    const url = `${this.baseUrl}cronograma-medicamentos/${id}`;

    return this.http.get<Cronograma>(url);
  }

  verCronogramaDePaciente(idUsuario: string): Observable<Cronograma[]> {
    const url = `${this.baseUrl}cronograma-medicamentos/paciente/${idUsuario}`;

    return this.http.get<Cronograma[]>(url);
  }

  verCronogramasDeCuidador(idUsuario: string): Observable<Cronograma[]> {
    const url = `${this.baseUrl}cronograma-medicamentos/cuidador/${idUsuario}`;

    return this.http.get<Cronograma[]>(url);
  }

  Crear(cronograma:Cronograma): Observable<any> {
  const url = `${this.baseUrl}cronograma-medicamentos/`;

  return this.http.post(url, cronograma);
}

  agregarMedicacion(id:string, items:[] ): Observable<any> {
  const url = `${this.baseUrl}cronograma-medicamentos/${id}`;
  const body = {id, items};

  return this.http.put(url, body);
  }

  eliminarMedicacion(id:string, posicionItem:number): Observable<any> {
    const url = `${this.baseUrl}cronograma-medicamentos/${id}/eliminarMedicamento`;
    const body = {id, posicionItem};
  
    return this.http.put(url, body);
    }
}
