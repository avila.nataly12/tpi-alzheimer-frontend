import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Recorrido } from '../../interfaces/recorrido';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UltimaUbicacion } from '../../interfaces/ultima-ubicacion';

@Injectable({
  providedIn: 'root'
})
export class RecorridoService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  crearRecorrido(recorrido: Recorrido): Observable<Recorrido> {
    const url = `${this.baseUrl}recorrido`;

    return this.http.post<Recorrido>(url, recorrido);
  }

  listarRecorridos(): Observable<Recorrido[]> {
    const url = `${this.baseUrl}recorrido`;

    return this.http.get<Recorrido[]>(url);
  }

  listarRecorridoPorIdUsuario(idPaciente: string): Observable<Recorrido[]> {
    const url = `${this.baseUrl}recorrido/listar/${idPaciente}`;

    return this.http.get<Recorrido[]>(url);
  }

  editarEstadoRecorrido(idRecorrido: string, estado: string): Observable<Recorrido> {
    const url = `${this.baseUrl}recorrido/${idRecorrido}`;
    const body = {idRecorrido, estado};

    return this.http.put<Recorrido>(url, body);
  }

  editarUltimaUbicacion(idPaciente: string, ubicacion: [number, number]): Observable<any> {
    const url = `${this.baseUrl}ultima-ubicacion/${idPaciente}`;
    const body = {idPaciente, ubicacion};

    return this.http.put(url, body);
  }

  listarUltimaUbicacionPorIdPaciente(idPaciente: string): Observable<UltimaUbicacion> {
    const url = `${this.baseUrl}ultima-ubicacion/${idPaciente}`;

    return this.http.get<UltimaUbicacion>(url)
  }

}
