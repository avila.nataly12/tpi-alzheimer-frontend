import { ComponentFactoryResolver, Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { getStorage, ref, uploadBytesResumable, getDownloadURL, deleteObject } from "firebase/storage";
import { ImagenesModel } from '../models/imagenes.model';
import { FileItems } from '../models/file.items';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { ArchivoService } from '../services/archivos/archivos.service';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ImagenesService {

private CARPETA_IMAGENES = 'img';
idUsuario:string;

private imagenesCollection : AngularFirestoreCollection<ImagenesModel>

progress:any;

  constructor(private db:AngularFirestore,     
    private archivoService: ArchivoService,
    private authService: AuthService,
    ) { 
      this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
      this.CARPETA_IMAGENES=this.idUsuario;
    this.imagenesCollection = db.collection<ImagenesModel>(this.idUsuario);

  }


  getImagenes(idUsuario:string):Observable<ImagenesModel[]> {
    this.imagenesCollection = this.db.collection<ImagenesModel>(idUsuario);


    return this.imagenesCollection.snapshotChanges().pipe(

      map( actions => actions.map(a => {

        const data = a.payload.doc.data() as ImagenesModel;
        const id = a.payload.doc.id;
        const nombre = data.nombreImagen;

        return {id,nombre, ...data}

      })


      )

    )

  }


  getImagen(id:any,idUsuario:any){
    this.imagenesCollection = this.db.collection<ImagenesModel>(idUsuario);

    return this.imagenesCollection.doc(id).valueChanges();

  }


  cargarImagenesFirebase(imagen:FileItems, imagesData:ImagenesModel){

    const storage = getStorage();

    let item = imagen;

    let imagenTrim = imagesData.nombreImagen;

    const storageRef = ref(storage, `${this.CARPETA_IMAGENES}/${imagenTrim.replace(/ /g, '')}`);

    const uploadTask = uploadBytesResumable(storageRef,item.archivo);

    uploadTask.on('state_changed', (snapshot) =>{

      this.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

      console.log(this.progress);


    },(err)=>{
      console.log('Error al subir archivo', err);
    },()=>{
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL)=>{

        item.url = downloadURL;
        this.guardarImagen({

          nombreImagen:imagesData.nombreImagen,
          imgUrl:item.url

        });

      });
    }
    
    )


  }



  
  cargarImagenesFirebase2(imagen:FileItems, imagesData:ImagenesModel,idAlbum:string, idUsuario:string, idUsuarioOrigen:string, tipoarchivo:string){

    const storage = getStorage();

    let item = imagen;

    let imagenTrim = imagesData.nombreImagen;
//aca tengo que diferenciar carpetas
    const storageRef = ref(storage, `${idUsuario}/${imagenTrim.replace(/ /g, '')}`);

    const uploadTask = uploadBytesResumable(storageRef,item.archivo);

    uploadTask.on('state_changed', (snapshot) =>{

      this.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

      console.log(this.progress);


    },(err)=>{
      console.log('Error al subir archivo', err);
    },()=>{
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL)=>{

        item.url = downloadURL;
        this.guardarImagen2({ 
          nombreImagen:imagesData.nombreImagen,
          imgUrl:item.url,
          idUsuario:idUsuario,
        },idAlbum,idUsuario,imagesData.id,idUsuarioOrigen,tipoarchivo);



        

      });
    }
    
    )


  }


  async guardarImagen(imagen:{nombreImagen:string, imgUrl:string}):Promise<any>{


    try {

      return await this.db.collection(this.idUsuario).add(imagen);
      
    } catch (err) {

      console.log(err);
      
    }


  }

  
  async guardarImagen2(imagen:{nombreImagen:string, imgUrl:string, idUsuario:string},idAlbum:string,idUsuario:string,imagesDataID:any,idUsuarioOrigen:string,tipoarchivo:string):Promise<any>{


    try {

      
      this.archivoService.Crear(idAlbum,idUsuario,imagen.nombreImagen,imagen.imgUrl,imagesDataID,idUsuarioOrigen,tipoarchivo)
      .subscribe(data => {
      }, err => {
        console.log('Error');
      });
      if(idUsuario=="fotoDePeril"){
      }else{
      return await this.db.collection(idUsuario).add(imagen);
      }
    } catch (err) {

      console.log(err);
      
    }


  }


  public eliminarImagen(id:string, imagenNombre:string){

    const storage = getStorage();

    const deleteImg = ref(storage, `${this.CARPETA_IMAGENES}/${imagenNombre.replace(/ /g, '')}`);


    deleteObject(deleteImg).then(()=>{

      Swal.fire('EXITO', 'El registro se elimino correctamente', 'success');

    }).catch((err)=>{

      console.error(err);

    });

    return this.imagenesCollection.doc(id).delete();
    
  }
  


}
