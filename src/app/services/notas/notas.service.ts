import { Injectable } from '@angular/core';
import { Nota } from '../../interfaces/nota';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';
//const AUTH_API = 'http://localhost:3000/nota/';
const AUTH_API = environment.baseUrl+'nota/';

const estadoCrear = true;
const estado = false;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    }),
};


@Injectable({
    providedIn: 'root'
})
export class NotasService {


    private baseUrl: string = environment.baseUrl;

    constructor(private http: HttpClient) { }

    verNotas(): Observable<Nota[]> {
        const url = `${this.baseUrl}nota/estado/activo`;
        return this.http.get<Nota[]>(url);
    }

    verNotasUsuarios(idUsuario: string) {
        const url = `${this.baseUrl}nota/usuario/`+idUsuario;
        return this.http.get<Nota[]>(url);
      }


    Crear(idUsuarioOrigen: Object, idUsuarioDestino: Object, titulo: string, contenido: string, prioridad: string,): Observable<any> {
        return this.http.post(
            AUTH_API,
            {
                idUsuarioOrigen,
                idUsuarioDestino,
                contenido,
                prioridad,
                estadoCrear,
                titulo
            }
            ,
            httpOptions
        );
    }


    Eliminar(id: string): Observable<any> {
        return this.http.put(
            AUTH_API + id,
            {
                estado
            }
            ,
            httpOptions
        );
    }



}
