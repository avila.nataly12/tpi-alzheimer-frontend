import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatosSaludService } from '../../services/datos-salud/datos-salud.service';
import { DatosSalud } from '../../interfaces/datos-salud';
import { Usuario } from '../../interfaces/usuario';
import { AuthService } from '../../services/auth/auth.service';
import Chart from 'chart.js/auto';
import { __exportStar } from 'tslib';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-datos-salud',
  templateUrl: './datos-salud.component.html'
})
export class DatosSaludComponent implements OnInit {

  form: any = {
    desde: null,
    hasta: null
  };

  hoy!:any;

  idPaciente = this.route.snapshot.paramMap.get('idPaciente')!;
  paciente!: Usuario;
  datosSalud!: DatosSalud[];
  public chart: any;
  public chart2: any;
  public chart3: any;
  public fechas: string[] = [];
  public oxigeno: string[] = [];
  public presionMinima: number[] = [];
  public presionMaxima: number[] = [];
  public temperatura: string[] = [];
  public cantidadDias: number;
  constructor(private route: ActivatedRoute,
    private authService: AuthService,
    private datosSalusService: DatosSaludService) {
    this.cantidadDias = 0;

    let date: Date = new Date();
    const mes =date.getMonth()+1;
   this.hoy=date.getFullYear() + "-" + mes + "-" + date.getDate();

   
  }

  ngOnInit(): void {
    this.datosSalusService.verDatosSaludPaciente(this.idPaciente)
      .subscribe(data => {
        if (data.length > 0) {
          this.datosSalud = data;

          data.forEach((elemento) => {

         //   this.fechas.push(elemento.fecha);
         
            this.fechas.push(new Date(elemento.fecha+"T00:00:00").toLocaleDateString('es-AR', {year:"numeric", month:"short", day:"numeric"}));

            this.oxigeno.push(elemento.oxigeno.toString());
            //var presionCompleta = elemento.presion.toString();
            //var presionSeparada=presionCompleta.split('/');

            this.presionMinima.push(elemento.presionsistolica);
            this.presionMaxima.push(elemento.presiondiastolica);

            this.temperatura.push(elemento.temperatura.toString());

          });
          this.cantidadDias = data.length;
          this.graficoOxigeno();
          this.graficoTemperatura();
          this.graficoPresion();

        } else {
        }
      });

    this.authService.getUsuarioPorId(this.idPaciente)
      .subscribe(data => {
        this.paciente = data;
      });
  }


  graficoOxigeno() {
    if (this.chart) {
      this.chart.destroy();
    }
    this.chart = new Chart("oxigeno", {
      type: 'bar', //this denotes tha type of chart

      data: {// values on X-Axis
        labels: this.fechas,
        datasets: [
          {
            label: "Oxígeno mm Hg",
            data: this.oxigeno,
            backgroundColor: '#7460ee'
          }
        ]
      },
      options: {
        aspectRatio: 2.5,
      }

    });



  }


  graficoTemperatura() {

    if (this.chart2) {
      this.chart2.destroy();
    }

    this.chart2 = new Chart("temperatura", {
      type: 'bar', //this denotes tha type of chart

      data: {// values on X-Axis
        labels: this.fechas,
        datasets: [
          {
            label: "Temperatura C°",
            data: this.temperatura,
            backgroundColor: '#009688'
          }
        ]
      },
      options: {
        aspectRatio: 2.5
      }

    });



  }


  graficoPresion() {

    if (this.chart3) {
      this.chart3.destroy();
    }


    this.chart3 = new Chart("presion", {
      type: 'bar', //this denotes tha type of chart

      data: {// values on X-Axis
        labels: this.fechas,
        datasets: [

          {
            label: "Mínima ",
            data: this.presionMinima,
            backgroundColor: '#2196f3'
            ,
          },
          {

            label: "Máxima ",
            data: this.presionMaxima,
            backgroundColor: '#f44336'
          }
        ]
      },
      options: {

        aspectRatio: 2,

        responsive: true,
        plugins: {
          title: {
            display: true,
            text: 'Presión arterial'
          }, subtitle: {
            display: true,
            text: 'Cantidad de días: ' + this.cantidadDias
          }
        }
      }

    });



  }

  exportar(divName: any) {
    window.print();

  }

  onSubmit(): void {
    var { desde, hasta } = this.form;

    if(desde!=null && hasta!=null){

    this.fechas = [];
    this.oxigeno = [];
    this.presionMinima = [];
    this.presionMaxima = [];
    this.temperatura = [];
    this.cantidadDias = 0;



    this.datosSalusService.verDatosSaludPacienteFechas(this.idPaciente, desde, hasta)
      .subscribe(data => {
        if (data.length > 0) {
          this.datosSalud = data;

          data.forEach((elemento) => {
            this.fechas.push(new Date(elemento.fecha+"T00:00:00").toLocaleDateString('es-es', {year:"numeric", month:"short", day:"numeric"}));

          //  this.fechas.push(elemento.fecha);
            this.oxigeno.push(elemento.oxigeno.toString());
            //var presionCompleta = elemento.presion.toString();
            //var presionSeparada=presionCompleta.split('/');

            this.presionMinima.push(elemento.presionsistolica);
            this.presionMaxima.push(elemento.presiondiastolica);

            this.temperatura.push(elemento.temperatura.toString());

          });
          this.cantidadDias = data.length;
          this.graficoOxigeno();
          this.graficoTemperatura();
          this.graficoPresion();

        } else {
        }
      });




       }else{
        Swal.fire({
          icon: 'error',
          title: 'Debe completar todos los campos',
          showCancelButton:true,
          confirmButtonText:'Ok',
          cancelButtonText:'Cerrar',
          allowOutsideClick:false
        }).then((result)=>{
      
       
        });

       }



    this.authService.getUsuarioPorId(this.idPaciente)
      .subscribe(data => {
        this.paciente = data;
      });




  }



}
