import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';
import { AuthService } from '../../../services/auth/auth.service';
import { DatosSalud } from '../../../interfaces/datos-salud';
import { DatosSaludService } from '../../../services/datos-salud/datos-salud.service';

@Component({
  selector: 'app-cargar-datos-salud',
  templateUrl: './cargar-datos-salud.component.html'
})
export class CargarDatosSaludComponent implements OnInit {

  datosSalud!: DatosSalud;

  datosSaludEdit!: DatosSalud;

  idPacienteParam = this.route.snapshot.paramMap.get('idPaciente')!;

  idPaciente!: string;

  idDatoSalud = this.route.snapshot.paramMap.get('id')!;

  idCuidador = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

  mensajepresion!: string;

  mensajepresionSistolica!: string;

  mensajepresionDiastolica!: string;

  mensajetemperatura!: string;

  mensajeoxigeno!: string;

  hoy!: any;

  formDatosSalud: FormGroup = this.fb.group({
    idCuidador: [''],
    idPaciente: [''],
    presionsistolica: ['', [Validators.required]],
    presiondiastolica: ['', [Validators.required]],
    temperatura: ['', [Validators.required, Validators.min(34), Validators.max(45)]],
    oxigeno: ['', [Validators.required, Validators.max(100)]],
    fecha: ['', [Validators.required]],
    hora: ['', [Validators.required]]
  });

  constructor(private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private datosSaludService: DatosSaludService,
    private _location: Location,
  ) {

    let date: Date = new Date();
    const mes = date.getMonth() + 1;
    this.hoy = date.getFullYear() + "-" + mes + "-" + date.getDate();


  }

  ngOnInit(): void {
    if (this.router.url === `/datos-salud/editar-datos-salud/${this.idDatoSalud}`) {
      this.datosSaludService.verDatoSalud(this.idDatoSalud)
        .subscribe(data => {
          this.datosSalud = data;
          console.log(data);
          this.formDatosSalud.controls['presionsistolica'].setValue(data.presionsistolica);
          this.formDatosSalud.controls['presiondiastolica'].setValue(data.presiondiastolica);
          this.formDatosSalud.controls['oxigeno'].setValue(data.oxigeno);
          this.formDatosSalud.controls['temperatura'].setValue(data.temperatura);
          this.formDatosSalud.controls['fecha'].setValue(data.fecha);
          this.formDatosSalud.controls['hora'].setValue(data.hora);

        }, error => {
          console.log("Error");
        });
    }
  }

  sendDatosSalud() {
    if (this.router.url === `/datos-salud/cargar-datos-salud/${this.idPacienteParam}`) {
      console.log('Pasa por registrar');
      this.datosSalud = this.formDatosSalud.value;

      this.datosSalud.idCuidador = this.idCuidador;
      this.datosSalud.idPaciente = this.idPacienteParam;

      // TODO: cambiar el formato de la fecha a DD/MM/AAAA
      this.mensajepresion = this.datosSalud.presionsistolica + "/" + this.datosSalud.presiondiastolica;

      if (this.datosSalud.presionsistolica <= 90) {
        console.log("Presion baja");
        this.mensajepresionSistolica = 'baja';
      }

      if (this.datosSalud.presiondiastolica <= 60) {
        console.log("Presion baja");
        this.mensajepresionDiastolica = 'baja';
      }

      if (this.datosSalud.presionsistolica >= 90 && this.datosSalud.presionsistolica <= 120) {
        console.log("Presion normal");
        this.mensajepresionSistolica = 'normal';
      }

      if (this.datosSalud.presiondiastolica >= 60 && this.datosSalud.presiondiastolica <= 80) {
        console.log("Presion normal");
        this.mensajepresionDiastolica = 'normal';
      }

      if (this.datosSalud.presionsistolica > 120 && this.datosSalud.presionsistolica < 129) {
        console.log("Presion elevada");
        this.mensajepresionSistolica = 'elevada';
      }

      if (this.datosSalud.presiondiastolica < 60) {
        console.log("Presion elevada");
        this.mensajepresionDiastolica = 'elevada';
      }

      if (this.datosSalud.presionsistolica > 130 && this.datosSalud.presionsistolica < 139) {
        console.log("Presion alta hipertension");
        this.mensajepresionDiastolica = 'alta - Hipertensión';
      }

      if (this.datosSalud.presiondiastolica > 80 && this.datosSalud.presiondiastolica < 89) {
        console.log("Presion alta hipertension");
        this.mensajepresionDiastolica = 'alta - Hipertensión';
      }

      if (this.datosSalud.temperatura < 36) {
        this.mensajetemperatura = 'baja';
      }

      if (this.datosSalud.temperatura > 37) {
        this.mensajetemperatura = 'alta';
      }

      if (this.datosSalud.temperatura >= 36 && this.datosSalud.temperatura <= 37) {
        this.mensajetemperatura = 'normal'
      }

      if (this.datosSalud.oxigeno < 95) {
        this.mensajeoxigeno = 'bajo'
      }

      if (this.datosSalud.oxigeno >= 95) {
        this.mensajeoxigeno = 'normal'
      }

      if (this.mensajepresionSistolica === 'normal' && this.mensajepresionDiastolica === 'normal' && this.mensajetemperatura === 'normal' && this.mensajeoxigeno === 'normal') {
        Swal.fire({
          icon: 'success',
          title: 'El paciente se encuentra estable',
          text: `Presión: ${this.mensajepresion}. 
                  Oxígeno: ${this.mensajeoxigeno}.
                  Temperatura: ${this.mensajetemperatura}.
                  `,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        }).then((result) => {
          if (result.isConfirmed) {

            this.datosSaludService.cargarDatosSalud(this.datosSalud)
              .subscribe(data => {
                this.router.navigate([`/datos-salud/historial-salud/${this.idPacienteParam}`]);
              }, err => {
                console.log(err);
              });
          }

        });

      } else {
        Swal.fire({
          icon: 'warning',
          title: 'El paciente necesita atención médica',
          text: `Presión sistólica: ${this.mensajepresionSistolica}.
                  Presión diástolica: ${this.mensajepresionDiastolica}.
                  Oxígeno: ${this.mensajeoxigeno}.
                  Temperatura: ${this.mensajetemperatura}.
                  `,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        }).then((result) => {
          if (result.isConfirmed) {
            this.datosSaludService.cargarDatosSalud(this.datosSalud)
              .subscribe(data => {
                this.router.navigate([`/datos-salud/historial-salud/${this.idPacienteParam}`]);
              }, err => {
                console.log(err);
              });
          }

        });

      }

    } if (this.router.url === `/datos-salud/editar-datos-salud/${this.idDatoSalud}`) {

      console.log("Pasa por editar");

      this.datosSaludEdit = this.formDatosSalud.value;
      this.datosSaludEdit.idCuidador = this.datosSalud.idCuidador;
      this.datosSaludEdit.idPaciente = this.datosSalud.idPaciente
      this.datosSaludEdit.id = this.datosSalud.id;

      this.mensajepresion = this.datosSaludEdit.presionsistolica + "/" + this.datosSaludEdit.presiondiastolica;

      if (this.datosSaludEdit.presionsistolica <= 90) {
        console.log("Presion baja");
        this.mensajepresionSistolica = 'baja';
      }

      if (this.datosSaludEdit.presiondiastolica <= 60) {
        console.log("Presion baja");
        this.mensajepresionDiastolica = 'baja';
      }

      if (this.datosSaludEdit.presionsistolica >= 90 && this.datosSaludEdit.presionsistolica <= 120) {
        console.log("Presion normal");
        this.mensajepresionSistolica = 'normal';
      }

      if (this.datosSaludEdit.presiondiastolica >= 60 && this.datosSaludEdit.presiondiastolica <= 80) {
        console.log("Presion normal");
        this.mensajepresionDiastolica = 'normal';
      }

      if (this.datosSaludEdit.presionsistolica > 120 && this.datosSaludEdit.presionsistolica < 129) {
        console.log("Presion elevada");
        this.mensajepresionSistolica = 'elevada';
      }

      if (this.datosSaludEdit.presiondiastolica < 60) {
        console.log("Presion elevada");
        this.mensajepresionDiastolica = 'elevada';
      }

      if (this.datosSaludEdit.presionsistolica > 130 && this.datosSaludEdit.presionsistolica < 139) {
        console.log("Presion alta hipertension");
        this.mensajepresionDiastolica = 'alta - Hipertensión';
      }

      if (this.datosSaludEdit.presiondiastolica > 80 && this.datosSaludEdit.presiondiastolica < 89) {
        console.log("Presion alta hipertension");
        this.mensajepresionDiastolica = 'alta - Hipertensión';
      }

      if (this.datosSaludEdit.temperatura < 36) {
        this.mensajetemperatura = 'baja';
      }

      if (this.datosSaludEdit.temperatura > 37) {
        this.mensajetemperatura = 'alta';
      }

      if (this.datosSaludEdit.temperatura >= 36 && this.datosSaludEdit.temperatura <= 37) {
        this.mensajetemperatura = 'normal'
      }

      if (this.datosSaludEdit.oxigeno < 95) {
        this.mensajeoxigeno = 'bajo'
      }

      if (this.datosSaludEdit.oxigeno >= 95) {
        this.mensajeoxigeno = 'normal'
      }

      if (this.mensajepresionSistolica === 'normal' && this.mensajepresionDiastolica === 'normal' && this.mensajetemperatura === 'normal' && this.mensajeoxigeno === 'normal') {
        Swal.fire({
          icon: 'success',
          title: 'El paciente se encuentra estable',
          text: `Presión: ${this.mensajepresion}. 
                  Oxígeno: ${this.mensajeoxigeno}.
                  Temperatura: ${this.mensajetemperatura}.
                  `,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        }).then((result) => {
          if (result.isConfirmed) {
            this.datosSaludService.editarDatosSalud(this.datosSaludEdit)
              .subscribe(data => {
                this.router.navigate([`/datos-salud/historial-salud/${this.datosSalud.idPaciente}`]);
              }, error => {
                console.log(error);

              });
          }

        });

      } else {
        Swal.fire({
          icon: 'warning',
          title: 'El paciente necesita atención médica',
          text: `Presión sistólica: ${this.mensajepresionSistolica}.
                  Presión diástolica: ${this.mensajepresionDiastolica}.
                  Oxígeno: ${this.mensajeoxigeno}. 
                  Temperatura: ${this.mensajetemperatura}.
                  `,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false
        }).then((result) => {
          if (result.isConfirmed) {
            this.datosSaludService.editarDatosSalud(this.datosSaludEdit)
              .subscribe(data => {
                this.router.navigate([`/datos-salud/historial-salud/${this.datosSalud.idPaciente}`]);
              }, error => {
                console.log(error);

              });
          }

        });

      }

    }

  }

  goBack() {
    this._location.back();
  }

}


