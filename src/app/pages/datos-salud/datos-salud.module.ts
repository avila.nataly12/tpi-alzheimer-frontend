import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatosSaludRoutingModule } from './datos-salud-routing.module';
import { CargarDatosSaludComponent } from './cargar-datos-salud/cargar-datos-salud.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CargarDatosSaludComponent
  ],
  imports: [
    CommonModule,
    DatosSaludRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class DatosSaludModule { }
