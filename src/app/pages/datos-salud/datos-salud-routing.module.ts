import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CargarDatosSaludComponent } from './cargar-datos-salud/cargar-datos-salud.component';
import { DatosSaludComponent } from './datos-salud.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'historial-salud/:idPaciente',
        component: DatosSaludComponent
      },
      {
        path: 'cargar-datos-salud/:idPaciente',
        component: CargarDatosSaludComponent
      },
      {
        path: 'editar-datos-salud/:id',
        component: CargarDatosSaludComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatosSaludRoutingModule { }
