import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PacientesComponent } from './pacientes.component';
import { PacienteDetalleComponent } from './paciente-detalle/paciente-detalle.component';
import { AgregarPacienteComponent } from './agregar-paciente/agregar-paciente.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'listado',
        component: PacientesComponent
      },
      {
        path: 'ver-paciente/:id',
        component: PacienteDetalleComponent
      },
      {
        path: 'agregar-paciente',
        component: AgregarPacienteComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
