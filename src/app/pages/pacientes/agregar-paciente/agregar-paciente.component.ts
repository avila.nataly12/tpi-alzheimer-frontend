import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AsociarService } from '../../../services/asociar/asociar.service';
import { AuthService } from '../../../services/auth/auth.service';
import Swal from 'sweetalert2';
import { count } from 'console';
import { ContactoService } from '../../../services/contacto/contacto.service';

@Component({
  selector: 'app-agregar-paciente',
  templateUrl: './agregar-paciente.component.html',
  styleUrls: ['./agregar-paciente.component.scss']
})
export class AgregarPacienteComponent implements OnInit {

  relacionarForm: FormGroup = this.fb.group({
    dniPaciente: ['', [Validators.required]]
  }); 
  idUsuario:string;

  constructor(
    private asociarService: AsociarService,
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private contactoService: ContactoService
    ) {
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
     }

  ngOnInit(): void {
    console.log( this.generaNss() );
  }


  generaNss() {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength =5;
    for (let i = 0; i < charactersLength; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}




  relacionar() {
    const {dniPaciente} = this.relacionarForm.value;
    const idCuidador = this.idUsuario;


    this.authService.dameUsuarioPorDni(dniPaciente)
      .subscribe(data => {

  const clave = this.generaNss();
  this.contactoService.EnviarWhatsapp("sistema",data.telefono,"Codigo de seguridad: " + clave).subscribe();


  Swal.fire({
    title: 'Introducir el codigo de seguridad',
    input: 'text',
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Guardar',
    allowOutsideClick: false
  }).then((result) => {
    const codigoIngresado= result.value 

    if (result.isConfirmed && codigoIngresado==clave) {


 



                            this.asociarService.asociarPaciente(idCuidador, dniPaciente)
                              .subscribe(res => {
                                Swal.fire({
                                  icon: 'success',
                                  title: 'Paciente asociado correctamente',
                                  showCancelButton:true,
                                  confirmButtonText:'Ver Pacientes',
                                  cancelButtonText:'Cerrar',
                                  allowOutsideClick:false
                                }).then((result)=>{
                              
                              
                                  if (result.value) {
                                  
                                    this.router.navigate(['//dashboard']);
                              
                              
                                  }else{
                                    window.location.href = "/pacientes/agregar-paciente";
                                  }
                                })

                              }, err => {
                                Swal.fire({
                                  icon: 'error',
                                  title: 'Oops...',
                                  text: 'Paciente no encontrado',
                                  allowOutsideClick:false
                                }).then((result)=>{
                              
                              
                                  if (result.value) {
                                  
                                    this.router.navigate(['/pacientes/agregar-paciente']);
                              
                              
                                  }else{
                                    window.location.href = "//dashboard";
                                  }
                                })
                              });//fin de lo que estaba




                            } else {
                              console.log("chau");

                              Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Codigo incorrecto',
                                allowOutsideClick:false
                              }).then((result)=>{ 
                              })


                            }
                        
                          });



    }, err => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Paciente no encontrado.',
        allowOutsideClick:false
      }).then((result)=>{
    
    
        if (result.value) {
         
          this.router.navigate(['/pacientes/agregar-paciente']);
    
    
         }else{
          window.location.href = "//dashboard";
        }
      })
    });//busco usuario


  }
}
