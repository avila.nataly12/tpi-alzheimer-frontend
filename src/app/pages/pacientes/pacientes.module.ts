import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PacientesRoutingModule } from './pacientes-routing.module';
import { PacienteDetalleComponent } from './paciente-detalle/paciente-detalle.component';
import { PacientesComponent } from './pacientes.component';
import { DashboardComponentsModule } from '../../dashboard/dashboard-components/dashboard-components.module';
import { ComponentsModule } from '../../component/component.module';
import { GaleriaComponent } from '../galeria/galeria.component';
import { GaleriaModule } from '../galeria/galeria.module';
import { AgregarPacienteComponent } from './agregar-paciente/agregar-paciente.component';


@NgModule({
  declarations: [ PacienteDetalleComponent, AgregarPacienteComponent,PacientesComponent],
  imports: [
    CommonModule,
    DashboardComponentsModule,
    ComponentsModule,
    PacientesRoutingModule,
    GaleriaModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PacientesModule { }


