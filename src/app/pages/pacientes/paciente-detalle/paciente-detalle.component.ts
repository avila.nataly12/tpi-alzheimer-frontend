import { Component, ElementRef,OnInit ,Renderer2,ViewChild} from '@angular/core';
import { CronogramaMedicamentosService } from '../../../services/cronograma-medicamentos/cronograma-medicamentos.service';
import { Cronograma } from '../../../interfaces/cronograma-medicamento';
import { ActivatedRoute,Router } from '@angular/router';
import { Usuario } from '../../../interfaces/usuario';
import { AuthService } from '../../../services/auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImagenesService } from '../../../services/imagenes.service';
import { ContactoService } from '../../../services/contacto/contacto.service';
import { ArchivoService } from '../../../services/archivos/archivos.service';
import { AsociarService } from '../../../services/asociar/asociar.service';
import { Archivos } from '../../../interfaces/archivo';
import Swal from 'sweetalert2';
import { ImagenesModel } from '../../../models/imagenes.model';

@Component({
  selector: 'app-paciente-detalle',
  templateUrl: './paciente-detalle.component.html',
  styleUrls: ['./paciente-detalle.component.scss']
})
export class PacienteDetalleComponent implements OnInit {

  idPaciente = this.route.snapshot.paramMap.get('id')!;
  paciente!: Usuario;
  items = [] as any;
  cronogramas!: Cronograma[];
  hayCronograma = true;
  foto!: Archivos;
  file: any;
  imgElement = '';
  imgURL = '../../../assets/img/noimage.png';
  imagen: any;
  idUsuarioCargador!:any;
  tipoarchivo!:string;
  medicamentosActivos = [] as any;

  imagenesData: ImagenesModel[] = [];
  formMedicamento: FormGroup = this.fb.group({
    idUsuarioOrigen: [''],
    idUsuarioDestino: [''],
    hora: [''],
    medicamento: [''],
    items: ['']
  });


  @ViewChild('imageFile', { static: true }) imageFile!: ElementRef;

  imagenesForm = this.fb.group({

    nombre: ['', [Validators.required]],
    imgFile: [''],
    idPaciente :['null']
  })



  constructor(private route: ActivatedRoute,
    private authService: AuthService,
    private cronogramaService: CronogramaMedicamentosService,
    private fb: FormBuilder,

    private renderer: Renderer2,
    private router: Router, 
    private imagenesSvc: ImagenesService,    
    private archivoService: ArchivoService,
    private asociarService: AsociarService,
    private contactoService: ContactoService
  ) { 

    this.archivoService.verFotoPerfil(this.idPaciente).subscribe(archivo => {
    this.foto=archivo;
    }, err => {
      console.log('Error'+err);
    });


  }

  ngOnInit(): void {
    this.authService.getUsuarioPorId(this.idPaciente)
      .subscribe(resp => {
        this.paciente = resp;
      }, error => {
        console.log(error.error);
      });

    this.cronogramaService.verCronogramaDePaciente(this.idPaciente)
      .subscribe(data => {
        if (data.length > 0) {
          this.cronogramas = data;
          this.cronogramas.forEach(cronograma => {
            cronograma.items.forEach(item => {
                if (item.estado==true){
                  this.medicamentosActivos.push(item);
                }
            });

          });
        } 

      }, error => {
        // this.hayCronograma = false;
        // console.log(error.error);
      });

  }


  enviar(event: any,idPaciente:any) {
  

    if (event.target.files.length > 0) {
      this.idPaciente=idPaciente;
      this.file = event.target.files;
      const reader = new FileReader();
      reader.readAsDataURL(this.file[0]);
      reader.onloadend = (event: any) => {

        this.imgURL = event.target.result;
        this.imgElement = event.target.result;
        elementImage.src = `${this.imgElement}`;

        this.imagen = {
          archivo: this.file[0]
        }
 

      }

 
      var containerImage = document.createElement('div');
      var status = document.createElement('p');
      var icon = document.createElement('i');
      var elementImage = document.createElement('img');


      containerImage.classList.add('containerImage');

      elementImage.crossOrigin = 'anonymous';

      this.onSubmit();
  }

  }


  onSubmit() {

    Swal.fire({
      title: '¿Está seguro de modificar la imágen?',
      showCancelButton: true,
      confirmButtonText: 'Guardar',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result.isConfirmed && result.value) {

        let cargarImagenDatos: any = {
          nombreImagen: this.idPaciente
        }
        const formImagen = this.imagenesForm.value;
        const formImagenIdPACIENTE = this.idPaciente;
        this.idUsuarioCargador=this.idPaciente;
    
     this.imagenesSvc.cargarImagenesFirebase2(this.imagen, cargarImagenDatos,this.idPaciente, "fotoDePerfil", this.idPaciente, this.tipoarchivo);
        Swal.fire({
          icon: 'success',
          title: 'El archivo se cargó correctamente',
          text: ''
        }).then((result) => {
 
            setTimeout(this.recargar,3000,this.idPaciente)

        })
      }  

    })

  }



recargar(idPaciente :any){
  window.location.href = "/pacientes/ver-paciente/"+idPaciente;

}

}
