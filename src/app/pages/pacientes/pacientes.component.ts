import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AsociarService } from '../../services/asociar/asociar.service';
import { AuthService } from '../../services/auth/auth.service';
import { Usuario } from '../../users/interface/usuario';
import { ContactoService } from '../../services/contacto/contacto.service';
import { Contacto } from '../../interfaces/contacto';
import { ImagenesModel } from '../../models/imagenes.model';
import { ImagenesService } from '../../services/imagenes.service';
import { ArchivoService } from '../../services/archivos/archivos.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Archivos } from '../../interfaces/archivo';
import {SwPush } from '@angular/service-worker';


@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.scss']
})
export class PacientesComponent implements OnInit {

  pacientes!: Usuario[];
  listaContacto!: Contacto[];
  public readonly VAPID_PUBLIC_KEY="BECAXe4bSGX5TzyaYYYcTj10x1Sz7kxbSqNa_eBgy68iZVnQCixyhoUN7l5NlGiDFn1zrX0r-yT48tjNri54C2Q"
  respuesta: any;
  id:string;
  bandera!:number;
  idUsuario!:string;
  idPaciente!:string;
  tipoarchivo!:string;
  idUsuarioCargador!:any;
  listaPacientes!: Usuario[];
  imgElement = '';
  imgURL = '../../../assets/img/noimage.png';
  imagen: any;
  imagenesData: ImagenesModel[] = [];
  imgProcess: any;
  btnActive = true;
  file: any;
  idAlbum:any;
  fotos!: Archivos[];
 
  @ViewChild('imageFile', { static: true }) imageFile!: ElementRef;

  imagenesForm = this.fb.group({

    nombre: ['', [Validators.required]],
    imgFile: [''],
    idPaciente :['null']
  })

 


  constructor(private fb: FormBuilder, private renderer: Renderer2,
    private router: Router, private route: ActivatedRoute,
     private imagenesSvc: ImagenesService,    
     private archivoService: ArchivoService,
    private asociarService: AsociarService,
    private authService: AuthService, 
    private contactoService: ContactoService,
    private swPush:SwPush,


  ) {
    this.id = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.fotos= [];

    this.asociarService.verPacientesAsociados(this.id)
    .subscribe(data => {
      this.pacientes=data;

      this.pacientes.forEach((elemento) => {
         this.archivoService.verFotoPerfil(elemento.id).subscribe(archivo => {
          this.fotos.push(archivo);
        }, err => {
      //    console.log('Error'+err);
        });

        });
     


      
    }, err => {
      console.log(err);
    });


   }

  ngOnInit(): void {
    this.subscribeToNotifications();
    this.asociarService.verPacientesAsociados(this.id)
      .subscribe(data => {
        this.pacientes=data;


        this.contactoService.listadoOrigenCuidador(this.id).subscribe(contactos => {
          this.listaContacto = contactos;
        }, err => {
          console.log('Error');
        });


        
      }, err => {
        console.log(err);
      });
  }






  enviar(event: any,idPaciente:any) {
  

    if (event.target.files.length > 0) {
      this.idPaciente=idPaciente;
      this.file = event.target.files;
      const reader = new FileReader();
      reader.readAsDataURL(this.file[0]);
      reader.onloadend = (event: any) => {

        this.imgURL = event.target.result;
        this.imgElement = event.target.result;
        elementImage.src = `${this.imgElement}`;

        this.imagen = {
          archivo: this.file[0]
        }
 

      }

      this.btnActive = false;

      var containerImage = document.createElement('div');
      var status = document.createElement('p');
      var icon = document.createElement('i');
      var elementImage = document.createElement('img');


      containerImage.classList.add('containerImage');

      elementImage.crossOrigin = 'anonymous';

      this.onSubmit();
  }

  }


  onSubmit() {

    Swal.fire({
      title: '¿Esta seguro de modificar la imagen?',
      showCancelButton: true,
      confirmButtonText: 'Guardar',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result.isConfirmed && result.value) {

        let cargarImagenDatos: any = {
          nombreImagen: this.idPaciente
        }
        const formImagen = this.imagenesForm.value;
        const formImagenIdPACIENTE = this.idPaciente;
        this.idUsuarioCargador=this.id;
    
     this.imagenesSvc.cargarImagenesFirebase2(this.imagen, cargarImagenDatos,this.idPaciente, this.idUsuarioCargador, this.idPaciente, this.tipoarchivo);
        Swal.fire({
          icon: 'success',
          title: 'El archivo se cargo correctamente',
          text: 'Ya disponible en el listado'
        }).then((result) => {
 
          this.router.navigate(['//pacientes/listado']);


        })
      }  

    })

  }




  
public enviarSOSCuidador(idPaciente:any) :void {


  console.log(idPaciente);

    this.contactoService.contactosCuidadoresPorPaciente(idPaciente).subscribe(contactos => {
      
      contactos.forEach((contacto) => {


        var nombre = contacto.nombre;
        var telefono = contacto.telefono;


      var mensaje="El cuidador necesita ayuda.";
      this.contactoService.EnviarWhatsapp(nombre,telefono,mensaje).subscribe(mensaje => {
      
        Swal.fire({
          icon: 'warning',
          title: 'Mensaje S.O.S enviado.',
          showCancelButton:true,
          confirmButtonText:'Llamar S.O.S',
          cancelButtonText:'Cancelar S.O.S',
          allowOutsideClick:false
        }).then((result)=>{
          if (result.isConfirmed) {
            window.location.href = "tel:"+telefono;
            console.log("Llamar por telefono");

          }else{
            console.log("Falsa alarma");
            this.contactoService.EnviarWhatsapp(nombre,telefono,"Falsa alarma.").subscribe();
          }
       
    
        })
 
 

    }, err => {
    console.log('Error');
  });


  });  });

}



subscribeToNotifications(){
//   console.log(this.id);
if (Notification.permission === "granted") {
  // Si es correcto, lanzamos una notificación
  //var notification = new Notification("¡Hola!");
}
 else (Notification.permission !== "denied") 
 {

  this.swPush.requestSubscription({
    serverPublicKey:this.VAPID_PUBLIC_KEY
  }).then(respuesta=>{
    
    this.respuesta=respuesta;
    this.authService.save(this.respuesta,this.id).subscribe(usuarios => {  
      console.log("save");   
    }, err => {
      console.log('Error save');
    });
    
  }).catch(err=>{
    this.respuesta=err;
    console.log("save "+err);
  })

}


  }  

}
