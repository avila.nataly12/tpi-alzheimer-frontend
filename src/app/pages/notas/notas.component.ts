import { Component, OnInit } from '@angular/core';
import { NotasService } from '../../services/notas/notas.service';
import { Nota } from '../../interfaces/nota';
import { Router } from '@angular/router'; 
import { AuthService } from '../../services/auth/auth.service';
import { Usuario } from '../../users/interface/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.scss']
})
export class NotasComponent implements OnInit {


  notas!: Nota[];
  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  idUsuario:string;
  listaPacientes!: Usuario[];
  pacienteNombre!:string;
  pacienteId!:string;
  tienenotas!:string;

  constructor(
    private fb: FormBuilder,
    private notasService: NotasService,
    private router: Router,
    private authService: AuthService
  ) { 
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

    this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
      this.listaPacientes = usuarios;

      /*
       this.listaPacientes.forEach((elemento) => {
        this.pacienteId=elemento.id; 
        this.pacienteNombre=elemento.nombre + " " + elemento.apellido; 
        });
  */
    }, err => {
      console.log('Error');
    });
    this.pacienteId=this.idUsuario; 

  }


  ngOnInit(): void {
    console.log(this.pacienteId);
    
         this.notasService.verNotasUsuarios(this.pacienteId)
          .subscribe(data => {
            this.notas = data;
            if(this.notas.length==0){
              this.tienenotas="NO";
            }else{
              this.tienenotas="SI";

            }
          }, err => {
            
            console.log('Error');
          });
      }
      

      eliminarNota(id:any){
                
        
        Swal.fire({
          icon: 'success',
          title: '¿Está seguro de eliminar la nota?',
          showCancelButton:true,
          confirmButtonText:'Eliminar',
          cancelButtonText:'Cancelar',
          allowOutsideClick:false
        }).then((result)=>{
      
      
          if (result.value) {
           
    this.notasService.Eliminar(id)
    .subscribe(data => {
      this.notasService.verNotas().subscribe(notas => {
      this.notas = notas; })
        
    }, err => {
      console.log('Error');
    });
    window.location.href = "/notas";

                 }else{
          }
        })

      }

 

 

}
