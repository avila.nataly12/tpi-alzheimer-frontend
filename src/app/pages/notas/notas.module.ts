import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotasComponent } from './notas.component';
import { VerNotaComponent } from './ver-nota/ver-nota.component';
import { NotasRoutes } from './nota.routing';
import { RouterModule } from '@angular/router';
import { CrearNotaComponent } from './crear-nota/crear-nota.component';
import { EliminarNotasComponent } from './notas-eliminar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    NotasComponent,
    VerNotaComponent,
    CrearNotaComponent,
    EliminarNotasComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(NotasRoutes)
  ]
})
export class NotasModule { }
