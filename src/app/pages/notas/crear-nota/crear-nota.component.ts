import { Component, OnInit } from '@angular/core';
import { NotasService } from '../../../services/notas/notas.service';
import { Nota } from '../../../interfaces/nota';
import { Router } from '@angular/router'; 
import { AuthService } from '../../../services/auth/auth.service';
import { Usuario } from '../../../users/interface/usuario';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-crear-nota',
  templateUrl: './crear-nota.component.html',
  styleUrls: ['./crear-nota.component.scss']
})
export class CrearNotaComponent implements OnInit {
  listaPacientes!: Usuario[];

  form: any = {
    titulo: null,
    descripcion: null,
    prioridad: 1,
    idPaciente: null,
    foto: null 
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  idUsuario:string;
  idUsuarioDestino:string;
  
  prioridades = [
    {name: '1', value: '1'},
    {name: '2', value: '2'},
    {name: '3', value: '3'},
    {name: '4', value: '4'},
    {name: '5', value: '5'}
    
  ];


  constructor(
    private notasService: NotasService,
    private router: Router,
    private authService: AuthService
  ) { 
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.idUsuarioDestino= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

  }

  ngOnInit(): void {
    this.idUsuarioDestino= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

   this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
    this.listaPacientes = usuarios;
 

  }, err => {
    console.log('Error');
  });

  }



  
  onSubmit(): void {

    const { titulo, descripcion, prioridad, idPaciente } = this.form; 
    const idUsuarioOrigen =this.idUsuario;
     if(this.form.idPaciente!=null){
     this.idUsuarioDestino=idPaciente;
   } 
    console.log(this.idUsuarioDestino);
    this.notasService.Crear(idUsuarioOrigen,this.idUsuarioDestino,titulo,descripcion,prioridad.value).subscribe(
      data => {
        
        
        Swal.fire({
          icon: 'success',
          title: 'Nota cargada correctamente.',
          showCancelButton:true,
          confirmButtonText:'Ver Notas',
          cancelButtonText:'Cerrar',
          allowOutsideClick:false
        }).then((result)=>{
      
      
          if (result.value) {
           
            this.router.navigate(['//notas']);
      
      
           }else{
            window.location.href = "/notas/crear";
          }
        })




      },
      err => {
        this.errorMessage = err.error.message;
        console.log( err.error.message);
        this.isSignUpFailed = true;
      }
    );

    }
  }
