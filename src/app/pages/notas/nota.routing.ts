import { Routes } from '@angular/router';
import { CrearNotaComponent } from './crear-nota/crear-nota.component';
import { NotasComponent } from './notas.component';
import { EliminarNotasComponent } from './notas-eliminar.component';
import { VerNotaComponent } from './ver-nota/ver-nota.component';


export const NotasRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: NotasComponent
            },
            {
                path: 'verNota/:1',
                component: VerNotaComponent
            },
            {
                path: 'eliminar/:id',
                component: EliminarNotasComponent
            },
            {
                path: 'crear',
                component: CrearNotaComponent
            }
        ]
    }
]