import { Component, OnInit } from '@angular/core';
import { NotasService } from '../../services/notas/notas.service';
import { Nota } from '../../interfaces/nota';
import { Router } from '@angular/router'; 
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-notas-eliminar',
  templateUrl: './notas-eliminar.component.html',
  styleUrls: ['./notas.component.scss']
})
export class EliminarNotasComponent implements OnInit {

  public readonly VAPID_PUBLIC_KEY="BCXCuXTH0M5LCKFujM6uTITpZypKebePhia4oR4RzmRBfNPQN7c7m2BMr1zUF7fvxX6C8_jrT5yUNApuJmN88M4"
  respuesta: any;
  notas!: Nota[];
  nota!: Nota;

  constructor(
    private notasService: NotasService,
    private router: Router,
    private route: ActivatedRoute,

   ) { } 

  ngOnInit(): void {

    const id = String(this.route.snapshot.paramMap.get('id'));
    console.log(id);

    


    this.notasService.Eliminar(id)
    .subscribe(data => {
      this.notasService.verNotas().subscribe(notas => {
      this.notas = notas; })
        
    }, err => {
      console.log('Error');
    });

 
  }

  

}
