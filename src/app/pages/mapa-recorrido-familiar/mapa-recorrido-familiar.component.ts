import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { MapService } from '../../services/mapa/map.service';

import * as mapboxgl from 'mapbox-gl';
import { RecorridoService } from '../../services/recorrido/recorrido.service';
import { AuthService } from '../../services/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Marker, Popup } from 'mapbox-gl';
import Swal from 'sweetalert2';
import { Usuario } from '../../interfaces/usuario';

@Component({
  selector: 'app-mapa-recorrido-familiar',
  templateUrl: './mapa-recorrido-familiar.component.html',
  styles: [
    `
    #map {
      width: 100%;
      height: 500px;
    }
    `
  ]
})

export class MapaRecorridoFamiliarComponent implements AfterViewInit {

  public isCollapsed = false;
  public innerWidth: number = 0;
  public defaultSidebar: string = "";
  public showMobileMenu = false;
  public expandLogo = false;
  public sidebartype = "full";

  @ViewChild('mapDiv')
  mapDivElement!: ElementRef;

  idPaciente = this.route.snapshot.paramMap.get('id')!;

  coordenadas!: number[][];

  ubicacionDestino!: [number, number];

  ubicacionPartida!: [number, number];

  ultimaUbicacion!: [number, number];

  hayCoordenadas = true;

  estadoRecorrido!: string;
  paciente!:Usuario;


  constructor(private mapService: MapService,
    private recorridoService: RecorridoService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) { 
      this.authService.getUsuarioPorId(this.idPaciente)
      .subscribe(resp => {
        this.paciente = resp;
      }, error => {
        console.log(error.error);
      });
    }

  ngAfterViewInit(): void {


    this.generateMap();
  }

  generateMap() {
    /*
    if (!this.mapService.userLocation) {
      throw Error('No hay localizacion de usuario');
    }*/

    const map = new mapboxgl.Map({
      container: this.mapDivElement.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.mapService.userLocation, // TODO: cambiar por la ubicacion de partida
      zoom: 14,

    });



        setTimeout(() => {
    this.recorridoService.listarRecorridoPorIdUsuario(this.idPaciente)
      .subscribe(resp => {
        console.log(resp);
        if (resp.length > 0) {
          this.coordenadas = resp[resp.length - 1].coordenadas;
          this.ubicacionPartida = [this.coordenadas[this.coordenadas.length - 1][0], this.coordenadas[this.coordenadas.length - 1][1]];
          this.ubicacionDestino = [this.coordenadas[0][0], this.coordenadas[0][1]];
          this.estadoRecorrido = resp[resp.length - 1].estado;
          // Marcador inicio
          new Marker({ color: 'red' })
            .setLngLat(this.ubicacionPartida)
            .setPopup(popUp)
            .addTo(map)

          // Marcador final
          new Marker({ color: 'red' })
            .setLngLat(this.ubicacionDestino)
            .setPopup(popUp)
            .addTo(map);

          this.recorridoService.listarUltimaUbicacionPorIdPaciente(this.idPaciente)
            .subscribe(data => {
              this.ultimaUbicacion = data.ubicacion;
              // Marcador cambiante
              new Marker({ color: 'blue' })
                .setLngLat(this.ultimaUbicacion)
                .addTo(map);
            });


          map.addControl(new mapboxgl.NavigationControl);

          map.fitBounds([this.ubicacionPartida, this.ubicacionDestino], {
            padding: 100
          });

          this.mapService.setMap(map);

          this.mapService.drawPolyLineCoordenadas(this.coordenadas);

          this.mapService.deletePlaces();
        } else {
          this.hayCoordenadas = false;
        }

        if(this.estadoRecorrido === "Finalizado") {
          Swal.fire({
            icon: 'success',
            title: 'Recorrido finalizado',
            allowOutsideClick: false
          }).then((result) => {
            if (result.isConfirmed) {
            //  this.router.navigate([`/auth/login`]);
            window.location.href = "/auth/login";

            }

          }, error => {
            console.log(error);

          });
      }

      }, error => {
        console.log(error);
      });


    }, 2000);

    const popUp = new Popup()
      .setHTML(`
            <h6>Ubicación actual</h6>
        `);


        if(this.estadoRecorrido != "Finalizado") {

        setTimeout(() => {
          this.generateMap()
         }, 10000);

        }


  }

}
