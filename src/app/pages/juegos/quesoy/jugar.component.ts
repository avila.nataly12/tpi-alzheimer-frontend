import { Component, OnInit } from '@angular/core';
import { Quesoy } from '../../../interfaces/quesoy';

import { JuegosService } from '../../../services/juegos/juegos.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../../services/auth/auth.service';
import { Location } from '@angular/common';


import Swal from 'sweetalert2';


@Component({
  selector: 'app-quesoy-jugar',
  templateUrl: './jugar.component.html',
  styleUrls: ['./jugar.component.scss']
})
export class JugarQueSoyComponent implements OnInit {
  juegos!: Quesoy[];
  palabraAencontrar:string;
  letrasDesordenadas:string[];
  cantidadLetras:number;
  cantidadLetrasRelleno:number;
  cantidadLetrasTotal:number;
  letrasRelleno: object;
  ubicacionLetras:number[];
  palabraAencontrarArray:string[];
  palabraanterior:string;
  duracion!:number;
  nivel!:string;

  
  public  generateRandomString(num: number,stringObject: String){
    var characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for ( let i = 0; i < stringObject.length; i++ ) {
      var characters =  characters.replace(stringObject[i], '')
    }


    let result1= '';
    const charactersLength = characters.length;
    for ( let i = 0; i < num; i++ ) {
        result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result1;
}


public desordenar(unArray:number[]){
  var t = unArray.sort(function(a,b) {return (Math.random()-0.5)});
  return [...t];
}  

 


public ubicarLetras(stringObject: String){

const ubicacionDesordenada=this.desordenar(this.ubicacionLetras);
for ( let i = 0; i < this.cantidadLetrasTotal; i++ ) {
 this.letrasDesordenadas[i]=stringObject[ubicacionDesordenada[i]];
}
return this.letrasDesordenadas;

}

    
    constructor(    private authService: AuthService,   private juegosService: JuegosService,  private router: Router, private route: ActivatedRoute,private _location: Location 
    ) {
      this.palabraAencontrar="cargando";
      this.cantidadLetras=0;
      this.cantidadLetrasRelleno=0;
      this.letrasRelleno=new String();
      this.cantidadLetrasTotal=20;
      this.ubicacionLetras=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19];
      this.letrasDesordenadas=[];
      this.palabraAencontrarArray=[];
      this.palabraanterior="";

      this.duracion=0;   

     }


     
  ngOnInit(): void {

    window.setInterval(() =>{
      this.duracion++;
    },1000);


 
    this.juegosService.verJuegosQueSoy()
    .subscribe(data => {
      this.juegos = data;

      if(this.palabraanterior!=""){

        const index = this.juegos.findIndex( x => x.nombre === this.palabraanterior );

        this.juegos.splice( index, 1 );
        console.log(this.juegos);
  
      }
      
      var cantidad = this.juegos.length;
      var randNumber = Math.floor(Math.random() * (cantidad -  1)) + 1;
      this.palabraAencontrar=data[randNumber].nombre.toUpperCase();
      const nivel = String(this.route.snapshot.paramMap.get('nivel'));

       if(nivel=="Fácil"){
        this.cantidadLetrasTotal=15;
        this.ubicacionLetras=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14];
      }
      if(nivel=="Medio"){
        this.cantidadLetrasTotal=17;
        this.ubicacionLetras=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
      }

      this.nivel=nivel;
 
      const stringObject = new String(this.palabraAencontrar);  
      this.cantidadLetras = stringObject.length;
      this.cantidadLetrasRelleno=this.cantidadLetrasTotal-this.cantidadLetras;
      this.letrasRelleno=new String(this.generateRandomString(this.cantidadLetrasRelleno,stringObject)); 
      const letrasmostrar=this.ubicarLetras(new String(this.letrasRelleno+this.palabraAencontrar))
      this.letrasDesordenadas=letrasmostrar;
      this.palabraAencontrarArray=Array.from(this.palabraAencontrar);
    }, err => {
      console.log('Error');
    });


    

  }

  cambiarPalabra(palabra : string){
    (document.querySelector('.mensajeGanador') as HTMLElement).style.display = 'none';
    const todos = document.querySelectorAll(".letrasDesordenadas");
    todos.forEach((elemento) => {
     elemento.classList.add("ocultas");
     });
     this.palabraanterior=palabra;

    this.ngOnInit();
  }
 
    mostrar(letra:string) {
      var letraocltar = '.letras_'+letra ;
       var letra = '.'+letra ;
   //(document.querySelector(letra) as HTMLElement).style.display = 'block';

   const todos = document.querySelectorAll(letra);
   todos.forEach((elemento) => {
    elemento.classList.remove("ocultas");
    });
    const todos2 = document.querySelectorAll(letraocltar);
    todos2.forEach((elemento) => {
     elemento.classList.add("ocultas2");
     });
    
    const validarOcultos = document.querySelectorAll(".ocultas");
    if(validarOcultos.length==0){
      this.guardarReporte();

   //   (document.querySelector('#botonGanador')as HTMLElement).click();
   // (document.querySelector('.mensajeGanador') as HTMLElement).style.display = 'block';


    
    Swal.fire({
      icon: 'success',
      title: '  ¡Felicitaciones! Completaste el juego.',
      showCancelButton:true,
      confirmButtonText:'Volver a Jugar',
      cancelButtonText:'Cerrar',
      allowOutsideClick:false
    }).then((result)=>{
      if (result.isConfirmed) {


        const todos2 = document.querySelectorAll(".ocultas2");
        todos2.forEach((elemento) => {
         elemento.classList.remove("ocultas2");
         });


        this.cambiarPalabra(this.palabraAencontrar);
      }
    })


    }

}


guardarReporte() {
  const idPaciente=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
 // const idJuego="634597acd225783c5ffe4bac";63852d2e0085fe090189c9e2
 const idJuego="63852d2e0085fe090189c9e2";
  const termino= true;
  var fecha= new Date().toLocaleDateString();
   
      this.juegosService.guardarReporte(idJuego,idPaciente,this.duracion,this.nivel,fecha,termino).subscribe(data => {
        console.log(data);
  
      });
  
  
  
    }

    goBack(){
      this._location.back();
    }

}
