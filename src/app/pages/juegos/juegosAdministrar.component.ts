import { Component, OnInit } from '@angular/core';
import { Juego } from '../../interfaces/juego';
import { JuegosHabilitados } from '../../interfaces/juegosHabilitados';
import { JuegosService } from '../../services/juegos/juegos.service';
import { AuthService } from '../../services/auth/auth.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Location } from '@angular/common';

@Component({
  selector: 'app-juegos-administrar',
  templateUrl: 'juegos-administrar.component.html',
  styleUrls: ['./juegos.component.scss']
})
export class JuegosAdministrarComponent implements OnInit {
 
 

  form: FormGroup = this.fb.group({
    nivel: [''],
 //   idHabilitado: [''],
  //  idJuego: ['']
  });

 

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  juegos!: Juego[];
  juegosHabilitados!: JuegosHabilitados[];
  idUsuario:string;
  pacienteNombre:string;
  juegoNombre!:string;
  pacienteId!:string;
  idJuego: string;
  idHabilitado!: string;
  nivel: string;
  constructor(
    private _location: Location,
    private fb: FormBuilder,
    private authService: AuthService,
    private juegosService: JuegosService ,  private router: Router, private route: ActivatedRoute
   ) {     
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.pacienteNombre="Nombre del paciente";
    this.juegoNombre="juego";
    const idPaciente = String(this.route.snapshot.paramMap.get('idPaciente'));

    

    this.authService.getUsuarioPorId(idPaciente)
    .subscribe(paciente => {
      this.pacienteNombre=paciente.apellido + " " + paciente.nombre;

     }, err => {
      console.log('Error');
    });


    this.pacienteId=idPaciente;
    const idJuego = String(this.route.snapshot.paramMap.get('idJuego'));

    this.juegosService.dameUnJuego(idJuego)
    .subscribe(juego => {
      this.juegoNombre=juego.nombre;

     }, err => {
      console.log('Error');
    });

    


    this.idJuego=idJuego;
    this.idHabilitado="null";
    this.nivel="no";
    
    this.juegosService.verHabilitados(idPaciente)
    .subscribe(data2 => {
      this.juegosHabilitados = data2;
      
      this.juegosHabilitados.forEach((elemento) => {
        if(elemento.idJuego==this.idJuego){
        this.idHabilitado=elemento.id; 
        this.nivel=elemento.nivel; 
      }
      });

     }, err => {
      console.log('Error');
    });
 

  }


  goBack(){
    this._location.back();
  }


  ngOnInit(): void {

  
   /*
    this.juegosService.verHabilitados(this.pacienteId)
    .subscribe(data2 => {
      this.juegosHabilitados = data2;
      
      this.juegosHabilitados.forEach((elemento) => {
      this.idHabilitado=elemento.id; 
      });
    
  }); */
}

  onSubmit(): void {
    
    const { nivel } = this.form.value; 
    
    console.log(this.form.value);
    const idUsuarioOrigen =this.idUsuario;
    const idUsuarioDestino=this.pacienteId;
    const idJuego =this.idJuego;
    var idHabilitado =this.idHabilitado;
    console.log(idHabilitado);

    this.juegosService.actualizarHabilitados(idHabilitado,nivel,idUsuarioDestino,idJuego).subscribe(
      data => {


        Swal.fire({
          icon: 'success',
          title: 'La modificacion se logro con éxito.',
          showCancelButton:true,
          confirmButtonText:'Volver al listado',
          cancelButtonText:'Cerrar',
          allowOutsideClick:false
        }).then((result)=>{
      
      
          if (result.value) {
           
            this.router.navigate(['//juegos/habilitar']);
      
      
           } 
        })







      },
      err => {
        this.errorMessage = err.error.message;
        console.log( err.error.message);
        this.isSignUpFailed = true;
      }
    );

    }


}
