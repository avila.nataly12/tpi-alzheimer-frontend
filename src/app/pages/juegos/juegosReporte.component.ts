import { AfterViewInit, Component,ElementRef, OnInit,ViewChild ,Renderer2 } from '@angular/core';
import { Reporte } from '../../interfaces/reporte';
import { Juego } from '../../interfaces/juego';
import { JuegosHabilitados } from '../../interfaces/juegosHabilitados';
import { JuegosService } from '../../services/juegos/juegos.service';
import { AuthService } from '../../services/auth/auth.service';
import { Router ,ActivatedRoute} from '@angular/router'; 
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from '../../users/interface/usuario';
import Chart from 'chart.js/auto';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-juegos-habilitar',
  templateUrl: './juegos-reporte.component.html',
  styleUrls: ['./juegos-reporte.component.scss']
})
export class JuegosReporteComponent  {
  
 
 
  form: any = {
    desde: null,
    hasta: null,
    idJuego: null,
    idPaciente: null 
  };
 
  public bandera:number;
  public cantidadResultados:number;
  public chart: any;
  public chart2: any;
  public chart3: any;
  public cantidadFacil: any;
  public cantidadMedio: any;
  public cantidadDificil: any;
  public duraciones: number[]=[];
  public fechas: string[]=[];
   juegosHabilitados!: JuegosHabilitados[];
  juegos!: Juego[];
  pacienteNombre!:string;
  juegoNombre!:string;
  idUsuario!:string;
  hoy!:any;
  pacienteId!:string;
  reportes!:Reporte[];
  listaPacientes!:Usuario[];
   rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
 
  constructor(private el : ElementRef,
     private authService: AuthService,
    private juegosService: JuegosService , 
     private router: Router, private route: ActivatedRoute   ) {   
    this.cantidadFacil=0;
    this.cantidadMedio=0;
    this.cantidadDificil=0; 
    this.cantidadResultados=0;
    this.bandera=0;
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
       console.log(this.el);
       let date: Date = new Date();
       const mes =date.getMonth()+1;
      this.hoy=date.getFullYear() + "-" + mes + "-" + date.getDate();
 
     }
  
 
    

     ngOnInit(): void {
      


      this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
        this.listaPacientes = usuarios; 
        console.log(this.listaPacientes);

   

    
    this.juegosService.verTodos()
    .subscribe(data => {
      this.juegos = data; 
      
       
    //this.onSubmit();
 
 

    }, err => {
      console.log('Error');
    });


      }, err => {
        console.log('Error');
      });


      


  }



  
  onSubmit() : void{
 


    this.cantidadFacil=0;
    this.cantidadMedio=0;
    this.cantidadDificil=0; 
    this.duraciones=[];
    this.fechas=[];
  
    var { desde, hasta, idJuego, idPaciente } = this.form; 
 
/*
    if(desde==null || hasta==null ){
      console.log(desde);
      const [d,m,y] = desde.split('/');
     const dateConversion = new Date(`${y}/${m}/${d}`);
     if(dateConversion){
      console.log("if")
     }else{
      console.log("else")

     }
     
    }*/



    if(desde==null || hasta==null || idJuego==null || idPaciente==null){
    Swal.fire({
      icon: 'error',
      title: 'Debe completar todos los campos',
      showCancelButton:true,
      confirmButtonText:'Ok',
      cancelButtonText:'Cerrar',
      allowOutsideClick:false
    }).then((result)=>{
  
   
    });
  }else{

 

        console.log(desde);
        console.log(hasta);
        console.log(idJuego);
        console.log(idPaciente);

    this.juegosService.dameReportePorFechas(desde,hasta,idJuego,idPaciente).subscribe(
      data => {
        this.reportes= data;

        this.cantidadResultados=this.reportes.length;
        this.bandera=1;

         data.forEach((elemento) => {
          

          if(elemento.nivel=="Fácil"){
            this.cantidadFacil++;
          }
          if(elemento.nivel=="Medio"){
            this.cantidadMedio++;
          }
          if(elemento.nivel=="Difícil"){
            this.cantidadDificil++;
          }


          if(this.fechas.length<=15){
          var minutos=parseInt(elemento.duracion)/60;
          this.duraciones.push(minutos);
          this.fechas.push(new Date(elemento.fechaCreacion).toLocaleDateString('es-es', {year:"numeric", month:"short", day:"numeric"}) + " / " + elemento.nivel);
        }


        });
 


 
        this.clicBotones();
  

         
       },
      err => { 
      }
    );

  }
 

   
  
  }




  onChange() : void{
 


    this.cantidadFacil=0;
    this.cantidadMedio=0;
    this.cantidadDificil=0; 
    this.duraciones=[];
    this.fechas=[];
  
    var { desde, hasta, idJuego, idPaciente } = this.form; 
 
 
        console.log(desde);
        console.log(hasta);
        console.log(idJuego);
        console.log(idPaciente);
 
        if(desde!=null && hasta!=null){
      
    this.juegosService.dameReportePorFechas(desde,hasta,idJuego,idPaciente).subscribe(
      data => {
        this.reportes= data;

        this.cantidadResultados=this.reportes.length;
        this.bandera=0;

         data.forEach((elemento) => {
          

          if(elemento.nivel=="Fácil"){this.cantidadFacil++;}
          if(elemento.nivel=="Medio"){this.cantidadMedio++;}
          if(elemento.nivel=="Difícil"){this.cantidadDificil++;}
           if(this.fechas.length<=15){
          var minutos=parseInt(elemento.duracion)/60;
          this.duraciones.push(minutos);
          this.fechas.push(new Date(elemento.fechaCreacion).toLocaleDateString('es-es', {year:"numeric", month:"short", day:"numeric"}) + " " + " / "+elemento.nivel);
           }


        });
 


 
        this.clicBotones();
  

         
       },
      err => { 
      }
    );    
  }
 

   
  
  }




  public clicBotones():void{
    
    let content3:HTMLElement= document.getElementById('grafico3')!;
    content3.click();


  let content2:HTMLElement= document.getElementById('grafico2')!;
    content2.click();  
  }

   
  public example2() : void{
 
    if (this.chart2) {
      this.chart2.destroy();
      }
  
      Chart.defaults.font.size = 16;

    this.chart2 = new Chart("example2", {
      type: 'bar',
      data: {
          labels: this.fechas,
          datasets: [{
              label: 'Minutos',
               data: this.duraciones,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          },]
      },
      options: {
        indexAxis: 'y',

        responsive: true,
          scales: {
              y: {
                  beginAtZero: true
              }
          },
          plugins: {
            legend: {

           /*   labels: {
                font: {
                    size: 16
                   }
                 },*/
                 display: false,

              position: 'top',
            },subtitle: {
              display: true,
              text: '¿Cuántos minutos demoró en cada juego?'
            }
          }
      }
    });

 
    }
  
  

    public example3() :void{
    if (this.chart3) {
      this.chart3.destroy();
    }

      this.chart3 = new Chart("example3", {
      type: 'doughnut',
  data: {
    labels: [ 'Difícil: '+ this.cantidadDificil, 'Medio: '+this.cantidadMedio,'Fácil: '+this.cantidadFacil],
    datasets: [
      {
        label: 'Dataset 1',
        data: [this.cantidadDificil,this.cantidadMedio,this.cantidadFacil],
       }
    ]
  },
  options: {
    
    aspectRatio:1.5,
    responsive: true,
    
     plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
      //  text: 'Juegos resueltos por nivel'
      text: 'Resultados'
      },subtitle: {
        display: true,
        text: 'Cantidad de veces jugadas: '+this.cantidadResultados
      }
    }
  },
  });

 
  }
  //compara 2 graficos de barra
  createChart(){
  
    this.chart = new Chart("MyChart", {
      type: 'bar', //this denotes tha type of chart

      data: {// values on X-Axis
        labels: ['2022-05-10', '2022-05-11', '2022-05-12','2022-05-13',
								 '2022-05-14', '2022-05-15', '2022-05-16','2022-05-17', ], 
	       datasets: [
          {
            label: "Sales",
            data: ['467','576', '572', '79', '92',
								 '574', '573', '576'],
            backgroundColor: 'blue'
          },
          {
            label: "Profit",
            data: ['542', '542', '536', '327', '17',
									 '0.00', '538', '541'],
            backgroundColor: 'limegreen'
          }  
        ]
      },
      options: {
        aspectRatio:2.5
      }
      
    });


    /*
 const labels = ['Enero', 'Febrero', 'Marzo', 'Abril']
 
const graph = document.querySelector("#grafica");
 
const data = {
    labels: labels,
    datasets: [{
        label:"Ejemplo 1",
        data: [1, 2, 3, 4],
        backgroundColor: 'rgba(9, 129, 176, 0.2)'
    }]
};
 
const config = {
    type: 'bar',
    data: data,
};
 
new Chart(graph, config);
*/


  }

 





}
