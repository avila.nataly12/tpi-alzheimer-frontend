import { Component, OnInit } from '@angular/core';
import { Quesoy } from '../../../interfaces/quesoy';

import { JuegosService } from '../../../services/juegos/juegos.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { IfStmt } from '@angular/compiler';
import Swal from 'sweetalert2';
import { AuthService } from '../../../services/auth/auth.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-quesoy-jugar',
  templateUrl: './jugarMemoria.component.html',
  styleUrls: ['./jugarMemoria.component.scss']
})
export class JugarMemoriaComponent implements OnInit {
  parteA!: Quesoy[];
  parteB!: Quesoy[];


  cartaA!:string;
  cartaB!:string;

  cartaAClass!:string;
  cartaBClass!:string;
  contador!:number;
  duracion!:number;
  limpiarCartas!:string;
  nivel!:string;
  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

    
    constructor(       private authService: AuthService,    private juegosService: JuegosService,  private router: Router, private route: ActivatedRoute,private _location: Location, 
    ) {
      this.cartaAClass="";
      this.cartaBClass="";  
      this.cartaA="";
      this.cartaB="";   
      this.limpiarCartas="NO";   
      this.contador=0;   
      this.duracion=0;   
     }

  ngOnInit(): void {

   
    window.setInterval(() =>{
      this.duracion++;
    },1000);


    this.nivel = String(this.route.snapshot.paramMap.get('nivel'));


    this.juegosService.verJuegosMemoria(this.nivel)
    .subscribe(data => {
      this.parteA = data.sort(function(a, b){return 0.5 - Math.random()});

    });

    this.juegosService.verJuegosMemoria(this.nivel)
    .subscribe(data => {
      this.parteB =  data.sort(function(b, c){return 0.1 - Math.random()});

    });


  }
  

  mostrar(palabra : string){

    if(this.contador<2){


    if(this.cartaA==""){
      const palabraAcomparar=palabra.split('_');
      this.cartaA=palabraAcomparar[0];
      this.cartaAClass=palabra;

      (document.querySelector('.'+palabra) as HTMLElement).style.display = 'block';
      (document.querySelector('.'+palabra+'Carta') as HTMLElement).style.display = 'none';
     // console.log(this.cartaA);
     this.contador++;
    }else{
      this.contador++;
      const palabraAcomparar=palabra.split('_');
      this.cartaB=palabraAcomparar[0]; 
      this.cartaBClass=palabra;

      (document.querySelector('.'+palabra) as HTMLElement).style.display = 'block';
      (document.querySelector('.'+palabra+'Carta') as HTMLElement).style.display = 'none';

      this.limpiarCartas="SI";   
      //console.log(this.cartaB);

    }

    if(this.cartaA==this.cartaB){
      this.contador=0;
      (document.querySelector('.'+this.cartaBClass) as HTMLElement).classList.remove("ocultas");;
      (document.querySelector('.'+this.cartaAClass) as HTMLElement).classList.remove("ocultas");;
 
    }else{
      /*console.log("-------");
      console.log(this.cartaA);
      console.log(this.cartaB);
      console.log("-------");*/

      if(this.cartaA!="" && this.cartaB!=""){

        setTimeout( () => {
          
      (document.querySelector('.'+this.cartaBClass+'Carta') as HTMLElement).style.display = 'block';
      (document.querySelector('.'+this.cartaAClass+'Carta') as HTMLElement).style.display = 'block';

      (document.querySelector('.'+this.cartaBClass) as HTMLElement).style.display = 'none';
      (document.querySelector('.'+this.cartaAClass) as HTMLElement).style.display = 'none';
      this.contador=0;
       }, 2000 );
      
      }



    }


    

if( this.limpiarCartas=="SI"){
  this.cartaA="";
  this.cartaB="";   
  this.limpiarCartas="NO";
}
 
}

this.validarSiGanamos();
}

 

validarSiGanamos(){

  const validarOcultos = document.querySelectorAll(".ocultas");
  if(validarOcultos.length==0){
    this.guardarReporte();

  
  Swal.fire({
    icon: 'success',
    title: '  ¡Felicitaciones! Completaste el juego.',
    showCancelButton:true,
    confirmButtonText:'Volver a Jugar',
    cancelButtonText:'Cerrar',
    allowOutsideClick:false
  }).then((result)=>{


    if (result.isConfirmed) {
     
      window.location.href = "/juegos/memoria/"+this.nivel;


     }
  })


  }
}
  guardarReporte() {
const idPaciente=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
//const idJuego="634597edd225783c5ffe4bad";
const idJuego="63852b850085fe090189c9e1";
const termino= true;
var fecha= new Date().toLocaleDateString();
 
    this.juegosService.guardarReporte(idJuego,idPaciente,this.duracion,this.nivel,fecha,termino).subscribe(data => {
      console.log(data);

    });



  }

  goBack(){
    this._location.back();
  }
}
 