import { Routes } from '@angular/router';
import { JugarQueSoyComponent } from './quesoy/jugar.component';
import { JugarMemoriaComponent } from './memoria/jugarMemoria.component';
import { JuegosComponent } from './juegos.component';
import { JuegosHabilitarComponent } from './juegosHabilitar.component';
import { JuegosReporteComponent } from './juegosReporte.component';
import { JuegosAdministrarComponent } from './juegosAdministrar.component';


export const JuegosRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: JuegosComponent
            },
            {
                path: 'quesoy/:nivel',
                component: JugarQueSoyComponent
            } 
            ,
            {
                path: 'memoria/:nivel',
                component: JugarMemoriaComponent
            },
            {
                path: 'habilitar',
                component: JuegosHabilitarComponent
            }
            /*,
            {
                path: 'reporte/:idJuego/:idPaciente',
                component: JuegosReporteComponent
            }
           
           ,
            {
                path: 'reporte/:idJuego/:idPaciente/:desde/:hasta',
                component: JuegosReporteComponent
            } */
              ,
            {
                path: 'reporte',
                component: JuegosReporteComponent
            },
            {
                path: 'administrar/:idJuego/:idPaciente',
                component: JuegosAdministrarComponent
            } 
        ]
    }
]