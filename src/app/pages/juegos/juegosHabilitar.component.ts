import { Component, OnInit } from '@angular/core';
import { Juego } from '../../interfaces/juego';
import { JuegosHabilitados } from '../../interfaces/juegosHabilitados';
import { JuegosService } from '../../services/juegos/juegos.service';
import { AuthService } from '../../services/auth/auth.service';
import { Router,ActivatedRoute } from '@angular/router'; 

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from '../../users/interface/usuario';


@Component({
  selector: 'app-juegos-habilitar',
  templateUrl: './juegos-habilitar.component.html',
  styleUrls: ['./juegos.component.scss']
})
export class JuegosHabilitarComponent implements OnInit {
  form: any = {
    desde: null,
    hasta: null,
    idJuego: null,
    idPaciente: null 
  };
  

  
  juegos!: Juego[];
  listaPacientes!: Usuario[];
  juegosHabilitados!: JuegosHabilitados[];
  idUsuario:string;
  pacienteNombre!:string;
  pacienteId!:string;
  idHabilitado:string;
  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private juegosService: JuegosService , 
    private router: Router, private route: ActivatedRoute,
    ) {     
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    

    

    this.idHabilitado="";
   
  }

  ngOnInit(): void {

 this.cargarListado();

    this.juegosService.verTodos()
      .subscribe(data => {
        this.juegos = data;

        console.log(this.juegos);
        this.juegosService.verHabilitados(this.pacienteId)
        .subscribe(data2 => {
          this.juegosHabilitados = data2;

        }, err => {
          console.log('Error');
        });

        
      }, err => {
        console.log('Error');
      });

      
  }

 
public verHabilitado():void{
  console.log(this.pacienteId);
  this.juegosService.verHabilitados(this.pacienteId)
  .subscribe(data2 => {
    this.juegosHabilitados = data2;

  }, err => {
    console.log('Error');
  });
}

public cargarListado():void{
  this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
    this.listaPacientes = usuarios;

    this.listaPacientes.forEach((elemento) => {
      this.pacienteId=elemento.id; 
      this.pacienteNombre=elemento.nombre + " " + elemento.apellido; 
      });
      this.verHabilitado();
  }, err => {
    console.log('Error');
  });

}

onSubmit(){
 

  const { desde, hasta, idJuego, idPaciente } = this.form; 
/*
  const idPaciente = String(this.route.snapshot.paramMap.get('idPaciente'));
  const idJuego = String(this.route.snapshot.paramMap.get('idJuego'));
  */
  console.log(this.form);
  console.log(idJuego);
  console.log(this.pacienteId);
  console.log(desde);
  console.log(hasta);

  window.location.href = "/juegos/reporte/"+idJuego+"/"+this.pacienteId+"/"+desde+"/"+hasta;

}


}
