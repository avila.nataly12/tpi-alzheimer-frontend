import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JuegosComponent } from './juegos.component';
import { JugarQueSoyComponent } from './quesoy/jugar.component';
import { JuegosHabilitarComponent } from './juegosHabilitar.component';
import { JuegosReporteComponent } from './juegosReporte.component';
import { JuegosAdministrarComponent } from './juegosAdministrar.component';
import { JugarMemoriaComponent } from './memoria/jugarMemoria.component';
import { JuegosRoutes } from './juegos.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    JuegosComponent,
    JuegosHabilitarComponent,
    JugarQueSoyComponent,
    JugarMemoriaComponent,
    JuegosAdministrarComponent,
    JuegosReporteComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(JuegosRoutes)
  ]
})
export class JuegosModule { }
