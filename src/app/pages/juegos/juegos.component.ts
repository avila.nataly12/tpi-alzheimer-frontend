import { Component, OnInit } from '@angular/core';
import { Juego } from '../../interfaces/juego';
import { JuegosHabilitados } from '../../interfaces/juegosHabilitados';
import { JuegosService } from '../../services/juegos/juegos.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.scss']
})
export class JuegosComponent implements OnInit {
  juegos!: Juego[];
  juegosHabilitados!: JuegosHabilitados[];
  idUsuario:string;
  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  
  constructor(
    private authService: AuthService,
    private juegosService: JuegosService 
   ) {     
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  }

  ngOnInit(): void {
    console.log(this.idUsuario);
    this.juegosService.verTodos()
      .subscribe(data => {
        this.juegos = data;


        this.juegosService.verHabilitados(this.idUsuario)
        .subscribe(data => {
          this.juegosHabilitados = data;
        }, err => {
          console.log('Error');
        });

        
      }, err => {
        console.log('Error');
      });
  }


}
