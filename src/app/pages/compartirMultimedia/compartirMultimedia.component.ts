import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ImagenesModel } from '../../models/imagenes.model';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ImagenesService } from '../../services/imagenes.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { ArchivoService } from '../../services/archivos/archivos.service';
import { Usuario } from '../../users/interface/usuario';
import { AuthService } from '../../services/auth/auth.service';
import { Archivos } from '../../interfaces/archivo';
import { count } from 'console';


@Component({
  selector: 'app-multimedia',
  templateUrl: './compartirMultimedia.component.html',
  styleUrls: ['./compartirMultimedia.component.scss']
})
export class CompartirMultimediaComponent implements OnInit {

  listaPacientes!: Usuario[];
  listaArchivos!: Archivos[];
  idUsuario!:string;
  usuarioArchivos!:string;
  idAlbumCuidador!:string;
  cantidad!:number;

  constructor(
    private fb: FormBuilder,
    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private imagenesSvc: ImagenesService,    
    private archivoService: ArchivoService,
    private authService: AuthService
     ) { 
      this.usuarioArchivos=this.idUsuario;
      this.idAlbumCuidador="1";
      
     }

  ngOnInit(): void {
    this.idUsuario=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

    this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
      this.listaPacientes = usuarios;

    }, err => {
      console.log('Error');
    });

//me di cuenta que tengo que guardar el tipo de archivo
    this.archivoService.verMultimedia(this.idUsuario).subscribe(archivos => {
      this.listaArchivos = archivos;
      this.cantidad=this.listaArchivos.length;
    }, err => {
      console.log('Error');
    });



  }

}
