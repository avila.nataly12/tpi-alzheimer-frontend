import { Routes } from '@angular/router';
 import { CompartirMultimediaComponent } from './compartirMultimedia.component';
import { CrearMultimediaComponent } from './crear-multimedia/crear-multimedia.component';
import { ListadoMultimediaComponent } from './listado-multimedia/listado-multimedia.component';
import { ListadoInformacionComponent } from './informacion/listado-informacion.component';
 

export const CompartirMultimediaRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: CompartirMultimediaComponent
            } ,  {
                path: 'crear',
                component: CrearMultimediaComponent
            } 
            ,  {
                path: 'listado',
                component: ListadoMultimediaComponent
            },
              {
                path: 'informacion',
                component: ListadoInformacionComponent
            } 
        ]
    }
]