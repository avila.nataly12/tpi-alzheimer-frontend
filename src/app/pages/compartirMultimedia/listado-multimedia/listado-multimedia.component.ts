
  import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
  import { ImagenesModel } from '../../../models/imagenes.model';
  import { FormBuilder, Validators } from '@angular/forms';
  import Swal from 'sweetalert2';
  import { ImagenesService } from '../../../services/imagenes.service';
  import { Router,ActivatedRoute } from '@angular/router'; 
  import { ArchivoService } from '../../../services/archivos/archivos.service';
  import { Usuario } from '../../../users/interface/usuario';
  import { AuthService } from '../../../services/auth/auth.service';
  import { Archivos } from '../../../interfaces/archivo';
   
  
  @Component({
    selector: 'app-listado-multimedia',
    templateUrl: './listado-multimedia.component.html',
    styleUrls: ['./listado-multimedia.component.scss']
  })
  export class ListadoMultimediaComponent implements OnInit {
  
    listaPacientes!: Usuario[];
    listaArchivos!: Archivos[];
    idUsuario!:string;
    usuarioArchivos!:string;
    idAlbumCuidador!:string;
    errorMessage!:string;
    rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

    constructor(
      private fb: FormBuilder,
      private renderer: Renderer2,
      private router: Router,
      private route: ActivatedRoute,
      private imagenesSvc: ImagenesService,    
      private archivoService: ArchivoService,
      private authService: AuthService
       ) { 
        this.idAlbumCuidador="1";
       }
  
    ngOnInit(): void {
      this.idUsuario=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
      this.usuarioArchivos=this.idUsuario;

      this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
        this.listaPacientes = usuarios;
      }, err => {
        console.log('Error');
      });
  
      this.cargarArchivos();
  
  
  
    }


    
    cambiarlista() {
    console.log(this.usuarioArchivos);
    this.cargarArchivos();
    }

    cambiarEstado(id:string){
       this.archivoService.cambiarEstado(id).subscribe(archivos => {
       }, err => {
        console.log('Error');
      });;
       console.log(id);
    }

    cargarArchivos(){
  
      this.archivoService.verArchivos(this.usuarioArchivos,this.idAlbumCuidador).subscribe(archivos => {
        this.listaArchivos = archivos;
      }, err => {
        console.log('Error');
      });
    }






    
  eliminar(id:any, nombreImagen:string) {


    Swal.fire({
      icon: 'question',
      title: 'Desea eliminar el registro?',
      showCancelButton:true,
      confirmButtonText:'Eliminar',
      allowOutsideClick:false
    }).then((result)=>{
      if (result.isConfirmed) {

      this.imagenesSvc.eliminarImagen(id, nombreImagen);
      this.archivoService.eliminar(id).subscribe(
        data => {
     
        },
        err => {
          this.errorMessage = err.error.message;
          console.log( err.error.message);
         }
      );
      window.location.href = "/compartirMultimedia/listado";

      }
   

    })
//tengo que recargar la lista de archivos     this.cargarArchivos();


  }


  
  }
  