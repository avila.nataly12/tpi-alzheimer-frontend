
  import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
  import { ImagenesModel } from '../../../models/imagenes.model';
  import { FormBuilder, Validators } from '@angular/forms';
  import Swal from 'sweetalert2';
  import { ImagenesService } from '../../../services/imagenes.service';
  import { Router,ActivatedRoute } from '@angular/router'; 
  import { ArchivoService } from '../../../services/archivos/archivos.service';
  import { Usuario } from '../../../users/interface/usuario';
  import { AuthService } from '../../../services/auth/auth.service';
  import { Archivos } from '../../../interfaces/archivo';
   
  
  @Component({
    selector: 'app-listado-informacion',
    templateUrl: './listado-informacion.component.html',
    styleUrls: ['./listado-informacion.component.scss']
  })
  export class ListadoInformacionComponent implements OnInit {
  
    listaPacientes!: Usuario[];
    listaArchivos!: Archivos[];
    idUsuario!:string;
    usuarioArchivos!:string;
    idAlbumCuidador!:string;
    errorMessage!:string;
    rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

    constructor(
      private fb: FormBuilder,
      private renderer: Renderer2,
      private router: Router,
      private route: ActivatedRoute,
      private imagenesSvc: ImagenesService,    
      private archivoService: ArchivoService,
      private authService: AuthService
       ) { 
        this.idAlbumCuidador="1";
       }
  
    ngOnInit(): void {
      this.idUsuario=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
      this.usuarioArchivos=this.idUsuario;

      this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
        this.listaPacientes = usuarios;
      }, err => {
        console.log('Error');
      });
  
  
  
  
    }


   


  
  }
  