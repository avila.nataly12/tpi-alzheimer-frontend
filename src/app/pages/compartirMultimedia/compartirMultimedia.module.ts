import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompartirMultimediaComponent } from './compartirMultimedia.component';
import { ListadoMultimediaComponent } from './listado-multimedia/listado-multimedia.component';
import { CompartirMultimediaRoutes } from './compartirMultimedia.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CompartirMultimediaComponent,
    ListadoMultimediaComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(CompartirMultimediaRoutes)
  ]
})
export class CompartirMultimediaModule { }
