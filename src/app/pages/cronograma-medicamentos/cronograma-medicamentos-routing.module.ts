import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CronogramaMedicamentosComponent } from './cronograma-medicamentos.component';
import { CrearCronogramaMedicamentosComponent } from './crear-cronograma-medicamentos/crear-cronograma-medicamentos.component';
import { EditarCronogramaMedicamentosComponent } from './editar-cronograma-medicamentos/editar-cronograma-medicamentos.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CronogramaMedicamentosComponent
      },
      {
        path: 'crear',
        component: CrearCronogramaMedicamentosComponent
      },
      {
        path: 'editar/:id',
        component: EditarCronogramaMedicamentosComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CronogramaMedicamentosRoutingModule { }