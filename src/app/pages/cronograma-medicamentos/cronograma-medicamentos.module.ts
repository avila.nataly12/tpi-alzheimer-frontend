import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CronogramaMedicamentosRoutingModule } from './cronograma-medicamentos-routing.module';
import { CronogramaMedicamentosComponent } from './cronograma-medicamentos.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CrearCronogramaMedicamentosComponent } from './crear-cronograma-medicamentos/crear-cronograma-medicamentos.component';
import { EditarCronogramaMedicamentosComponent } from './editar-cronograma-medicamentos/editar-cronograma-medicamentos.component';


@NgModule({
  declarations: [CronogramaMedicamentosComponent, CrearCronogramaMedicamentosComponent, EditarCronogramaMedicamentosComponent],
  imports: [
    CommonModule,
    //ComponentsModule,
    CronogramaMedicamentosRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class CronogramaMedicamentosModule { }
