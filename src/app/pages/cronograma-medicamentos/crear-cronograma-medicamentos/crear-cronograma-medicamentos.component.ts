import { Component, HostDecorator, OnInit } from '@angular/core';
import { CronogramaMedicamentosService } from '../../../services/cronograma-medicamentos/cronograma-medicamentos.service';
import { Cronograma } from '../../../interfaces/cronograma-medicamento';
import { Router,ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../../services/auth/auth.service';
import { Usuario } from '../../../users/interface/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';

@Component({
  selector: 'app-crear-cronograma-medicamentos',
  templateUrl: './crear-cronograma-medicamentos.component.html',
  styleUrls: ['./crear-cronograma-medicamentos.component.scss']
})
export class CrearCronogramaMedicamentosComponent implements OnInit {
  listaPacientes!: Usuario[];

  formCronograma: FormGroup = this.fb.group({
    idUsuarioOrigen: [''],
    idUsuarioDestino: ['', [Validators.required]],
    hora:['', [Validators.required]],
    medicamento:['', [Validators.required]],
    items:['']
  });

  idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  idUsuarioDestino!:string;
  items = [] as any;
  cronograma!:Cronograma;
  dias:String[]=["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"];
  diasAgregados:String[]=[];
  diaSelectedArray = [] as any;


  constructor(
    private cronogramaService: CronogramaMedicamentosService,
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder,
    private _location: Location,
    private route: ActivatedRoute
  ) {  }

  ngOnInit(): void {
   this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
    this.listaPacientes = usuarios;
    console.log(this.listaPacientes);
  }, err => {
    console.log('Error');
  });

  }

  sendCronograma(): void {
    this.cronograma = this.formCronograma.value;
    this.items.push({dias:this.diaSelectedArray, hora:this.formCronograma.value["hora"], medicamento: this.formCronograma.value["medicamento"], estado:true});

    this.cronograma.items=this.items;
     if(this.cronograma.idUsuarioDestino!=null){
     this.idUsuarioDestino=String(this.route.snapshot.paramMap.get('idPaciente'));
     this.cronograma.idUsuarioOrigen=this.idUsuario;
   } 
    this.cronogramaService.Crear(this.cronograma).subscribe(
      data => {
        Swal.fire({
          icon: 'success',
          title: 'Cronograma creado correctamente.',
          allowOutsideClick:false
        }).then((result)=>{
      
      
          if (result.value) {
           
            this._location.back();
      
      
           }else{
            window.location.href = "/cronograma-medicamentos/crear";
          }
        })
      },
      err => {
        console.log( err.error);
      }
    );

  }

  diasSeleccionados(diaSelected: any, checked: boolean){
    if (checked) { //Si el elemento fue seleccionado
      //Agregamos la categoría seleccionada al arreglo de categorías seleccionadas
      this.diaSelectedArray.push(diaSelected);
    } else { //Si el elemento fue deseleccionado
      //Removemos la categoría seleccionada del arreglo de categorías seleccionadas
      this.diaSelectedArray.splice(this.diaSelectedArray.indexOf(diaSelected), 1);
    }
  }

  goBack(){
    this._location.back();
  }
    
  }
