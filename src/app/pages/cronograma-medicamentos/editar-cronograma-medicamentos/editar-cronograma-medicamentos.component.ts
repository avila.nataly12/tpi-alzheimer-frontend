import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { CronogramaMedicamentosService } from '../../../services/cronograma-medicamentos/cronograma-medicamentos.service';
import { Cronograma } from '../../../interfaces/cronograma-medicamento';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'; 
import { Usuario } from '../../../interfaces/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-cronograma-medicamentos',
  templateUrl: './editar-cronograma-medicamentos.component.html',
  styleUrls: ['./editar-cronograma-medicamentos.component.scss']
})
export class EditarCronogramaMedicamentosComponent implements OnInit {
  
  idUsuario:string;
  cronograma!: Cronograma;
  items = [] as any;
  idCronograma = this.route.snapshot.paramMap.get('id')!;
  paciente!: Usuario;
  dias:String[]=["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
  diaSelectedArray = [] as any;
  errorMessage!:string;
  medicamentosActivos = [] as any;
  pacienteNombre!:string;
  formMedicamento: FormGroup = this.fb.group({
    idUsuarioOrigen: [''],
    idUsuarioDestino: [''],
    hora:['', [Validators.required]],
    medicamento:['', [Validators.required]],
    items:['']
  });

  constructor(
    private authService: AuthService,
    private cronogramaService: CronogramaMedicamentosService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private _location: Location,

  ) {
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
 
    
   }

  ngOnInit(): void {
  this.cronogramaService.verCronograma(this.idCronograma)
  .subscribe(resp => {
    this.cronograma = resp;

    this.authService.getUsuarioPorId(this.cronograma.idUsuarioDestino).subscribe(usuario => {
      this.pacienteNombre =usuario.apellido + ' ' + usuario.nombre;
    });

  }, error => {
    console.log(error.error);
    
  });

}


goBack(){
  this._location.back();
}

sendMedicamento(): void {
  this.items.push({dias:this.diaSelectedArray, hora:this.formMedicamento.value["hora"], medicamento:this.formMedicamento.value["medicamento"], estado:true});

  this.cronogramaService.agregarMedicacion(this.idCronograma,this.items).subscribe(
    data => {
      location.reload();
    },
    err => {
      console.log( err.error);
    }
  );

}


eliminarMedicamento(index:any) {
  Swal.fire({
    icon: 'question',
    title: '¿Desea eliminar el medicamento?',
    showCancelButton:true,
    confirmButtonText:'Eliminar',
    allowOutsideClick:false
  }).then((result)=>{
    if (result.isConfirmed) {
      console.log(index);

      this.cronogramaService.eliminarMedicacion(this.idCronograma, index).subscribe(
        data => {

        }, err => {
          console.log(err);
        }
      );window.location.reload();
     } 
  })     

}


diasSeleccionados(diaSelected: any, checked: boolean){
  if (checked) { //Si el elemento fue seleccionado
    //Agregamos la categoría seleccionada al arreglo de categorías seleccionadas
    this.diaSelectedArray.push(diaSelected);
  } else { //Si el elemento fue deseleccionado
    //Removemos la categoría seleccionada del arreglo de categorías seleccionadas
    this.diaSelectedArray.splice(this.diaSelectedArray.indexOf(diaSelected), 1);
  }
}


}
