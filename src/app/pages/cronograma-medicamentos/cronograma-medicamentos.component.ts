import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { Usuario } from '../../users/interface/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CronogramaMedicamentosService } from '../../services/cronograma-medicamentos/cronograma-medicamentos.service';
import { Cronograma } from '../../interfaces/cronograma-medicamento';


@Component({
  selector: 'app-cronograma-medicamentos',
  templateUrl: './cronograma-medicamentos.component.html',
  styleUrls: ['./cronograma-medicamentos.component.scss']
})
export class CronogramaMedicamentosComponent implements OnInit {

  cronogramas!: Cronograma[];
  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  idUsuario: string;
  listaPacientes!: Usuario[];
  pacienteNombre!: string;
  pacienteId!: string;
  tienemedicamentos!: string;
  cronogramaPaciente: Usuario[] = [];
  medicamentosActivos = [] as any;

  constructor(
    private fb: FormBuilder,
    private cronogramaService: CronogramaMedicamentosService,
    private router: Router,
    private authService: AuthService
  ) {
    this.idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

    this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
      this.listaPacientes = usuarios;
    }, err => {
      console.log('Error');
    });
    this.pacienteId = this.idUsuario;
    console.log(this.cronogramas);
  }

  ngOnInit(): void {
    if (this.rolUsuario === 'Cuidador') {
      this.cronogramaService.verCronogramasDeCuidador(this.idUsuario)
        .subscribe(data => {
          this.cronogramas = data;
          this.authService.listadoDePacientesPorCuidador(this.idUsuario)
            .subscribe(data => {
              this.listaPacientes = data;

              this.listaPacientes.forEach(paciente => {
                this.cronogramas.forEach(cronograma => {
                  if (paciente.id === cronograma.idUsuarioDestino) {
                    
                    cronograma.paciente = paciente;
                       
                  }
                });
              });
            });
        }, err => {
          console.log('Error');
        });

    } else{
      this.cronogramaService.verCronogramaDePaciente(this.idUsuario)
        .subscribe(data => {
          this.cronogramas = data;
          this.cronogramas.forEach(cronograma => {
            cronograma.items.forEach(item => {
                if (item.estado==true){
                  this.medicamentosActivos.push(item);
                }
            });

          });
        }, err => {
          //  console.log('sin medicamentos');
        });
    }

  }

}
