import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GaleriaComponent } from './galeria.component';
import { CrearAlbumComponent } from './crear-album/crear-album.component';
import { EditarAlbumComponent } from './editar-album/editar-album.component';
import { VerAlbumComponent } from './ver-album/ver-album.component';
import { AgregarPersonaComponent } from './agregar-persona/agregar-persona.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: GaleriaComponent
      },
      {
        path: 'crear/:id',
        component: CrearAlbumComponent
      },
      {
        path: 'album/:id',
        component: VerAlbumComponent
      },
      {
        path: 'album/editar/:id',
        component: EditarAlbumComponent
      },
      {
        path: 'cargar-foto/:id',
        component: AgregarPersonaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GaleriaRoutingModule { }
