import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { GaleriaService } from '../../../services/galeria/galeria.service';
import { Album } from '../../../interfaces/album';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'; 
import { Usuario } from '../../../interfaces/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-album',
  templateUrl: './editar-album.component.html',
  styleUrls: ['./editar-album.component.scss']
})
export class EditarAlbumComponent implements OnInit {


  idAlbum = this.route.snapshot.paramMap.get('id')!;
  nombre!:string;
  album!:Album;
  errorMessage!:string;

  formAlbum: FormGroup = this.fb.group({
    nombre:['', [Validators.required]]
  });

  constructor(
    private authService: AuthService,
    private galeriaService: GaleriaService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private _location: Location,
  ) { }

  ngOnInit(): void {
      this.galeriaService.verAlbum(this.idAlbum)
        .subscribe(data => {
          this.album = data;
          
          this.formAlbum.controls['nombre'].setValue(this.album.nombre);

        }, error => {
          console.log("Error");
        });
    }
  

  sendAlbum(id:string) {
    const nombre = this.formAlbum.get('nombre')?.value;

    Swal.fire({
      icon: 'success',
          title: 'El álbum se editó correctamente',
    }).then((result)=>{
      if (result.isConfirmed) {
      this.galeriaService.editarAlbum(id, nombre).subscribe(
        data => {
        },
        err => {
          this.errorMessage = err.error.message;
          console.log( err.error.message);

          }
       ); 
       this.router.navigate(['/galeria']);
       }
    })
}

  goBack(){
    this._location.back();
  }
}
