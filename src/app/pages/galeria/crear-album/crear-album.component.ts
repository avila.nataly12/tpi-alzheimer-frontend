import { Component, HostDecorator, OnInit } from '@angular/core';
import { GaleriaService } from '../../../services/galeria/galeria.service';
import { Album } from '../../../interfaces/album';
import { Router,ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../../services/auth/auth.service';
import { Usuario } from '../../../users/interface/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';

@Component({
  selector: 'app-crear-album',
  templateUrl: './crear-album.component.html',
  styleUrls: ['./crear-album.component.scss']
})
export class CrearAlbumComponent implements OnInit {
  listaPacientes!: Usuario[];

  formAlbum: FormGroup = this.fb.group({
    idUsuarioOrigen: [''],
    idUsuarioDestino: ['', ],
    nombre:['', [Validators.required]],
  });


  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  idUsuarioDestino!:string;
  items = [] as any;
  album!:Album;


  constructor(
    private galeriaService: GaleriaService,
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder,
    private _location: Location,
    private route: ActivatedRoute
  ) {  
  }

  ngOnInit(): void {
   this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
    this.listaPacientes = usuarios;
    console.log(this.listaPacientes);
  }, err => {
    console.log('Error');
  });
  }

  sendAlbum(): void {
    this.album = this.formAlbum.value;
    // if(this.album.idUsuarioDestino!=null){
    //   this.idUsuarioDestino=String(this.route.snapshot.paramMap.get('idPaciente'));
    //   this.album.idUsuarioOrigen=this.idUsuario;
    // } 

    if (this.rolUsuario === 'Cuidador') { 
      this.album.idUsuarioOrigen = String(this.route.snapshot.paramMap.get('id'));
    } else if (this.rolUsuario === 'Paciente') {
      this.album.idUsuarioOrigen = String(this.route.snapshot.paramMap.get('id'));
      this.album.idUsuarioDestino = String(this.route.snapshot.paramMap.get('id'));
    }

    this.galeriaService.crearAlbum(this.album).subscribe(
      data => {
        // this.album.idUsuarioOrigen = String(this.route.snapshot.paramMap.get('id'));
        // console.log(this.album.idUsuarioOrigen);
        //   if (this.rolUsuario === 'Cuidador') {
        //     this.album.idUsuarioDestino = this.album.idUsuarioDestino; 
        //   } else this.idUsuarioDestino = this.idUsuario;
        Swal.fire({
          icon: 'success',
          title: 'Álbum creado correctamente.',
          allowOutsideClick:false
        }).then((result)=>{
      
      
          if (result.value) {
           
            
            window.location.href = "/galeria";

      
           }else{
            window.location.href = "/galeria/crear/"+this.route.snapshot.paramMap.get('idPaciente');
          }
        })
      },
      err => {
        console.log( err.error);
      }
    );

  }

  goBack(){
    this._location.back();
  }
    
  }
