import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GaleriaRoutingModule } from './galeria-routing.module';
import { GaleriaComponent } from './galeria.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CrearAlbumComponent } from './crear-album/crear-album.component';
import { VerAlbumComponent } from './ver-album/ver-album.component';
import { EditarAlbumComponent } from './editar-album/editar-album.component';
// import { EditarCronogramaMedicamentosComponent } from './editar-cronograma-medicamentos/editar-cronograma-medicamentos.component';


@NgModule({
  declarations: [GaleriaComponent, CrearAlbumComponent,VerAlbumComponent, EditarAlbumComponent],
  imports: [
    CommonModule,
    //ComponentsModule,
    GaleriaRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class GaleriaModule { }

