import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { GaleriaService } from '../../../services/galeria/galeria.service';
import { Album } from '../../../interfaces/album';
import { ArchivoService } from '../../../services/archivos/archivos.service';
import { Archivos } from '../../../interfaces/archivo';
import Swal from 'sweetalert2';
import { ImagenesService } from '../../../services/imagenes.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-ver-album',
  templateUrl: './ver-album.component.html',
  styleUrls: ['./ver-album.component.scss']
})
export class VerAlbumComponent implements OnInit {

  idAlbum = this.route.snapshot.paramMap.get('id')!;
  album!:Album;
  usuarioArchivos!:any;
  idUsuario!:any;
  errorMessage!:string;
  listaArchivos!: Archivos[];
  nombredelalbum!:any;
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private galeriaService: GaleriaService,
    private archivoService: ArchivoService,
    private imagenesSvc: ImagenesService,    
    private _location: Location,
  ) { 
    this.idUsuario=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.usuarioArchivos=this.idUsuario;

  }

  ngOnInit(): void {
    
    this.galeriaService.verAlbum(this.idAlbum)
      .subscribe(data => {
        this.album=data;
        this.nombredelalbum=data.nombre
        var idUusairoDestino =this.album.idUsuarioDestino;

        this.cargarArchivos(idUusairoDestino);
      });

}



cargarArchivos(idUusairoDestino:any){
  
  this.archivoService.verArchivos(idUusairoDestino,this.idAlbum).subscribe(archivos => {
    this.listaArchivos = archivos;
    console.log(this.listaArchivos);
  }, err => {
    console.log('Error');
  });
}



    
eliminar(id:any, nombreImagen:string) {


  Swal.fire({
    icon: 'question',
    title: 'Desea eliminar el registro?',
    showCancelButton:true,
    confirmButtonText:'Eliminar',
    allowOutsideClick:false
  }).then((result)=>{
    if (result.isConfirmed) {

    this.imagenesSvc.eliminarImagen(id, nombreImagen);
    this.archivoService.eliminar(id).subscribe(
      data => {
   
      },
      err => {
        this.errorMessage = err.error.message;
        console.log( err.error.message);
       }
    );
    window.location.href = "/galeria/album/"+this.idAlbum;

    }
 

  })
 

}

goBack(){
  this._location.back();
}
listadoAlbumes(){
  window.location.href = "/galeria";

  }

}

