import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GaleriaService } from '../../services/galeria/galeria.service';
import { Album } from '../../interfaces/album';
import { Archivos } from '../../interfaces/archivo';
import { Usuario } from '../../interfaces/usuario';
import { AuthService } from '../../services/auth/auth.service';
import { __exportStar } from 'tslib';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';
import { bootstrapApplication } from '@angular/platform-browser';
import { ArchivoService } from '../../services/archivos/archivos.service';
 


@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html'
})
export class GaleriaComponent implements OnInit {

  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  idPaciente = this.route.snapshot.paramMap.get('id')!;
  galeria!: Album[];
  archivos: Archivos[]=[];
  album!:Album;
  idAlbum!:string;
  listaPacientes!: Usuario[];
  usuarioAlbumes!:string;
  errorMessage!:string;
  cantidaddeAlbum!:number;
  banderaBusco!:string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private galeriaService: GaleriaService,
    private _location: Location,
    private archivoService: ArchivoService,

    ) {
     
      this.banderaBusco="SI";
      if (this.rolUsuario === 'Cuidador') {
      this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
        this.listaPacientes = usuarios;
      }, err => {
        console.log('Error');
      });
      this.banderaBusco="NO";
    }

  }

  ngOnInit(): void {
    
    if (this.rolUsuario === 'Cuidador') {
      this.idUsuario=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
      this.usuarioAlbumes=this.idUsuario;
     
      this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
        this.listaPacientes = usuarios;

        this.listaPacientes.forEach((elemento) => {
          this.usuarioAlbumes=elemento.id;
    
        });
 


        this.cargarContactos();
        this.cantidaddeAlbum=1;
        console.log(this.cantidaddeAlbum);
        this.banderaBusco="SI";

      }, err => {
        console.log('Error');
      });
    } else {
      this.galeriaService.verGaleriaPaciente(this.idUsuario)
        .subscribe(data => {
          this.galeria = data;
          this.cantidaddeAlbum=this.galeria.length;
          this.buscofotosporalbum();
          
        }, err => {
          console.log('Error');
        });
    }
    
  }

  cambiarlista() {

    this.cargarContactos();
    
  
    setInterval(() => {
      this.banderaBusco="SI";
  }, 1000);


    }

  cargarContactos(){
    this.galeriaService.verGaleriaPaciente(this.usuarioAlbumes).subscribe(data => {
      this.galeria = data;
      this.cantidaddeAlbum=this.galeria.length;

        this.buscofotosporalbum();
        



    }, err => {
      console.log('Error');
 
    });

  }


  buscofotosporalbum(){
    this.galeria.forEach((elemento) => {
        

      this.archivoService.demeUnArchivo(elemento.idUsuarioDestino,elemento.id).subscribe(foto => {
        rutafoto="/assets/img/portadaalbum.png";
          
      

     
       var imagen = JSON.stringify(foto);
       var rutafoto;

       var nuevafoto = JSON.parse(imagen, (key, value) => {

        if(key=="ruta"){
            rutafoto= value;
        }
      });
    
       (document.getElementById(elemento.id) as HTMLElement).innerHTML = '<img src="'+rutafoto+'"  class="card-img-top img-responsive"  >';
   


      //  console.log(this.archivos);

      }, err => {
        console.log('Error');
      });


});

  }

  eliminar(id:string, estado:string) {
    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar el álbum?',
      showCancelButton:true,
      confirmButtonText:'Eliminar',
      allowOutsideClick:false
    }).then((result)=>{
      if (result.isConfirmed) {
      this.galeriaService.eliminarAlbum(id, "false").subscribe(
        data => {
          this.album = data;
        },
        err => {
          this.errorMessage = err.error.message;
          console.log( err.error.message);

          }
      );
      window.location.reload();
       }
    })
}

goBack(){
  this._location.back();
}
}
