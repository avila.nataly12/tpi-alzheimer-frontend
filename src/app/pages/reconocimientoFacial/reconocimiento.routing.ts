import { Routes } from '@angular/router';
import { ReconocimientoComponent } from './reconocimiento.component';
 

export const ReconocimientoRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: ReconocimientoComponent
            }
        ]
    }
]