import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReconocimientoComponent } from './reconocimiento.component';
import { ReconocimientoRoutes } from './reconocimiento.routing';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    ReconocimientoComponent 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ReconocimientoRoutes)
  ]
})
export class ReconocimientoModule { }
