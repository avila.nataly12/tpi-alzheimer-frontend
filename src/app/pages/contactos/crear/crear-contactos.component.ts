import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { Usuario } from '../../../users/interface/usuario';
import { Router } from '@angular/router'; 
import { ContactoService } from '../../../services/contacto/contacto.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-crear-contacto',
  templateUrl: './crear-contactos.component.html',
  styleUrls: ['./crear-contactos.component.scss']
})
export class CrearContactosComponent implements OnInit {

  listaPacientes!: Usuario[];
  
  form: any = {
    nombre: null,
    telefono: null,
    idPaciente: null,
    paciente: null,
    cuidador: null 
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  idUsuario:string;
  idOrigen:string;


  constructor(
    private contactoService: ContactoService,
    private router: Router,
    private authService: AuthService
  ) { 
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.idOrigen=  this.idUsuario;

  }

  ngOnInit(): void {

    this.idOrigen=  this.idUsuario;

    this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
      this.listaPacientes = usuarios;
   
  
    }, err => {
      console.log('Error');
    });
  
    }



      
  onSubmit(): void {

    const { nombre, telefono, paciente, cuidador, idPaciente } = this.form; 

    
    this.contactoService.Crear(idPaciente, nombre, "549"+telefono,paciente, cuidador,this.idOrigen).subscribe(
      data => {

        Swal.fire({
          icon: 'success',
          title: 'El contacto se cargó correctamente',
          text: 'Ya disponible en el listado'
        }).then((result) => {
 
          this.router.navigate(['//contactos']);


        })


      },
      err => {
        this.errorMessage = err.error.message;
        console.log( err.error.message);
        this.isSignUpFailed = true;
      }
    );

    }


}
