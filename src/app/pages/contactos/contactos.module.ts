import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactosComponent } from './contactos.component';
import { ListadoContactosComponent } from './listado/listado-contactos.component';
import { CrearContactosComponent } from './crear/crear-contactos.component';
import { ContactosRoutes } from './contactos.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditarContactosComponent } from './editar/editar-contactos.component';



@NgModule({
  declarations: [
    ContactosComponent,
    ListadoContactosComponent,
    CrearContactosComponent,
    EditarContactosComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ContactosRoutes)
  ]
})
export class ContactosModule { }
