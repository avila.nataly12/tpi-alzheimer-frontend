import { Routes } from '@angular/router';
 import { ContactosComponent } from './contactos.component';
 import { CrearContactosComponent } from './crear/crear-contactos.component';
 import { EditarContactosComponent } from './editar/editar-contactos.component';
import { ListadoContactosComponent } from './listado/listado-contactos.component';
 

export const ContactosRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: ContactosComponent
            } ,  {
                path: 'crear',
                component: CrearContactosComponent
            }  ,  {
                path: 'editar/contacto/:idContacto',
                component: EditarContactosComponent
            } 
            ,  {
                path: 'listado',
                component: ListadoContactosComponent
            } 
        ]
    }
]