import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ImagenesModel } from '../../models/imagenes.model';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ImagenesService } from '../../services/imagenes.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { ContactoService } from '../../services/contacto/contacto.service';
import { Usuario } from '../../users/interface/usuario';
import { AuthService } from '../../services/auth/auth.service';
import { Contacto } from '../../interfaces/contacto';
 

@Component({
  selector: 'app-multimedia',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.scss']
})
export class ContactosComponent implements OnInit {

  listaPacientes!: Usuario[];
  listaContacto!: Contacto[];
  idUsuario!:string;
  usuarioContactos!:string;
  idAlbumCuidador!:string;
  errorMessage!:string;
  cantidad!:number;

  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  constructor(
    private fb: FormBuilder,
    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private imagenesSvc: ImagenesService,    
    private contactoService: ContactoService,
    private authService: AuthService
     ) { 

      this.idAlbumCuidador="1";
     }

  ngOnInit(): void {
    this.idUsuario=(this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.usuarioContactos=this.idUsuario;
    this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
    this.listaPacientes = usuarios;
    this.listaPacientes.forEach((elemento) => {
      this.usuarioContactos=elemento.id;

    });

    this.cargarContactos()
    }, err => {
      console.log('Error');
    });

 


  }

  cambiarlista() {
    this.cargarContactos();
    }



  cargarContactos(){
    console.log(this.usuarioContactos);

    this.contactoService.buscarOrigen(this.idUsuario,this.usuarioContactos).subscribe(contactos => {
      this.listaContacto = contactos;
      this.cantidad=this.listaContacto.length;
    }, err => {
      console.log('Error');
    });
  }

 
  eliminar(id:any) {


    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar el registro?',
      showCancelButton:true,
      confirmButtonText:'Eliminar',
      allowOutsideClick:false
    }).then((result)=>{
      if (result.isConfirmed) {

       this.contactoService.Eliminar(id).subscribe(
        data => {

          
        Swal.fire({
          icon: 'success',
          title: 'Se eliminó correctamente',
          text: ''
        }).then((result) => {
 
        //  this.router.navigate(['//contactos']);
          window.location.href = "/contactos/";

        })


        },
        err => {
          this.errorMessage = err.error.message;
          console.log( err.error.message);
         }
      );
    

      }
   

    })


  }



}
