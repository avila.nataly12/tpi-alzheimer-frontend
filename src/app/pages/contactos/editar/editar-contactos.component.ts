import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { Usuario } from '../../../users/interface/usuario';
import { Router ,ActivatedRoute} from '@angular/router'; 
import { ContactoService } from '../../../services/contacto/contacto.service';
import Swal from 'sweetalert2';
import { Contacto } from '../../../interfaces/contacto';


@Component({
  selector: 'app-editar-contacto',
  templateUrl: './editar-contactos.component.html',
  styleUrls: ['./editar-contactos.component.scss']
})
export class EditarContactosComponent implements OnInit {
   idContacto!:any;

   listaPacientes!: Usuario[];
   contacto!: Contacto;
  
  nombrePaciente!:any;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  idUsuario:string;
  idOrigen:string;


  constructor(
    private contactoService: ContactoService,
    private router: Router,
    private authService: AuthService, private route: ActivatedRoute
  ) { 
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.idOrigen=  this.idUsuario;


  }

  form: any = {
    nombre: "",
    telefono:"",
    pacienteCheckbox:"",
    cuidadorCheckbox:"",
  };

  ngOnInit(): void {

    this.idContacto = String(this.route.snapshot.paramMap.get('idContacto'));

    this.contactoService.getContacto(this.idContacto).subscribe(data => {
      this.contacto= data;

      this.form = {
        nombre: this.contacto.nombre,
        telefono: this.contacto.telefono.slice(3),
        pacienteCheckbox: this.contacto.paciente,
        cuidadorCheckbox: this.contacto.cuidador 
      };

      
    this.authService.getUsuarioPorId(this.contacto.idPaciente).subscribe(usuario => {
      
      this.nombrePaciente = usuario.apellido + ' ' + usuario.nombre;

          }, err => {
            console.log('Error');
          });

  
    }, err => {
      console.log('Error');
    });
    
    }



      
  onSubmit(): void {

    const { nombre, telefono, pacienteCheckbox, cuidadorCheckbox } = this.form; 
 const estado="true";
    this.contactoService.Editar(this.idContacto, nombre, "549"+telefono,estado,pacienteCheckbox, cuidadorCheckbox).subscribe(
      data => {

        Swal.fire({
          icon: 'success',
          title: 'El contacto se editó correctamente',
          text: 'Ya disponible en el listado'
        }).then((result) => {
 
          this.router.navigate(['//contactos']);


        })


      },
      err => {
        this.errorMessage = err.error.message;
        console.log( err.error.message);
        this.isSignUpFailed = true;
      }
    );

    }


}
