import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IdentificarComponent } from './identificar.component';
import { RouterModule } from '@angular/router';
import { IdentificarRoutes } from './identificar.routing';



@NgModule({
  declarations: [
    IdentificarComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(IdentificarRoutes),
  ]
})
export class IdentificarModule { }
