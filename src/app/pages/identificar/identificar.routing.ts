import { Routes } from '@angular/router';
import { IdentificarComponent } from './identificar.component'; 


export const IdentificarRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: IdentificarComponent
            } 
        ]
    }
]