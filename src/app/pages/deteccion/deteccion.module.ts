import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeteccionComponent } from './deteccion.component';
import { RouterModule } from '@angular/router';
import { DeteccionRoutes } from './deteccion.routing';



@NgModule({
  declarations: [
    DeteccionComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(DeteccionRoutes),
  ]
})
export class DeteccionModule { }
