import { Routes } from '@angular/router';
import { DeteccionComponent } from './deteccion.component'; 


export const DeteccionRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: DeteccionComponent
            } 
        ]
    }
]