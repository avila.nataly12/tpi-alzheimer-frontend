import { Component, OnInit } from '@angular/core';
import { ImagenesService } from '../../services/imagenes.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-deteccion',
  templateUrl: './deteccion.component.html',
  styleUrls: ['./deteccion.component.css']
})
export class DeteccionComponent implements OnInit {

  idImagen:any;
  imagenData:any;
  imgNombre:any;
  imgFoto:any;
  idUsuario:any;

  constructor(private imagenesSvc:ImagenesService,
    private authService: AuthService) { }

  ngOnInit() {
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

    this.obtenerImg();

  }


  obtenerImg(){

    this.idImagen = localStorage.getItem('id');
    this.imagenesSvc.getImagen(this.idImagen,this.idUsuario).subscribe(res=>{

        this.imagenData = res;

        this.imgNombre = this.imagenData.nombreImagen;
        this.imgFoto = this.imagenData.imgUrl;
     

    })

  }

  volver(){

    localStorage.removeItem('id');
    location.href = '/identificar';

  }

}
