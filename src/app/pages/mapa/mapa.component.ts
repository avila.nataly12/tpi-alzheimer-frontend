import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { MapService } from '../../services/mapa/map.service';
import { Recorrido } from '../../interfaces/recorrido';
import { RecorridoService } from '../../services/recorrido/recorrido.service';
import { Marker, Popup } from 'mapbox-gl';
import { Feature } from './interfaces/places';
import { AuthService } from '../../services/auth/auth.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.scss'],
  styles: [
    `
    #map {
      width: 100%;
      height: 500px;
    }
    `
  ]
})
export class MapaComponent implements AfterViewInit {

  @ViewChild('mapDiv')
  mapDivElement!: ElementRef;

  estadoRecorrido!: string;

  idPaciente = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

  coordenadas!: number[][];

  ubicacionDestino!: [number, number];

  ubicacionPartida!: [number, number];

  ultimaUbicacion!: [number, number];

  subscription!: Subscription;

  idPacienteParam = this.route.snapshot.paramMap.get('id')!;

  direccionPaciente!: [number, number];

  constructor(private mapService: MapService,
    private authService: AuthService,
    private recorridoService: RecorridoService,
    private route: ActivatedRoute) { }

  ngAfterViewInit(): void {
    console.log(this.mapService.userLocation);
    this.generateMap();

  }

  generateMap() {

    if (this.rolUsuario === "Cuidador") {
      this.authService.getUsuarioPorId(this.idPacienteParam)
        .subscribe(data => {
          this.direccionPaciente = data.coordsdir;
          console.log(this.direccionPaciente);

          const map = new mapboxgl.Map({
            container: this.mapDivElement.nativeElement,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: this.direccionPaciente,
            zoom: 14,

          });

          map.addControl(new mapboxgl.NavigationControl());

          const popUp = new Popup()
            .setHTML(`
                <h6>Ubicación actual</h6>
            `);

          new Marker({ color: 'red' })
            .setLngLat(this.direccionPaciente)
            .setPopup(popUp)
            .addTo(map)

          this.mapService.setMap(map);
        });

    } else {
      this.recorridoService.listarRecorridoPorIdUsuario(this.idPaciente)
        .subscribe(data => {
          this.estadoRecorrido = data[data.length - 1].estado;

          if (this.estadoRecorrido === 'En curso') {
            console.log('Recorrido en curso');

            const map = new mapboxgl.Map({
              container: this.mapDivElement.nativeElement,
              style: 'mapbox://styles/mapbox/streets-v11',
              center: this.mapService.userLocation,
              zoom: 14,

            });

            this.recorridoService.listarRecorridoPorIdUsuario(this.idPaciente)
              .subscribe(resp => {

                setTimeout(() => {

                  console.log(resp);
                  this.coordenadas = resp[resp.length - 1].coordenadas;
                  this.ubicacionPartida = [this.coordenadas[this.coordenadas.length - 1][0], this.coordenadas[this.coordenadas.length - 1][1]];
                  this.ubicacionDestino = [this.coordenadas[0][0], this.coordenadas[0][1]];
                  this.estadoRecorrido = resp[resp.length - 1].estado;

                  // Marcador inicio
                  new Marker({ color: 'red' })
                    .setLngLat(this.ubicacionPartida)
                    .addTo(map)

                  // Marcador final
                  new Marker({ color: 'red' })
                    .setLngLat(this.ubicacionDestino)
                    .addTo(map);

                  this.recorridoService.listarUltimaUbicacionPorIdPaciente(this.idPaciente)
                    .subscribe(data => {
                      this.ultimaUbicacion = data.ubicacion;
                      // Marcador cambiante
                      new Marker({ color: 'blue' })
                        .setLngLat(this.ultimaUbicacion)
                        .addTo(map);
                    });

                  map.fitBounds([this.ubicacionPartida, this.ubicacionDestino], {
                    padding: 100
                  });

                  this.mapService.setMap(map);

                  this.mapService.drawPolyLineCoordenadas(this.coordenadas);

                  this.mapService.deletePlaces();
                }, 2000);


              }, error => {
                console.log(error);

              });

          } else {
            return;

          }

        }, error => {
          console.log(error);
        });

      setTimeout(() => {
        this.generateMap()
      }, 10000);

    }

  }

  editarEstadoRecorrido() {

  }

}
