import { Component, OnInit } from '@angular/core';
import { AsociarService } from '../../../services/asociar/asociar.service';
import { Usuario } from '../../../interfaces/usuario';
import { AuthService } from '../../../services/auth/auth.service';
import { ImagenesService } from '../../../services/imagenes.service';
import { ArchivoService } from '../../../services/archivos/archivos.service';
import { Archivos } from '../../../interfaces/archivo';

@Component({
  selector: 'app-seleccionar-paciente',
  templateUrl: './seleccionar-paciente.component.html',
})
export class SeleccionarPacienteComponent implements OnInit {

  pacientes!: Usuario[];
  idCuidador = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  fotos!: Archivos[];

  constructor(private asociarService: AsociarService,
              private authService: AuthService,
              private archivoService: ArchivoService) { }

  ngOnInit(): void {

    this.asociarService.verPacientesAsociados(this.idCuidador)
      .subscribe(data => {
        this.pacientes = data;
        this.fotos = [];

        this.pacientes.forEach((paciente) => {
          this.archivoService.verFotoPerfil(paciente.id)
            .subscribe((archivo) => {
              this.fotos.push(archivo);
            })
        })
      });
  }

}
