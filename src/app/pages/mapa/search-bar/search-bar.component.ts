import { Component, OnInit } from '@angular/core';
import { MapService } from '../../../services/mapa/map.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Usuario } from '../../../users/interface/usuario';
import { AsociarService } from '../../../services/asociar/asociar.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styles: [
    `
    .search-container {
    top: 100px;
    width: 350px;
    left: 400px;
    background-color: white;
    padding: 20px;
    box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    border: 1px solid rgba(0, 0, 0, 0.1);
}
    `
  ]
})
export class SearchBarComponent implements OnInit {

  cordsdireccion!: any;
  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
  idPaciente = this.route.snapshot.paramMap.get('id')!;
  pacientesAsociados!: Usuario[];
  paciente!: any;

  constructor(private mapService: MapService,
              private authService: AuthService,
              private asociarService: AsociarService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.rolUsuario === "Cuidador") {
      this.authService.getUsuarioPorId(this.idPaciente)
        .subscribe(data => {
          this.paciente = data;
        });
    } else {
      this.authService.getUsuarioPorId(this.idUsuario)
        .subscribe(data => {
          this.paciente = data;
        })
    }
  }

  onQueryChanged(query: string = '') {

    this.mapService.getPlacesByQuery(query);

  }

}