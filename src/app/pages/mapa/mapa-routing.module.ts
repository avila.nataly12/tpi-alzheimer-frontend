import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapaComponent } from './mapa.component';
import { SeleccionarPacienteComponent } from './seleccionar-paciente/seleccionar-paciente.component';
import { MapaPacienteComponent } from './mapa-paciente/mapa-paciente.component';
import { LoginGuard } from '../../services/auth/login.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: SeleccionarPacienteComponent,
      },
      {
        path: 'crear-recorrido/:id',
        component: MapaComponent,
      },
      {
        path: 'empezar-recorrido',
        component: MapaComponent,
      },
      {
        path: 'recorrido/:id',
        component: MapaPacienteComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapaRoutingModule { }
