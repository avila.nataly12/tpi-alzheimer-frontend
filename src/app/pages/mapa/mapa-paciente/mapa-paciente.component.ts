import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecorridoService } from '../../../services/recorrido/recorrido.service';
import { MapService } from '../../../services/mapa/map.service';
import { AuthService } from '../../../services/auth/auth.service';

import * as mapboxgl from 'mapbox-gl';
import { Marker, Popup } from 'mapbox-gl';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mapa-paciente',
  templateUrl: './mapa-paciente.component.html',
  styles: [
    `
    #map {
      width: 100%;
      height: 500px;
    }
    `
  ]
})
export class MapaPacienteComponent implements AfterViewInit {

  @ViewChild('mapDiv')
  mapDivElement!: ElementRef;

  idPaciente = this.route.snapshot.paramMap.get('id')!;

  coordenadas!: number[][];

  ubicacionDestino!: [number, number];

  ubicacionPartida!: [number, number];

  ultimaUbicacion!: [number, number];

  hayCoordenadas = true;

  estadoRecorrido!: string;

  constructor(private mapService: MapService,
    private recorridoService: RecorridoService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngAfterViewInit(): void {
    this.generateMap();
  }


 

  generateMap() {

    const map = new mapboxgl.Map({
      container: this.mapDivElement.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.mapService.userLocation, // TODO: cambiar por la ubicacion de partida
      zoom: 14,

    });
    setTimeout(() => {

    this.recorridoService.listarRecorridoPorIdUsuario(this.idPaciente)
      .subscribe(resp => {
        console.log(resp);
        if (resp.length > 0) {
          this.coordenadas = resp[resp.length - 1].coordenadas;
          this.ubicacionPartida = [this.coordenadas[this.coordenadas.length - 1][0], this.coordenadas[this.coordenadas.length - 1][1]];
          this.ubicacionDestino = [this.coordenadas[0][0], this.coordenadas[0][1]];
          this.estadoRecorrido = resp[resp.length - 1].estado;
          // Marcador inicio
          new Marker({ color: 'red' })
            .setLngLat(this.ubicacionPartida)
            .setPopup(popUp)
            .addTo(map)

          // Marcador final
          new Marker({ color: 'red' })
            .setLngLat(this.ubicacionDestino)
            .setPopup(popUp)
            .addTo(map);

          this.recorridoService.listarUltimaUbicacionPorIdPaciente(this.idPaciente)
            .subscribe(data => {
              this.ultimaUbicacion = data.ubicacion;
              
              // Marcador cambiante
              new Marker({ color: 'blue' })
                .setLngLat(this.ultimaUbicacion)
                .addTo(map);
            });


          map.addControl(new mapboxgl.NavigationControl);

          map.fitBounds([this.ubicacionPartida, this.ubicacionDestino], {
            padding: 100
          });

          this.mapService.setMap(map);

          this.mapService.drawPolyLineCoordenadas(this.coordenadas);

          this.mapService.deletePlaces();
        } else {
          this.hayCoordenadas = false;
        }

        if(this.estadoRecorrido === "Finalizado") {
          Swal.fire({
            icon: 'success',
            title: 'Recorrido finalizado',
            showCancelButton: false,
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false
          }).then((result) => {
            if (result.isConfirmed) {
       //       this.router.navigate([/pacientes/ver-paciente/${this.idPaciente}]);
       window.location.href = "/pacientes/ver-paciente/"+this.idPaciente;
      }

          }, error => {
            console.log(error);

          });
      }

      }, error => {
        console.log(error);
      });


    }, 2000);

    const popUp = new Popup()
      .setHTML(`
            <h6>Ubicación actual</h6>
        `);


        console.log(this.estadoRecorrido);

        if(this.estadoRecorrido != "Recorrido finalizado") {
        setTimeout(() => {
          this.generateMap()
         }, 10000);
        }
  }

}

