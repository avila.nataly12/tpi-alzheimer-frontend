import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapaRoutingModule } from './mapa-routing.module';

import { MapaComponent } from './mapa.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ComponentsModule } from '../../component/component.module';
import { SeleccionarPacienteComponent } from './seleccionar-paciente/seleccionar-paciente.component';
import { MapaPacienteComponent } from './mapa-paciente/mapa-paciente.component';

@NgModule({
  declarations: [MapaComponent, SearchBarComponent, SearchResultsComponent, SeleccionarPacienteComponent, MapaPacienteComponent],
  imports: [
    CommonModule,
    //ComponentsModule,
    MapaRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class MapaModule { }
