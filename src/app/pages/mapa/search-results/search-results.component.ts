import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MapService } from '../../../services/mapa/map.service';
import { Feature } from '../interfaces/places';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Recorrido } from '../../../interfaces/recorrido';
import { RecorridoService } from '../../../services/recorrido/recorrido.service';
import { AuthService } from '../../../services/auth/auth.service';
import { AsociarService } from '../../../services/asociar/asociar.service';
import { Usuario } from '../../../users/interface/usuario';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styles: [
    `
    .pointer {
    cursor: pointer;
}

p {
    font-size: 12px;
}
    `
  ]
})
export class SearchResultsComponent implements OnInit {

  selectedId: string = '';

  coords = this.mapService.coordinates;

  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

  idUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;

  idPacienteParam = this.route.snapshot.paramMap.get('id')!;

  idCuidadorAsociado!: string;

  pacientesAsociados!: Usuario[];

  coordsdir!: [number, number];

  formRecorrido: FormGroup = this.fb.group({
    idUsuarioOrigen: [''],
    idUsuarioDestino: [''],
    coordenadas: [''],
    fechaEstimadaPartida: ['', [Validators.required]],
    fechaEstimadaLlegada: ['', [Validators.required]],
    horaEstimadaPartida: ['', [Validators.required]],
    horaEstimadaLlegada: ['', [Validators.required]]
  });

  constructor(private mapService: MapService,
    private recorridoService: RecorridoService,
    private authService: AuthService,
    private asociarService: AsociarService,
    private fb: FormBuilder,
    private route: ActivatedRoute) { }

  get isLoadingPlaces(): boolean {
    return this.mapService.isLoadingPlaces;
  }

  get places(): Feature[] {
    return this.mapService.places;
  }

  flyTo(place: Feature) {
    this.selectedId = place.id;

    const [lng, lat] = place.center;
    this.mapService.flyTo([lng, lat]);

  }

  getDirections(place: Feature) {
    if (!this.mapService.userLocation) throw Error('No hay userLocation');

    this.mapService.deletePlaces();

    const start = this.coordsdir;
    const end = place.center as [number, number];

    this.mapService.getRouteBetweenPoints(start, end);

  }

  sendRecorrido() {

    const recorrido: Recorrido = this.formRecorrido.value;

    recorrido.idUsuarioOrigen = this.idUsuario;

    recorrido.coordenadas = this.mapService.coordinates;

    recorrido.estado = 'Por confirmar';

    if (this.rolUsuario === "Paciente") {
      recorrido.idUsuarioDestino = this.idCuidadorAsociado;

    } else {
      recorrido.idUsuarioDestino = this.idPacienteParam;

    }

    this.recorridoService.crearRecorrido(recorrido)
      .subscribe(resp => {
        Swal.fire({
          icon: 'success',
          title: 'Recorrido creado correctamente',
          allowOutsideClick:false
        }).then((result)=>{
      
      
          if (result.value) {
           
            window.location.href = `/pacientes/ver-paciente/${this.idPacienteParam}`;
  
           }else{
            window.location.href = `/pacientes/ver-paciente/${this.idPacienteParam}`;
          }
        })
        console.log('Recorrido creado');
        // TODO: agregar notificacion animada de recorrido creado

      }, error => {
        console.log(error);

      });
  }

  ngOnInit(): void {
    if (this.rolUsuario === "Cuidador") {
      this.authService.getUsuarioPorId(this.idPacienteParam)
        .subscribe(data => {
          this.coordsdir = data.coordsdir;
        })
    } else {
      this.asociarService.obtenerCuidadorAsociado(this.idUsuario)
        .subscribe(resp => {
          this.idCuidadorAsociado = resp.idCuidador;

        }, error => {
          console.log(error);

        });
    }

  }

}