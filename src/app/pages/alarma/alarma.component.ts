import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import {SwPush } from '@angular/service-worker';
import { NotasService } from '../../services/notas/notas.service';
import { Nota } from '../../interfaces/nota';
import { Router,ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../services/auth/auth.service';
import { Usuario } from '../../users/interface/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ContactoService } from '../../services/contacto/contacto.service';




interface RecommendedVoices {
	[key: string]: boolean;
}


@Component({
  selector: 'app-notas',
  templateUrl: './alarma.component.html',
  styleUrls: ['./alarma.component.scss']
})
export class AlarmaComponent implements OnInit {

 
	/**
 * 
 * {"publicKey":"BECAXe4bSGX5TzyaYYYcTj10x1Sz7kxbSqNa_eBgy68iZVnQCixyhoUN7l5NlGiDFn1zrX0r-yT48tjNri54C2Q",
 * "privateKey":"KejSBzyPPkZ7Z6LTkje8yXrCAGX3M6hX6-ngK0NNC8o"}
 * 
 */


	 public readonly VAPID_PUBLIC_KEY="BECAXe4bSGX5TzyaYYYcTj10x1Sz7kxbSqNa_eBgy68iZVnQCixyhoUN7l5NlGiDFn1zrX0r-yT48tjNri54C2Q"
	 respuesta: any;

	 
  public sayCommand: string;
	public recommendedVoices: RecommendedVoices;
	public rates: number[];
	public selectedRate: number;
	public selectedVoice: SpeechSynthesisVoice | null;
	public text: string;
	public voices: SpeechSynthesisVoice[];
	public nombreUsuario!:string;


  
  rolUsuario = (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;
  idUsuario:string;
  listaPacientes!: Usuario[];
  pacienteNombre!:string;
  pacienteId!:string;
  hora!:string;
  medicamento!:string;
  miTelefono!:string;
  constructor(private route: ActivatedRoute,
    private fb: FormBuilder,
    private swPush:SwPush,
    private notasService: NotasService,
    private router: Router,
    private authService: AuthService,
	private contactoService: ContactoService,

  ) { 


    this.voices = [];
		this.rates = [ .25, .5, .75, 1, 1.25, 1.5, 1.75, 2 ];
		this.selectedVoice = this.voices[0];
		this.selectedRate = 1;
		this.nombreUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).nombre;

   		 this.text = "Alarma de medicamento";
		this.sayCommand = "";

 
		this.recommendedVoices = Object.create( null );
		this.recommendedVoices[ "Sabina" ] = true;
		this.recommendedVoices[ "Alex" ] = true;
		this.recommendedVoices[ "Alva" ] = true;
		this.recommendedVoices[ "Damayanti" ] = true;
		this.recommendedVoices[ "Daniel" ] = true;
		this.recommendedVoices[ "Fiona" ] = true;
		this.recommendedVoices[ "Fred" ] = true;
		this.recommendedVoices[ "Karen" ] = true;
		this.recommendedVoices[ "Mei-Jia" ] = true;
		this.recommendedVoices[ "Melina" ] = true;
		this.recommendedVoices[ "Moira" ] = true;
		this.recommendedVoices[ "Rishi" ] = true;
		this.recommendedVoices[ "Samantha" ] = true;
		this.recommendedVoices[ "Tessa" ] = true;
		this.recommendedVoices[ "Veena" ] = true;
		this.recommendedVoices[ "Victoria" ] = true;
		this.recommendedVoices[ "Yuri" ] = true;
 




    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
	
	this.miTelefono= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).telefono;
	console.log(this.miTelefono);
    this.pacienteId=this.idUsuario; 

  }


  public demoSelectedVoice() : void {

		if ( ! this.selectedVoice ) {

			console.warn( "Expected a voice, but none was selected." );
			return;

		}

		var demoText = "Voz demo";

		this.stop();
		this.synthesizeSpeechFromText( this.selectedVoice, this.selectedRate, demoText );

	}



  ngOnInit() {
		/*this.voices = speechSynthesis.getVoices();
		this.selectedVoice = ( this.voices[ 0 ] );
		this.updateSayCommand();

		if ( ! this.voices.length ) {

			speechSynthesis.addEventListener(
				"voiceschanged",
				() => {

					this.voices = speechSynthesis.getVoices();
					this.selectedVoice = ( this.voices[ 0 ]  );
					this.updateSayCommand();

				}
			);

		}*/

 
 
    this.hora = String(this.route.snapshot.paramMap.get('hora'));
    this.medicamento = String(this.route.snapshot.paramMap.get('medicamento'));
    this.idUsuario = String(this.route.snapshot.paramMap.get('id'));

    this.authService.getUsuarioPorId(this.idUsuario).subscribe(usuario => {
      
    this.pacienteNombre = usuario.apellido + ' ' + usuario.nombre;
 	this.text = 'El paciente '+ this.pacienteNombre + ' debe tomar el medicamento: ' + this.medicamento + ' a las ' + this.hora + '';
	 this.synthesizeSpeechFromText(  this.voices[ 0 ], this.selectedRate, this.text );
	 this.speak(); 
	 this.subscribeToNotifications(); 
	 
	 this.contactoService.EnviarWhatsapp(this.nombreUsuario,this.miTelefono,this.text).subscribe(mensaje => {
	 Swal.fire({
      icon: 'success',
      title:this.text,
//	  timer: 2000 ,
      showCancelButton:true,
      confirmButtonText:'OK',
      cancelButtonText:'Cerrar',
      allowOutsideClick:false
    }).then((result)=>{
  
  
      if (result.isConfirmed) {
 
//        window.location.href = "/juegos/memoria/"+this.nivel;
  
       }else{
		/*console.log("paso en realidad?")
		let element:HTMLElement = document.getElementById('myButton') as HTMLElement;
		console.log(element);
		element.click();*/


	   }
    })

	});



  }, err => {
    console.log('Error');
  });
/*
  let element:HTMLElement = document.getElementById('myButton') as HTMLElement;
  console.log(element);
  element.click();


setInterval(() => {
	element.click();}, 10000);*/

  }

 
 



    public speak() : void {
	//this.voices = speechSynthesis.getVoices();
	//this.selectedVoice = this.voices[2] ;

		if ( ! this.selectedVoice || ! this.text ) {
			return;

		}

		this.stop();
		this.synthesizeSpeechFromText( this.selectedVoice, this.selectedRate, this.text );
	 
	}


	// I stop any current speech synthesis.
	public stop() : void {

		if ( speechSynthesis.speaking ) {

			speechSynthesis.cancel();

		}

	}


	// I update the "say" command that can be used to generate the a sound file from the
	// current speech synthesis configuration.
	public updateSayCommand() : void {

		if ( ! this.selectedVoice || ! this.text ) {

			return;

		}

		// With the say command, the rate is the number of words-per-minute. As such, we
		// have to finagle the SpeechSynthesis rate into something roughly equivalent for
		// the terminal-based invocation.
		var sanitizedRate = Math.floor( 200 * this.selectedRate );
		var sanitizedText = this.text
			.replace( /[\r\n]/g, " " )
			.replace( /(["'\\\\/])/g, "\\$1" )
		;

		this.sayCommand = `say --voice ${ this.selectedVoice.name } --rate ${ sanitizedRate } --output-file=demo.aiff "${ sanitizedText }"`;

	}

	// ---
	// PRIVATE METHODS.
	// ---

	// I perform the low-level speech synthesis for the given voice, rate, and text.
	public synthesizeSpeechFromText(
  
		voice: SpeechSynthesisVoice,
		rate: number,
		text: string
		) : void {
      
		var utterance = new SpeechSynthesisUtterance( text );
		utterance.voice = this.selectedVoice;
		utterance.rate = rate;
		utterance.lang = 'es-ES';
		//utterance.pitch = 20;

		/*
		this.voices = speechSynthesis.getVoices();
		this.selectedVoice = ( this.voices[4] );
		this.updateSayCommand();
		console.log(this.selectedVoice);
		*/
		
		speechSynthesis.speak( utterance );

	}




	
	subscribeToNotifications(){
   
		this.swPush.requestSubscription({
		  serverPublicKey:this.VAPID_PUBLIC_KEY
		}).then(respuesta=>{
		  
		  this.respuesta=respuesta;
			this.authService.save(this.respuesta,this.idUsuario).subscribe(usuarios => {  
			  console.log("save");   
			  this.sendToNotifications()
			}, err => {
			  console.log('Error save');
			});
		  
		}).catch(err=>{
		  this.respuesta=err;
		  console.log("save "+err);
		})
	  }  
	
	
	
	  sendToNotifications(){
			const link=window.location.href;
			this.authService.send(this.idUsuario,this.text,"Alarma de Medicamento",link).subscribe(usuarios => {
			  console.log("send");     
			}, err => {
			  console.log('send Error');
			});
		  
	   
	  }  

	  

}
