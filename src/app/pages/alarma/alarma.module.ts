import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlarmaComponent } from './alarma.component';
import { AlarmaRoutes } from './alarma.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AlarmaComponent 
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(AlarmaRoutes)
  ]
})
export class AlarmaModule { }
