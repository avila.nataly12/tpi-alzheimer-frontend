import { Routes } from '@angular/router';
 import { AlarmaComponent } from './alarma.component'; 


export const AlarmaRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: ':hora/:medicamento/:id',
                component: AlarmaComponent
            } 
        ]
    }
]