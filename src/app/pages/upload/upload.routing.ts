import { Routes } from '@angular/router';
import { UploadComponent } from './upload.component'; 
import { UploadListadoComponent } from './uploadListado.component'; 


export const UploadRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: ':idAlbum',
                component: UploadComponent
            } ,
            {
                path: 'listado/:idAlbum',
                component: UploadListadoComponent
            }
        ]
    }
]