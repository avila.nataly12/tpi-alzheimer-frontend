import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import * as faceapi from 'face-api.js';
import { ImagenesModel } from '../../models/imagenes.model';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ImagenesService } from '../../services/imagenes.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { ArchivoService } from '../../services/archivos/archivos.service';
import { Usuario } from '../../users/interface/usuario';
import { AuthService } from '../../services/auth/auth.service';
import { Console } from 'console';
import { Location } from '@angular/common';
import { GaleriaService } from '../../services/galeria/galeria.service';
import { Album } from '../../interfaces/album';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  idUsuario!:string;
  tipoarchivo!:string;
  idUsuarioCargador!:any;
  listaPacientes!: Usuario[];
  imgElement = '';
  imgURL = '../../../assets/img/noimage.png';
  imagen: any;
  imagenesData: ImagenesModel[] = [];
  imgProcess: any;
  btnActive = true;
  file: any;
  idAlbum:any;
  nombredelalbum!:any;
  nombredelpaciente!:any;
 album!:Album;
 albumIdPaciente!:any;
rol!:any;

  @ViewChild('imageFile', { static: true }) imageFile!: ElementRef;

  imagenesForm = this.fb.group({

    nombre: ['', [Validators.required]],
    imgFile: [''],
    idPaciente :['null']
  })

  constructor(private fb: FormBuilder, private renderer: Renderer2,
    private router: Router, private route: ActivatedRoute,
     private imagenesSvc: ImagenesService,    
     private archivoService: ArchivoService,
     private authService: AuthService,
     private _location: Location,
     private galeriaService: GaleriaService,


     ) { }

  ngOnInit(): void {
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.rol= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!)).rol;

    this.idAlbum = String(this.route.snapshot.paramMap.get('idAlbum'));
    if(this.idAlbum!=1){
      this.imgURL='../../../assets/img/solofoto.png';
      this.galeriaService.verAlbum(this.idAlbum)
      .subscribe(data => {
        this.album=data;
        this.nombredelalbum=data.nombre;
        this.albumIdPaciente=data.idUsuarioDestino;
                this.dameNombrePaciente(this.albumIdPaciente);
      });
    }

    this.authService.listadoDePacientesPorCuidador(this.idUsuario).subscribe(usuarios => {
      this.listaPacientes = usuarios;
   
  
    }, err => {
      console.log('Error');
    });



    this.mostrarImg()
  }



  dameNombrePaciente(albumIdPaciente: any) {

    this.authService.getUsuarioPorId(albumIdPaciente)
    .subscribe(paciente => {
      this.nombredelpaciente=paciente.apellido + " " + paciente.nombre;

     }, err => {
      console.log('Error');
    });
  }

  selectImage(event: any) {

    if (event.target.files.length > 0) {

      this.file = event.target.files;
      const reader = new FileReader();
      reader.readAsDataURL(this.file[0]);
      reader.onloadend = (event: any) => {

        this.imgURL = event.target.result;
        this.imgElement = event.target.result;
        elementImage.src = `${this.imgElement}`;

        this.imagen = {
          archivo: this.file[0]
        }


      }

      this.btnActive = false;

      var containerImage = document.createElement('div');
      var status = document.createElement('p');
      var icon = document.createElement('i');
      var elementImage = document.createElement('img');


      containerImage.classList.add('containerImage');

      elementImage.crossOrigin = 'anonymous';

     
 
 //image/jpeg  image/png
 this.tipoarchivo=this.file[0]["type"];
      if(this.tipoarchivo=="image/jpeg" ||  this.tipoarchivo=="image/png"|| this.tipoarchivo=="image/jpg" ){
        if(this.idAlbum!=1){
        icon.classList.add('fa');
        icon.classList.add('fa-3x');
        icon.classList.add('fa-spinner');
        icon.classList.add('fa-pulse');
  
        status.classList.add('status');
  
        status.appendChild(icon)
  
        containerImage.appendChild(status);
  
        this.imgProcess = elementImage;
  
        
        this.renderer.appendChild(this.imageFile.nativeElement, containerImage);
            
            this.processFace(this.imgProcess, containerImage);
            }else{
              this.onSubmit();
            }
    }else{
      this.onSubmit();
    }

    }

  }


  processFace = async (image: any, imageContainer: any) => {
     
    await faceapi.nets.tinyFaceDetector.loadFromUri('/assets/models');
    await faceapi.nets.faceLandmark68Net.loadFromUri('/assets/models');
    await faceapi.nets.faceRecognitionNet.loadFromUri('/assets/models');

      //nuevo , test
      imageContainer.querySelector('.status').style.color = 'white';
      imageContainer.querySelector('.status').innerText = 'Procesando Archivo';
      
    const detection = await faceapi.detectSingleFace(image, new faceapi.TinyFaceDetectorOptions())
      .withFaceLandmarks()
      .withFaceDescriptor()
      



    if (typeof detection === 'undefined') {


      //imageContainer.querySelector('.status').innerText = 'No se pudo procesar la imagen';
      imageContainer.querySelector('.status').style.color = 'white';

   /*   setTimeout(() => {
        imageContainer.querySelector('.status').innerText = '';
        this.imgURL = '../../../assets/img/noimage.png';
        this.imagenesForm.reset();

      }, 4000);*/
      this.onSubmit();
      
      setTimeout(() => {
        imageContainer.querySelector('.status').innerText = '';
        imageContainer.querySelector('.status').style.color = 'white';

      }, 4000);


      this.btnActive = true;

    } else {
      imageContainer.querySelector('.status').innerText = 'Archivo procesado';
      imageContainer.querySelector('.status').style.color = 'white';
      this.onSubmit();

      setTimeout(() => {
        imageContainer.querySelector('.status').innerText = '';
        imageContainer.querySelector('.status').style.color = 'white';


      }, 4000);

    }


  }


  mostrarImg() {

    this.imagenesSvc.getImagenes(this.idUsuario).subscribe(res => {

      this.imagenesData = [];

      res.forEach((element: ImagenesModel) => {

        this.imagenesData.push({
          ...element
        })

      })

    })

  }

   sleep(ms:number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

  onSubmit() {

    Swal.fire({
      title: 'Introducir el nombre del archivo',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Guardar',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result.isConfirmed && result.value) {

        let cargarImagenDatos: any = {
          nombreImagen: result.value 
        }
        const formImagen = this.imagenesForm.value;
        const formImagenIdPACIENTE = formImagen.idPaciente;
        this.idUsuarioCargador=this.idUsuario;//27112022 volve a descomentarlo 3011
       // console.log(formImagenIdPACIENTE);//esto en algun momento esta dando 
        if(formImagenIdPACIENTE!="null" && formImagenIdPACIENTE!="" && formImagenIdPACIENTE!=null ){
          this.idUsuarioCargador=formImagen.idPaciente;
         }  



     //this.imagenesSvc.cargarImagenesFirebase(this.imagen, cargarImagenDatos);
     //idUsuarioCargador este es la carpeta



 

     if(this.idAlbum!=1){
        this.idUsuarioCargador=this.albumIdPaciente;
     }


    
     //console.log(this.idUsuarioCargador + this.idUsuario +"asd")
     this.imagenesSvc.cargarImagenesFirebase2(this.imagen, cargarImagenDatos,this.idAlbum, this.idUsuarioCargador, this.idUsuario, this.tipoarchivo);

     
     (document.querySelector('.subiendoImagen') as HTMLElement).style.display = 'block';

     setTimeout(this.functionRef,5000,this.idAlbum)



        
      } else {

        if (!result.isConfirmed && !result.value) {
          location.reload();
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Debe llenar el nombre',
            confirmButtonText: 'OK'

          }).then((result) => {
            this.imagenesForm.reset();
          })
        }

      }

    })

  }





  
  functionRef(Idalbum:any){
    (document.querySelector('.subiendoImagen') as HTMLElement).style.display = 'none';

    Swal.fire({
         icon: 'success',
         title: 'El archivo se cargó correctamente',
         text: 'Ya disponible en el listado'
       }).then((result) => {

          if(Idalbum!=1){
            window.location.href = "/galeria/album/"+Idalbum;

          }else{
           window.location.href = "/upload/"+Idalbum;
          }

       });
     }


     

  eliminar(id:any, nombreImagen:string) {


    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar el registro?',
      showCancelButton:true,
      confirmButtonText:'Eliminar',
      allowOutsideClick:false
    }).then((result)=>{
      if (result.isConfirmed) {

        this.imagenesSvc.eliminarImagen(id, nombreImagen);
      }
    })



  }

  volver(){
  window.location.href = "/galeria/album/"+this.idAlbum;

  }

  volverMultimedia(){
    window.location.href = "/compartirMultimedia/listado";
  
    }


}
