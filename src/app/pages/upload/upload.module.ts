import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadComponent } from './upload.component';
import { UploadListadoComponent } from './uploadListado.component';
import { UploadRoutes } from './upload.routing';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    UploadComponent,UploadListadoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule ,
    RouterModule.forChild(UploadRoutes),
   ]
})
export class UploadModule { }
