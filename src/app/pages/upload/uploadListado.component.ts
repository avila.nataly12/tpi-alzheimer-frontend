import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import * as faceapi from 'face-api.js';
//import { ImagenesModel } from 'src/app/models/imagenes.model';
import { ImagenesModel } from '../../models/imagenes.model';
import { getStorage, ref, uploadBytesResumable, getDownloadURL, deleteObject } from "firebase/storage";

import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
//import { ImagenesService } from 'src/app/services/imagenes.service';
import { ImagenesService } from '../../services/imagenes.service';
import { ArchivoService } from '../../services/archivos/archivos.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../services/auth/auth.service';
import { Archivos } from '../../interfaces/archivo';


@Component({
  selector: 'app-upload-listado',
  templateUrl: './uploadListado.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadListadoComponent implements OnInit {
  idUsuario!:string;
  idAlbum:any;
  imgElement = '';
  imgURL = '../../../assets/img/noimage.png';
  imagen: any;
  imagenesData: ImagenesModel[] = [];
  imgProcess: any;
  btnActive = true;
  file: any;
  errorMessage = '';
  archivos!: Archivos[];

  @ViewChild('imageFile', { static: true }) imageFile!: ElementRef;

  imagenesForm = this.fb.group({

    nombre: ['', [Validators.required]],
    imgFile: ['']

  })

  constructor(private fb: FormBuilder, private renderer: Renderer2,
    private archivoService: ArchivoService,
    private imagenesSvc: ImagenesService,
    private router: Router, private route: ActivatedRoute,
    private authService: AuthService

    ) { }

  ngOnInit(): void {
    this.idUsuario= (this.authService.getDecodedAccessToken(localStorage.getItem('token')!))._id;
    this.idAlbum = String(this.route.snapshot.paramMap.get('idAlbum'));

     this.dameArchivos()
   // this.mostrarImg()
  }
 

  dameArchivos(){
    this.archivoService.verArchivos(this.idUsuario,this.idAlbum).subscribe(
      data => {
        this.archivos = data;

      },
      err => {
        this.errorMessage = err.error.message;
        console.log( err.error.message);
       }
    );
  }
  mostrarImg() { 

    this.imagenesSvc.getImagenes(this.idUsuario).subscribe(res => {

      this.imagenesData = [];

      res.forEach((element: ImagenesModel) => {
         this.imagenesData.push({
          ...element
        })

      })

    })

  }
 


  eliminar(id:any, nombreImagen:string) {


    Swal.fire({
      icon: 'question',
      title: 'Desea eliminar el registro?',
      showCancelButton:true,
      confirmButtonText:'Eliminar',
      allowOutsideClick:false
    }).then((result)=>{
      if (result.isConfirmed) {

      //  this.imagenesSvc.eliminarImagen(id, nombreImagen);
      this.imagenesSvc.eliminarImagen(id, nombreImagen);
         //luego elimino de mongo
      this.archivoService.eliminar(id).subscribe(
        data => {
          //eliminar
  
        },
        err => {
          this.errorMessage = err.error.message;
          console.log( err.error.message);
         }
      );


      }
    })



  }


 




}
