// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
   /* projectId: 'deteccionfaceapi-93ce6',//deteccionfaceapi-bc00a
    appId: '1:747088274818:web:c1b454da4f217d3b37db9b',
    storageBucket: 'deteccionfaceapi-93ce6.appspot.com',//deteccionfaceapi-bc00a.appspot.com
    locationId: 'southamerica-east1',//nam5 (us-central)
    apiKey: 'AIzaSyCWlz66p5Ju-vLDKpLUy2m1qTaxSmEed2c',
    authDomain: 'deteccionfaceapi-93ce6.firebaseapp.com',//deteccionfaceapi-bc00a.appspot.com
    messagingSenderId: '747088274818',//619498483217*/
    

    apiKey: "AIzaSyBymIfuAxQjFZN7NBymy8XTD7ONWbQ9s9Q",
    authDomain: "deteccionfaceapi-bc00a.firebaseapp.com",
    projectId: "deteccionfaceapi-bc00a",
    storageBucket: "deteccionfaceapi-bc00a.appspot.com",
    messagingSenderId: "619498483217",
    appId: "1:619498483217:web:bbd32fdf33a3bab772cbb0",
//    measurementId: "G-LELTQX0Q8F",
    locationId: 'nam5(us-central)',


  },
  production: false,
  baseUrl: 'http://localhost:8080/',
  apiKey: 'pk.eyJ1IjoibmF0YXZpbGEiLCJhIjoiY2w4YjN5Ymk1MGhlajN2bHlmdnI3ZGg3ZyJ9.l6rNDRSpojQpphQXH-GYLA'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
